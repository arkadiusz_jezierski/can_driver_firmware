/*
 * File:  pwm_actuator_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */

#include "comm_settings.h"

#if PWM_ACTUATOR_QNTY > 0

#include "pwm_driver.h"
#ifdef DEBUG
#include "uart.h"
#endif
#include <../sources/plib/SPI/spi_open.c>

void makeCommandForPwmActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address) {

    if (address == 0) {
        makeBroadcastCommandForPwmActuator(outputMessage, inputMessage, categoryDevices, command);
        return;
    }

    signed char channel;
    unsigned char value;
    switch (command) {
        case CMD_CAN_PING:
            insertCanCommand(outputMessage, CMD_CAN_PONG);
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            insertResetCounters(outputMessage);
            break;
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("ALL OFF");
#endif
            memset(requestedPWM, 0, PWM_ACTUATOR_QNTY);
            break;

        case CMD_CAN_SET_PWM:
#ifdef DEBUG
            UART_SendNewLine("SET PWM");
#endif
            value = getCanChannelPwmValue(inputMessage);
            channel = getCanPwmChannel(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setPwmValue(channel, value);
            }
            break;

        case CMD_CAN_GET_PWM:
#ifdef DEBUG
            UART_SendNewLine("GET PWM");
#endif
            channel = getCanPwmChannel(categoryDevices, address);
            value = getPwmValue(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_PWM);
                setCanChannelPwmValue(outputMessage, (unsigned char) value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_PWM_UP:
#ifdef DEBUG
            UART_SendNewLine("SET PWM UP");
#endif
            value = getCanChannelPwmValue(inputMessage);
            channel = getCanPwmChannel(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0 && channel < PWM_ACTUATOR_QNTY) {
                increasePWM(requestedPWM, value, (unsigned char) channel);
            }
            break;

        case CMD_CAN_PWM_DOWN:
#ifdef DEBUG
            UART_SendNewLine("SET PWM DOWN");
#endif
            value = getCanChannelPwmValue(inputMessage);
            channel = getCanPwmChannel(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0 && channel < PWM_ACTUATOR_QNTY) {
                decreasePWM(requestedPWM, value, (unsigned char) channel);
            }
            break;

        case CMD_CAN_SET_PWM_ALL:
#ifdef DEBUG
            UART_SendNewLine("SET PWM ALL");
#endif
            channel = getCanPwmChannel(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel == 0) {
#ifdef DEBUG
                UART_SendNewLine("RESET TMP PWM ");
#endif
                resetPWM(tmpPWM);
            }
            if (channel >= 0) {
                if ((channel + 7) < PWM_ACTUATOR_QNTY) {
                    for (char i = 0; i < 7; i++) {
                        tmpPWM[channel + i] = getCanChannelPwmValueForMultiSet(inputMessage, 6 - i);
#ifdef DEBUG
                        UART_SendNewLine("1. TMP PWM ");
                        byte2ascii((unsigned char) channel + i, 0);
                        UART_SendNewLine("==");
                        byte2ascii((unsigned char) tmpPWM[channel + i], 0);
#endif
                    }
                }
                if (channel < PWM_ACTUATOR_QNTY && (channel + 7) >= PWM_ACTUATOR_QNTY) {
                    for (char i = 0; i < (PWM_ACTUATOR_QNTY - channel); i++) {
                        unsigned char offset = PWM_ACTUATOR_QNTY - channel - 1 - i;
                        tmpPWM[channel + i] = getCanChannelPwmValueForMultiSet(inputMessage, offset);
#ifdef DEBUG
                        UART_SendNewLine("2. TMP PWM ");
                        byte2ascii((unsigned char) channel + i, 0);
                        UART_SendNewLine("==");
                        byte2ascii((unsigned char) tmpPWM[channel + i], 0);
#endif
                    }
                    memcpy(requestedPWM, tmpPWM, PWM_ACTUATOR_QNTY);

                }
            }
            break;
            
            case CMD_CAN_SET_PWM_ALL_THE_SAME:
#ifdef DEBUG
            UART_SendNewLine("BRCST SET ALL");
#endif
            value = getCanChannelPwmValue(inputMessage);
            memset(requestedPWM, value, PWM_ACTUATOR_QNTY);
            break;

        case CMD_CAN_PWM_UP_ALL:
#ifdef DEBUG
            UART_SendNewLine("BRCST CMD_CAN_PWM_UP_ALL");
#endif
            value = getCanChannelPwmValue(inputMessage);
            increaseAllChannelPWM(requestedPWM, value);
            break;

        case CMD_CAN_PWM_DOWN_ALL:
#ifdef DEBUG
            UART_SendNewLine("BRCST CMD_CAN_PWM_DOWN_ALL");
#endif
            value = getCanChannelPwmValue(inputMessage);
            decreaseAllChannelPWM(requestedPWM, value);
            break;
    }
}

void makeBroadcastCommandForPwmActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command) {

    unsigned char value;
    switch (command) {
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("BRCST ALL OFF");
#endif
            memset(requestedPWM, 0, PWM_ACTUATOR_QNTY);
            break;
        
        case CMD_CAN_SET_PWM:
#ifdef DEBUG
            UART_SendNewLine("SET PWM");
#endif
            value = getCanChannelPwmValue(inputMessage);
            memset(requestedPWM, value, PWM_ACTUATOR_QNTY);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            break;

        case CMD_CAN_PWM_UP:
#ifdef DEBUG
            UART_SendNewLine("SET PWM UP");
#endif
            value = getCanChannelPwmValue(inputMessage);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            increaseAllChannelPWM(requestedPWM, value);
            break;

        case CMD_CAN_PWM_DOWN:
#ifdef DEBUG
            UART_SendNewLine("SET PWM DOWN");
#endif
            value = getCanChannelPwmValue(inputMessage);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            decreaseAllChannelPWM(requestedPWM, value);
            break;
            
        case CMD_CAN_SET_PWM_ALL_THE_SAME:
#ifdef DEBUG
            UART_SendNewLine("BRCST SET ALL");
#endif
            value = getCanChannelPwmValue(inputMessage);
            memset(requestedPWM, value, PWM_ACTUATOR_QNTY);
            break;

        case CMD_CAN_PWM_UP_ALL:
#ifdef DEBUG
            UART_SendNewLine("BRCST CMD_CAN_PWM_UP_ALL");
#endif
            value = getCanChannelPwmValue(inputMessage);
            increaseAllChannelPWM(requestedPWM, value);
            break;

        case CMD_CAN_PWM_DOWN_ALL:
#ifdef DEBUG
            UART_SendNewLine("BRCST CMD_CAN_PWM_DOWN_ALL");
#endif
            value = getCanChannelPwmValue(inputMessage);
            decreaseAllChannelPWM(requestedPWM, value);
            break;

    }
}

unsigned char readDevicePwmActuator(CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent) {


    static unsigned char initialize = 1;
    if (initialize || initCategory()) {
        //                initPingTable(&pingInfo);
        initPwmStatusesTable(categoryDevices, &pwmStatuses);
        initialize = 0;
    }

    if (timerEvent) {
        checkRequestedPwmValue();
        checkDevStatBeforeEepromUpdate();
        if (checkPwmValues(outputMessage, categoryDevices, &pwmStatuses))
            return 1;
    }


    return 0;
}

void checkRequestedPwmValue() {
    char changeOn = 0;
    for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        if (pwm[i] > requestedPWM[i]) {
            pwm[i]--;
            changeOn = 1;
        } else if (pwm[i] < requestedPWM[i]) {
            pwm[i]++;
            changeOn = 1;
        }
    }
    if (changeOn) {
        changeOn = 0;
        writePWM(pwm);
    }
}

void checkDevStatBeforeEepromUpdate() {
    char changed = 0;
    static char timerCounter = 0;
    for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        if (eepromPWM[i] != requestedPWM[i]) {
            changed = 1;
            break;
        }
    }
    
    if (changed) {
        timerCounter++;
        if (timerCounter > 220) {
            timerCounter = 0;
            
            for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
                if (eepromPWM[i] != requestedPWM[i]) {
                    updateDevStateInEeprom(i, requestedPWM[i]);
                }
            }
            
        }
    } else {
        timerCounter = 0;
    }
}

signed char getCanPwmChannel(categoryStruct* categoryDevices, unsigned char address) {
#ifdef DEBUG
    UART_SendNewLine("dev qnty: ");
    byte2ascii(categoryDevices->devicesQnt, 1);
    UART_SendNewLine("adr: ");
    byte2ascii(address, 1);
#endif
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
#ifdef DEBUG
        UART_SendNewLine("chk adr: ");
        byte2ascii(categoryDevices->deviceAddress[dev], 1);
#endif
        if (categoryDevices->deviceAddress[dev] == address) {
            return dev;
        }
    }
    return -1;
}

unsigned char getCanChannelPwmValue(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_PWM_VALUE) {
        return message->Data[OFFSET_CAN_PWM_VALUE];
    }
    return 0;
}

unsigned char getCanChannelPwmValueForMultiSet(CANMessage *message, unsigned char channelOffset) {
    if (message->NoOfBytes > OFFSET_CAN_PWM_VALUE + channelOffset) {
        #ifdef DEBUG
        for (int i = 0; i < message->NoOfBytes; i++){
                        UART_SendNewLine("BUFF");
                        byte2ascii((unsigned char) i, 0);
                        UART_SendNewLine("==");
                        byte2ascii((unsigned char)message->Data[i], 1);
        }
#endif
        return message->Data[OFFSET_CAN_PWM_VALUE + channelOffset];
    }
    return 0;
}

void setCanChannelPwmValue(CANMessage* message, unsigned char state) {
    message->Data[OFFSET_CAN_PWM_VALUE] = state;
    message->NoOfBytes++;
}

void initPwmStatusesTable(categoryStruct* categoryDevices, pwmStatus *pwmStatuses) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {

        pwmStatuses[dev].address = categoryDevices->deviceAddress[dev];
        pwmStatuses[dev].pwmValueInitialized = FALSE;
#ifdef DEBUG
        UART_SendNewLine("State adr ");
        byte2ascii(pwmStatuses[dev].address, 1);
        UART_SendString(" stat: ");
        byte2ascii(pwmStatuses[dev].pwmValue, 1);
        UART_SendString(" dev adr: ");
        byte2ascii(categoryDevices->deviceAddress[dev], 1);
#endif
    }
}

unsigned char checkPwmValues(CANMessage *message, categoryStruct* categoryDevices, pwmStatus *pwmStatuses) {
    unsigned char value;
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        value = getPwmValue(dev);
#ifdef DEBUG
//        UART_SendNewLine("State id ");
//        byte2ascii(dev, 1);
//        UART_SendString(" stat: ");
//        byte2ascii(value, 1);
//        UART_SendString(" last: ");
//        byte2ascii(pwmStatuses[dev].pwmValue, 1);
#endif
        if (pwmStatuses[dev].pwmValue != value || pwmStatuses[dev].pwmValueInitialized == FALSE) {
            pwmStatuses[dev].pwmValueInitialized = TRUE;
            pwmStatuses[dev].pwmValue = value;
            insertCanCommand(message, CMD_CAN_SET_PWM);
            setCanChannelPwmValue(message, (unsigned char) value);
            setSourceCanAddress(message, pwmStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
    }
    return 0;
}

void initPwmActuatorPortIO() {
    ADCON1bits.PCFG = 0x0f;

    LED_DIR &= ~((1 << PIN_BLANK) | (1 << PIN_XLAT));
    LED_PORT |= (1 << PIN_BLANK);
    LED_PORT &= ~(1 << PIN_XLAT);

    OpenSPI(0, 0, 0);

    resetPWM(pwm);
    resetPWM(requestedPWM);
    resetPWM(eepromPWM);
    
    if (startupInfo == RESET_BY_WDT || startupInfo == RESET_BY_RESET_INSTRUCTION) {
        loadDevStateFromEeprom();
    } else {
#ifdef POWER_ON_START
        for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
            pwm[i] = requestedPWM[i] = eepromPWM[i] = 0xff;
        }
#else
        for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
            pwm[i] = requestedPWM[i] = eepromPWM[i] = 0;
        }
#endif
        initDevStateInEeprom();
    }
    
    writePWM(pwm);

}

//void setAllPWM(unsigned char *pwm) {
//    writePWM(pwm);
//}

//void setPWM(unsigned char *pwm, unsigned char value, unsigned char channel) {
//    if (channel < OUT_CNT) {
//        pwm[channel] = value;
//        writePWM(pwm);
//    }
//}

int remapPWM(unsigned char pwm) {
    //    return 16 * pwm;
    return pwmMap[pwm];
}

unsigned char getPWM(unsigned char *pwm, unsigned char channel) {
    if (channel < PWM_ACTUATOR_QNTY) {
        return pwm[channel];
    } else {
        return 0;
    }
}

void writeToBuff(unsigned char *pwm, unsigned char *buffer) {
    int tmpPWM;
    char offset;
    char i = 0;
    offset = OFFSET
    if ((288 - (PWM_ACTUATOR_QNTY * 12)) % 8) {
        tmpPWM = ~remapPWM(pwm[i++]);
        buffer[offset++] = (tmpPWM >> 8) & 0x0f;
        buffer[offset++] = tmpPWM & 0xff;
    }
    //8->24, 14->15
    while (i < PWM_ACTUATOR_QNTY) {
        tmpPWM = ~remapPWM(pwm[i++]);
        buffer[offset++] = (tmpPWM >> 4) & 0xff;
        buffer[offset] = (tmpPWM << 4) & 0xf0;
        tmpPWM = ~remapPWM(pwm[i++]);
        buffer[offset++] |= ((tmpPWM >> 8) & 0x0f);
        buffer[offset++] = tmpPWM & 0xff;
    }
}

void putBufSPI(unsigned char *wrptr, unsigned char len) {
    while (len > 0) // test for string null character
    {
        SSPBUF = *wrptr++; // initiate SPI bus cycle
        while (!SSPSTATbits.BF); // wait until 'BF' bit is set
        len--;
    }
}

void resetPWM(unsigned char *pwm) {
    for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        pwm[i] = 0;
    }
}

void writePWM(unsigned char *pwm) {
    unsigned char total[36];
    memset(total, 0xff, 36);
    writeToBuff(pwm, total);
    putBufSPI(total, 36);

    LED_PORT |= (1 << PIN_BLANK);
    LED_PORT |= (1 << PIN_XLAT);
    LED_PORT &= ~(1 << PIN_XLAT);
    LED_PORT &= ~(1 << PIN_BLANK);

}

void increasePWM(unsigned char *pwm, unsigned char value, unsigned char channel) {
    char tmpPWM = getPWM(pwm, channel);
    if (tmpPWM <= (0xff - value)) {
        pwm[channel] = tmpPWM + value;
    } else {
        pwm[channel] = 0xff;
    }
}

void increaseAllChannelPWM(unsigned char *pwm, unsigned char value) {
    for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        if (getPWM(pwm, i) <= (0xff - value)) {
            pwm[i] += value;
        } else {
            pwm[i] = 0xff;
        }
    }
}

void decreasePWM(unsigned char *pwm, unsigned char value, unsigned char channel) {
    unsigned char tmpPWM = getPWM(pwm, channel);
    if (tmpPWM >= value) {
        pwm[channel] = tmpPWM - value;
    } else {
        pwm[channel] = 0;
    }
}

void decreaseAllChannelPWM(unsigned char *pwm, unsigned char value) {
    for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        if (getPWM(pwm, i) >= value) {
            pwm[i] -= value;
        } else {
            pwm[i] = 0;
        }
    }
}

unsigned char getPwmValue(signed char channel) {
    if (channel < PWM_ACTUATOR_QNTY) {
        return getPWM(requestedPWM, (unsigned char) channel);
    }
    return 0;
}

void setPwmValue(signed char channel, unsigned char value) {
    if (channel < PWM_ACTUATOR_QNTY) {
        requestedPWM[channel] = value;
    }
}

void initDevStateInEeprom() {
    for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i, eepromPWM[i]);
    }
}

void updateDevStateInEeprom(unsigned char dev, unsigned char val) {
    eepromPWM[dev] = val;
    eepromWrite(OFFSET_EEPROM_DEV_STATE + dev, val);
}

void loadDevStateFromEeprom() {
    for (char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        pwm[i] = eepromPWM[i] = requestedPWM[i] = eepromRead(OFFSET_EEPROM_DEV_STATE + i);
    }
}

void handleIRSupport() {
    char poweredOn = 0;
    for (signed char i = 0; i < PWM_ACTUATOR_QNTY; i++) {
        if (getPwmValue(i) > 0) {
            poweredOn = 1;
            break;
        }
    }
    
    if (poweredOn) {
        memset(requestedPWM, 0, PWM_ACTUATOR_QNTY);
    } else {
        memset(requestedPWM, 0xff, PWM_ACTUATOR_QNTY);
    }
}

#endif