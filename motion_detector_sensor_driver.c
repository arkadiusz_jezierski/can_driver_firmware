/* 
 * File:   bistable_switch_actuator_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */

#include "comm_settings.h"

#if MOTION_DETECTOR_QNTY > 0

#include "motion_detector_sensor_driver.h"
#ifdef DEBUG
#include "uart.h"
#endif

void makeCommandForMotionDetector(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address) {

    if (address == 0) {
        makeBroadcastCommandForMotionDetector(outputMessage, inputMessage, categoryDevices, command);
        return;
    }

    signed char devNmb;
    signed char state, level;
    switch (command) {
        case CMD_CAN_PING:
            insertCanCommand(outputMessage, CMD_CAN_PONG);
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            insertResetCounters(outputMessage);
            break;

        case CMD_CAN_SET_SENSITIVITY:
#ifdef DEBUG
            UART_SendNewLine("SET SENSITIVITY");
#endif
            level = getCanMotionDetectorSensitivity(inputMessage);
            devNmb = getMotionDetectorNumber(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) devNmb, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (devNmb >= 0 && level >= 0) {
                setSensitivity(devNmb, level);
            }
#ifdef DEBUG
            UART_SendNewLine("LATA: ");
            byte2ascii(LATA, 1);
#endif
            break;
        case CMD_CAN_SET_LED_POWER:
#ifdef DEBUG
            UART_SendNewLine("SET SENSITIVITY");
#endif
            state = getCanMotionDetectorLedPower(inputMessage);
            devNmb = getMotionDetectorNumber(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) devNmb, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (devNmb >= 0 && state >= 0) {
                setLedPower(devNmb, state);
            }
#ifdef DEBUG
            UART_SendNewLine("LATA: ");
            byte2ascii(LATA, 1);
#endif
            break;
        case CMD_CAN_GET_SENSITIVITY:
#ifdef DEBUG
            UART_SendNewLine("GET");
#endif
            devNmb = getMotionDetectorNumber(categoryDevices, address);
            level = getSensitivity(devNmb);

#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) devNmb, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (devNmb >= 0 && level >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_SENSITIVITY);
                setCanSensitivity(outputMessage, (unsigned char) level);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
        case CMD_CAN_GET_LED_POWER:
#ifdef DEBUG
            UART_SendNewLine("GET LED POWER");
#endif
            devNmb = getMotionDetectorNumber(categoryDevices, address);
            state = getLedPower(devNmb);

#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) devNmb, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (devNmb >= 0 && state >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_LED_POWER);
                setCanLedPower(outputMessage, (unsigned char) state);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
        case CMD_CAN_GET_MOTION_DETECTED_STATUS:
#ifdef DEBUG
            UART_SendNewLine("GET LED POWER");
#endif
            devNmb = getMotionDetectorNumber(categoryDevices, address);
            state = getMotionDetectorStatus(devNmb);

#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) devNmb, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (devNmb >= 0 && state >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_MOTION_DETECTED_STATUS);
                setCanMotionDetectorStatus(outputMessage, (unsigned char) state);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
        case CMD_CAN_GET_TEMPER_CONTACT_STATUS:
#ifdef DEBUG
            UART_SendNewLine("GET LED POWER");
#endif
            devNmb = getMotionDetectorNumber(categoryDevices, address);
            state = getMotionDetectorStatus(devNmb);

#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) devNmb, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (devNmb >= 0 && state >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_TEMPER_CONTACT_STATUS);
                setCanMotionDetectorTmpStatus(outputMessage, (unsigned char) state);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
    }
}

void makeBroadcastCommandForMotionDetector(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command) {

    signed char devNmb;
    signed char level, state;
    switch (command) {

        case CMD_CAN_SET_SENSITIVITY:
#ifdef DEBUG
            UART_SendNewLine("BRCST SET_SENSITIVITY");
#endif
            level = getCanMotionDetectorSensitivity(inputMessage);
            if (level >= 0) {
                for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
                    devNmb = getMotionDetectorNumber(categoryDevices, categoryDevices->deviceAddress[dev]);
#ifdef DEBUG
                    UART_SendNewLine("dev nmb ");
                    byte2ascii(devNmb, 0);
                    UART_SendString(" adr: ");
                    byte2ascii(categoryDevices->deviceAddress[dev], 0);
#endif
                    if (devNmb >= 0) {
                        setSensitivity(devNmb, level);
                    }
                }
            }
            break;

        case CMD_CAN_SET_LED_POWER:
#ifdef DEBUG
            UART_SendNewLine("BRCST SET LED_POWER");
#endif
            state = getCanMotionDetectorLedPower(inputMessage);
            if (state >= 0) {
                for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
                    devNmb = getMotionDetectorNumber(categoryDevices, categoryDevices->deviceAddress[dev]);
#ifdef DEBUG
                    UART_SendNewLine("Output ");
                    byte2ascii(devNmb, 0);
                    UART_SendString(" adr: ");
                    byte2ascii(categoryDevices->deviceAddress[dev], 0);
#endif
                    if (devNmb >= 0) {
                        setLedPower(devNmb, state);
                    }
                }
            }
            break;

    }
}

unsigned char readDeviceMotionDetector(CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent) {


    static unsigned char initialize = 1;
    if (initialize || initCategory()) {
        //                initPingTable(&pingInfo);
        initDeviceStatusesTable(categoryDevices, &deviceStatuses);
        initialize = 0;
    }

    if (timerEvent) {
        checkDevStatBeforeEepromUpdate();
        if (checkDeviceStatuses(outputMessage, categoryDevices, &deviceStatuses)){
#ifdef DEBUG
            UART_SendString("Something was changed");
#endif
            return 1;
        }
    }


    return 0;
}

void checkDevStatBeforeEepromUpdate() {
    static char timerCounter = 0;
    unsigned char ledPower = getLedPower(0);
    unsigned char sensitivity = getSensitivity(0);

    if (eepromLedPower != ledPower || eepromSensitivity != sensitivity) {
        timerCounter++;
        if (timerCounter > 220) {
            timerCounter = 0;
            
            if (eepromLedPower != ledPower) {
                updateDevStateInEeprom(EEPROM_LED_POWER, ledPower);
            }
            if (eepromSensitivity != sensitivity) {
                updateDevStateInEeprom(EEPROM_SENSITIVITY, sensitivity);
            }
         
        }
    } else {
        timerCounter = 0;
    }
  
}


signed char getMotionDetectorNumber(categoryStruct* categoryDevices, unsigned char address) {
    //    UART_SendNewLine("dev qnty: ");
    //    byte2ascii(categoryDevices->devicesQnt, 1);
    //    UART_SendNewLine("adr: ");
    //    byte2ascii(address, 1);
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        //          UART_SendNewLine("chk adr: ");
        //    byte2ascii(categoryDevices->deviceAddress[dev], 1);
        if (categoryDevices->deviceAddress[dev] == address) {
            return dev;
        }
    }
    return -1;
}

signed char getCanMotionDetectorSensitivity(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_MOTION_SENS) {
        return message->Data[OFFSET_CAN_MOTION_SENS];
    } else {
        return -1;
    }
}

signed char getCanMotionDetectorLedPower(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_MOTION_LED_POWER) {
        return message->Data[OFFSET_CAN_MOTION_LED_POWER];
    } else {
        return -1;
    }
}

void setCanSensitivity(CANMessage* message, unsigned char level) {
    message->Data[OFFSET_CAN_MOTION_SENS] = level;
    message->NoOfBytes++;
}

void setCanLedPower(CANMessage* message, unsigned char state) {
    message->Data[OFFSET_CAN_MOTION_LED_POWER] = state;
    message->NoOfBytes++;
}

void setCanMotionDetectorStatus(CANMessage* message, unsigned char state) {
    message->Data[OFFSET_CAN_MOTION_STATUS] = state;
    message->NoOfBytes++;
}

void setCanMotionDetectorTmpStatus(CANMessage* message, unsigned char state) {
    message->Data[OFFSET_CAN_MOTION_TMP_STATUS] = state;
    message->NoOfBytes++;
}

void initDeviceStatusesTable(categoryStruct* categoryDevices, deviceStatus *deviceStatuses) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {

        deviceStatuses[dev].address = categoryDevices->deviceAddress[dev];
        deviceStatuses[dev].sensitivityInitialized = FALSE;
        deviceStatuses[dev].ledPowerInitialized = FALSE;
        deviceStatuses[dev].motionStatusInitialized = FALSE;
        deviceStatuses[dev].temperStatusInitialized = FALSE;

#ifdef DEBUG
        UART_SendNewLine("State adr ");
        byte2ascii(deviceStatuses[dev].address, 1);
        UART_SendString(" motion: ");
        byte2ascii(deviceStatuses[dev].motionStatus, 1);
        UART_SendString(" temper: ");
        byte2ascii(deviceStatuses[dev].temperStatus, 1);
        UART_SendString(" sensitiv: ");
        byte2ascii(deviceStatuses[dev].sensitivity, 1);
        UART_SendString(" ledPower: ");
        byte2ascii(deviceStatuses[dev].ledPower, 1);
#endif
    }
}

unsigned char checkDeviceStatuses(CANMessage *message, categoryStruct* categoryDevices, deviceStatus *deviceStatuses) {
    unsigned char sensitivity, ledPower, motion, temper;
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        sensitivity = getSensitivity(dev);
        if (deviceStatuses[dev].sensitivity != sensitivity || deviceStatuses[dev].sensitivityInitialized == FALSE) {
            deviceStatuses[dev].sensitivityInitialized = TRUE;
            deviceStatuses[dev].sensitivity = sensitivity;
            insertCanCommand(message, CMD_CAN_SET_SENSITIVITY);
            setCanSensitivity(message, (unsigned char) sensitivity);
            setSourceCanAddress(message, deviceStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
        ledPower = getLedPower(dev);
        if (deviceStatuses[dev].ledPower != ledPower || deviceStatuses[dev].ledPowerInitialized == FALSE) {
            deviceStatuses[dev].ledPowerInitialized = TRUE;
            deviceStatuses[dev].ledPower = ledPower;
            insertCanCommand(message, CMD_CAN_SET_LED_POWER);
            setCanLedPower(message, (unsigned char) ledPower);
            setSourceCanAddress(message, deviceStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
        motion = getMotionDetectorStatus(dev);
        if (deviceStatuses[dev].motionStatus != motion || deviceStatuses[dev].motionStatusInitialized == FALSE) {
            deviceStatuses[dev].motionStatusInitialized = TRUE;
            deviceStatuses[dev].motionStatus = motion;
            insertCanCommand(message, CMD_CAN_GET_MOTION_DETECTED_STATUS);
            setCanMotionDetectorStatus(message, (unsigned char) motion);
            setSourceCanAddress(message, deviceStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
        temper = getMotionDetectorTmpStatus(dev);
        if (deviceStatuses[dev].temperStatus != temper || deviceStatuses[dev].temperStatusInitialized == FALSE) {
            deviceStatuses[dev].temperStatusInitialized = TRUE;
            deviceStatuses[dev].temperStatus = temper;
            insertCanCommand(message, CMD_CAN_GET_TEMPER_CONTACT_STATUS);
            setCanLedPower(message, (unsigned char) temper);
            setSourceCanAddress(message, deviceStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
    }
    return 0;
}

void initMotionDetectorPortIO() {
    ADCON1bits.PCFG = 0x0f;
    OUTPUT_DIR &= ~((1 << OUTPUT_LED_PIN) | (1 << OUTPUT_SENS_1_PIN) | (1 << OUTPUT_SENS_2_PIN));
    OUTPUT_PORT &= ~((1 << OUTPUT_LED_PIN) | (1 << OUTPUT_SENS_1_PIN) | (1 << OUTPUT_SENS_2_PIN));
    INPUT_DIR |= ((1 << INPUT_MOTION_PIN) | (1 << INPUT_TMP_PIN));
    
    eepromLedPower = getLedPower(0);
    eepromSensitivity = getSensitivity(0);
    
    initDevStateInEeprom();
    
    if (startupInfo == RESET_BY_WDT || startupInfo == RESET_BY_RESET_INSTRUCTION) {
        loadDevStateFromEeprom();
    } else {
        initDevStateInEeprom();
    }
}

signed char getSensitivity(unsigned char devNmb) {

    if (devNmb < MOTION_DETECTOR_QNTY) {
        switch (devNmb) {
            case 0: if (OUTPUT_SENS_1_STATE && OUTPUT_SENS_2_STATE) {
                    return 3;
                } else if (!OUTPUT_SENS_1_STATE && OUTPUT_SENS_2_STATE) {
                    return 2;
                } else if (OUTPUT_SENS_1_STATE && !OUTPUT_SENS_2_STATE) {
                    return 1;
                } else {
                    return 0;
                }
        }
    }

    return -1;
}

signed char getLedPower(unsigned char devNmb) {

    if (devNmb < MOTION_DETECTOR_QNTY) {
        switch (devNmb) {
            case 0: if (OUTPUT_LED_STATE) {
                    return 1;
                } else {
                    return 0;
                }
        }
    }

    return -1;
}

signed char getMotionDetectorStatus(unsigned char devNmb) {

    if (devNmb < MOTION_DETECTOR_QNTY) {
        switch (devNmb) {
            case 0: if (SENSOR_MOTION_STATE) {
                    return 1;
                } else {
                    return 0;
                }
        }
    }

    return -1;
}

signed char getMotionDetectorTmpStatus(unsigned char devNmb) {

    if (devNmb < MOTION_DETECTOR_QNTY) {
        switch (devNmb) {
            case 0: if (SENSOR_TMP_STATE) {
                    return 1;
                } else {
                    return 0;
                }
        }
    }

    return -1;
}

void setSensitivity(unsigned char devNmb, unsigned char level) {

    if (devNmb < MOTION_DETECTOR_QNTY) {
        if (level == 0) {
            switch (devNmb) {
                case 0: OUTPUT_SENS_1_OFF
                    OUTPUT_SENS_2_OFF
                    break;
            }
        } else if (level == 1) {
            switch (devNmb) {
                case 0: OUTPUT_SENS_1_ON
                    OUTPUT_SENS_2_OFF
                    break;
            }
        } else if (level == 2) {
            switch (devNmb) {
                case 0: OUTPUT_SENS_1_OFF
                    OUTPUT_SENS_2_ON
                    break;
            }
        } else if (level == 3) {
            switch (devNmb) {
                case 0: OUTPUT_SENS_1_ON
                    OUTPUT_SENS_2_ON
                    break;
            }
        }
    }
}

void setLedPower(unsigned char devNmb, unsigned char state) {

    if (devNmb < MOTION_DETECTOR_QNTY) {
        if (state == 0) {
            switch (devNmb) {
                case 0: OUTPUT_LED_OFF
                    break;
            }
        } else {
            switch (devNmb) {
                case 0: OUTPUT_LED_ON
                    break;
            }
        }
    }
}

void initDevStateInEeprom() {
    eepromWrite(OFFSET_EEPROM_DEV_STATE + EEPROM_LED_POWER, eepromLedPower);
    eepromWrite(OFFSET_EEPROM_DEV_STATE + EEPROM_SENSITIVITY, eepromSensitivity);
}

void updateDevStateInEeprom(unsigned char type, unsigned char val) {
    if (type == EEPROM_LED_POWER) {
        eepromLedPower = val;
    } else if (type == EEPROM_SENSITIVITY) {
        eepromSensitivity = val;
    }
    
    eepromWrite(OFFSET_EEPROM_DEV_STATE + type, val);
}

void loadDevStateFromEeprom() {
    eepromLedPower = eepromRead(OFFSET_EEPROM_DEV_STATE + EEPROM_LED_POWER);
    setLedPower(0, eepromLedPower);
    eepromSensitivity = eepromRead(OFFSET_EEPROM_DEV_STATE + EEPROM_SENSITIVITY);
    setSensitivity(0, eepromSensitivity);
}

#endif