/* 
 * File:   bistable_switch_sensor_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */

#ifndef BISTABLE_SWITCH_SENSOR_DRIVER_H
#define	BISTABLE_SWITCH_SENSOR_DRIVER_H

#if SWITCH_SENSOR_QNTY > 0

#include "comm_can_constans.h"
#include "utils.h"

#include "uart.h"
#include "offline_driver.h" 


#define ACK_INIT        1
#define ACK_AWAITING    2
#define ACK_RECEIVED    0

#if PROXIMITY_SENSOR_ACTUATOR
#define NO_ACK_TOUT     120//2000
#define NO_ACK_TOUT_IN_OFFLINE      3 * NO_ACK_TOUT
#else
#define NO_ACK_TOUT     SEC_VALUE
#define NO_ACK_TOUT_IN_OFFLINE      3 * NO_ACK_TOUT
#endif

#if PROXIMITY_SENSOR_ACTUATOR
#define SENSOR_DIR       TRISA
#define LED_OUT_DIR      TRISA
#define SENSOR_PORT      PORTA
#define LED_OUT_PORT     LATA
#define SENSOR_PIN_1     0
#define LED_OUT_PIN      1

#define SENSOR_STATE_1 (SENSOR_PORT & (1 << SENSOR_PIN_1))
#define LED_OUT_ON      (LED_OUT_PORT |= ( 1 << LED_OUT_PIN))
#define LED_OUT_OFF     (LED_OUT_PORT &= ~( 1 << LED_OUT_PIN))

#elif MAGNETIC_SENSOR
#define SENSOR_DIR       TRISC
#define SENSOR_PORT      PORTC
#define SENSOR_PIN_1     1
#define SENSOR_PIN_2     2
#define SENSOR_PIN_3     3
#define SENSOR_PIN_4     4

#define SENSOR_STATE_1 SENSOR_PORT & (1 << SENSOR_PIN_1)
#define SENSOR_STATE_2 SENSOR_PORT & (1 << SENSOR_PIN_2)
#define SENSOR_STATE_3 SENSOR_PORT & (1 << SENSOR_PIN_3)
#define SENSOR_STATE_4 SENSOR_PORT & (1 << SENSOR_PIN_3)

#else
#define SENSOR_DIR       TRISA
#define SENSOR_DIR_B     TRISB
#define SENSOR_PORT      PORTA
#define SENSOR_PORT_B    PORTB
#define SENSOR_PIN_1     2
#define SENSOR_PIN_2     3
#define SENSOR_PIN_3     7
#define SENSOR_PIN_4     7

#define SENSOR_STATE_1 SENSOR_PORT & (1 << SENSOR_PIN_1)
#define SENSOR_STATE_2 SENSOR_PORT & (1 << SENSOR_PIN_2)
#define SENSOR_STATE_3 SENSOR_PORT_B & (1 << SENSOR_PIN_3)
#define SENSOR_STATE_4 SENSOR_PORT & (1 << SENSOR_PIN_4)

#endif





typedef void (*functionReceiver) (CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address);
typedef unsigned char (*functionTransmitter) (CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent);

typedef struct {
    unsigned char address;
    signed char state;
    unsigned char ackStatus;
} sensorStatus;

sensorStatus sensorStatuses[SWITCH_SENSOR_QNTY];

unsigned long ackTimeout = NO_ACK_TOUT;


void makeCommandForBistableSensor(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address );
unsigned char readDeviceBistableSensor(CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent);
void makeBroadcastCommandForBistableSensor(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command);

void setCanInputStatus(CANMessage* message, unsigned char state);


void initSensorStatusesTable( categoryStruct* categoryDevices, sensorStatus *deviceStatuses);
unsigned char checkSensorStatuses(CANMessage *message, categoryStruct* categoryDevices, sensorStatus *deviceStatuses);
signed char getCanInputNumber( categoryStruct* categoryDevices, unsigned char address);
unsigned char checkAckStatus(CANMessage *message, categoryStruct* categoryDevices);
void setACK(CANMessage *message, categoryStruct* categoryDevices, sensorStatus *deviceStatuses);

signed char getSensorStatus(unsigned char pinNo);
void initBistableSwitchSensorPortIO(void);

void setAckTimeout(void);

#endif
#endif	/* BISTABLE_SWITCH_SENSOR_DRIVER_H */

