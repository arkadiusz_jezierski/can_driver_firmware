#include "offline_driver.h"

#if SWITCH_ACTUATOR_QNTY > 0
#include "bistable_switch_actuator_driver.h"
#endif
#if SWITCH_SENSOR_QNTY > 0
#include "bistable_switch_sensor_driver.h"
#endif
#if PWM_ACTUATOR_QNTY > 0
#include "pwm_driver.h"
#endif
#if RGB_ACTUATOR_QNTY > 0
#include "rgb_driver.h"
#endif


void setOfflineMode() {
#ifdef DEBUG
    UART_SendNewLine("OFFLINE MODE ON");
#endif
    
#ifdef OFFLINE_MODE    
    if (offline == 0) {
        initOfflineMode();
    }
#endif
    offline = 1;
}

void exitOfflineMode() {
#ifdef DEBUG
    UART_SendNewLine("OFFLINE MODE OFF");
#endif
    offline = 0;
}

unsigned char checkOfflineMode() {
    return offline;
}




#ifdef OFFLINE_MODE
void initOfflineMode() {
#if SWITCH_SENSOR_QNTY > 0
    for (unsigned char i = 0; i < SWITCH_SENSOR_QNTY; i++) {
        sensorOfflineStatus[i] = getSensorStatus(i);
    }
#endif
}


#if SWITCH_SENSOR_QNTY > 0
char checkSensorChange(unsigned char sensor) {
    signed char currentStatus = getSensorStatus(sensor);
    if (currentStatus != sensorOfflineStatus[sensor]) {
        sensorOfflineStatus[sensor] = currentStatus;
        return 1;
    }
    
    return 0;
}
#endif



void makeOfflineModeOperations() {

#ifdef PROXIMITY_SENSOR_ACTUATOR
    if (checkSensorChange(0)) {
        switchOutputState(0);
    }
    
#else

    #if SWITCH_ACTUATOR_QNTY == 5 && SWITCH_SENSOR_QNTY == 3
    if (checkSensorChange(0)) {
        switchOutputState(0);
        switchOutputState(2);
    }
    if (checkSensorChange(1)) {
        switchOutputState(1);
        switchOutputState(3);
    }
    setOutputStatus(4, 0);
#endif

#if SWITCH_ACTUATOR_QNTY == 4 && SWITCH_SENSOR_QNTY == 3
    if (checkSensorChange(0)) {
        switchOutputState(0);
        switchOutputState(1);
    }

    setOutputStatus(2, 0);
    setOutputStatus(3, 0);
#endif

#if SWITCH_ACTUATOR_QNTY == 2 && SWITCH_SENSOR_QNTY == 1
    if (checkSensorChange(0)) {
        switchOutputState(0);
        switchOutputState(1);
    }
#endif

#if SWITCH_ACTUATOR_QNTY == 1 && SWITCH_SENSOR_QNTY == 0

    setOutputStatus(0, 1);

#endif

#if SWITCH_ACTUATOR_QNTY == 2 && SWITCH_SENSOR_QNTY == 0

    setOutputStatus(0, 1);
    setOutputStatus(1, 1);

#endif

    
#endif

}
#endif  //OFFLINE_MODE