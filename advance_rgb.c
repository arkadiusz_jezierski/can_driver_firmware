
#include "comm_settings.h"

#if RGB_ADVANCE_ACTUATOR_QNTY > 0


#include "advance_rgb.h"

void initRgbActuatorPortIO() {
    ADCON1bits.PCFG = 0x0f;

    UART_Init(115200);

    if (startupInfo == RESET_BY_WDT || startupInfo == RESET_BY_RESET_INSTRUCTION) {
        loadDevStateFromEeprom();
    } else {
        resetRGB();
        initDevStateInEeprom();
    }

}


void makeCommandForRgbActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address) {

    if (address == 0) {
        makeBroadcastCommandForRgbActuator(outputMessage, inputMessage, categoryDevices, command);
        return;
    }

    signed char channel;
    unsigned int value;
    singleRGB values;
    switch (command) {
        case CMD_CAN_PING:
            insertCanCommand(outputMessage, CMD_CAN_PONG);
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            insertResetCounters(outputMessage);
            break;
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("ALL OFF");
#endif
            setMode(MODE_OFF);
            break;

        case CMD_CAN_SET_RGB_CHANNEL_RED:
#ifdef DEBUG
            UART_SendNewLine("SET rgb red");
#endif
            value = getCanRgbSingleValue(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setRed(value, channel);
            }
            break;

        case CMD_CAN_SET_RGB_CHANNEL_GREEN:
#ifdef DEBUG
            UART_SendNewLine("SET rgb green");
#endif
            value = getCanRgbSingleValue(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setGreen(value, channel);
            }
            break;

        case CMD_CAN_SET_RGB_CHANNEL_BLUE:
#ifdef DEBUG
            UART_SendNewLine("SET rgb blue");
#endif
            value = getCanRgbSingleValue(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setBlue(value, channel);
            }
            break;

        case CMD_CAN_SET_RGB_CHANNEL_ALL:
#ifdef DEBUG
            UART_SendNewLine("SET rgb all");
#endif
            values = getCanRgbAllValues(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("red ");
            byte2ascii((unsigned char) values.red, 1);
            UART_SendNewLine("green ");
            byte2ascii((unsigned char) values.green, 1);
            UART_SendNewLine("blue ");
            byte2ascii((unsigned char) values.blue, 1);
#endif
            if (channel >= 0) {
                setRGB(values, channel);
            }
            break;

        case CMD_CAN_SET_RGB_MODE:
#ifdef DEBUG
            UART_SendNewLine("SET rgb mode");
#endif
            value = getCanRgbMode(inputMessage);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
   
            setMode(value);
            
            break;
        case CMD_CAN_SWITCH_MODE:
       
            setMode(switchMode());
                    
            break;
        case CMD_CAN_SET_RGB_SPEED:
#ifdef DEBUG
            UART_SendNewLine("SET rgb speed");
#endif
            value = getCanRgbSpeed(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setSpeed(value);
            }
            break;

        case CMD_CAN_GET_RGB_CHANNEL_RED:
#ifdef DEBUG
            UART_SendNewLine("GET rgb red");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getRed(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_RED);
                setCanRgbSingleValue(outputMessage, value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_CHANNEL_GREEN:
#ifdef DEBUG
            UART_SendNewLine("GET rgb green");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getGreen(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_GREEN);
                setCanRgbSingleValue(outputMessage, value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_CHANNEL_BLUE:
#ifdef DEBUG
            UART_SendNewLine("GET rgb blue");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getBlue(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_BLUE);
                setCanRgbSingleValue(outputMessage, value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_CHANNEL_ALL:
#ifdef DEBUG
            UART_SendNewLine("GET rgb all");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            values = getRGB(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("red ");
            byte2ascii((unsigned char) values.red, 1);
            UART_SendNewLine("green ");
            byte2ascii((unsigned char) values.green, 1);
            UART_SendNewLine("blue ");
            byte2ascii((unsigned char) values.blue, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_ALL);
                setCanRgbAllValues(outputMessage, values);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_MODE:
#ifdef DEBUG
            UART_SendNewLine("GET rgb mode");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getMode();
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_MODE);
                setCanRgbMode(outputMessage, (unsigned char) value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_SPEED:
#ifdef DEBUG
            UART_SendNewLine("GET rgb speed");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getSpeed();
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_SPEED);
                setCanRgbSpeed(outputMessage, (unsigned char) value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
    }
}

unsigned char readDeviceRgbActuator(CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent) {


    static unsigned char initialize = 1;
    if (initialize || initCategory()) {

        initRgbStatusesTable(categoryDevices, &rgbStatuses);
        initialize = 0;
    }

    if (timerEvent) {
        checkDevStatBeforeEepromUpdate();
        if (checkRgbValues(outputMessage, categoryDevices, &rgbStatuses))
            return 1;
    }

    return 0;
}

void checkDevStatBeforeEepromUpdate() {
    char changed = 0;
    static char timerCounter = 0;
    
    for (char i = 0; i < RGB_ADVANCE_ACTUATOR_QNTY; i++) {
        singleRGB values = getRGB(i);
        if (eepromDevStat[i].values.red != values.red
                || eepromDevStat[i].values.green != values.green
                || eepromDevStat[i].values.blue != values.blue
                || eepromDevStat[i].mode != getMode()
                || eepromDevStat[i].speed != getSpeed()) {
            changed = 1;
            break;
        }
    }
    
    if (changed) {
        timerCounter++;
        if (timerCounter > 220) {
            timerCounter = 0;
            
            for (char i = 0; i < RGB_ADVANCE_ACTUATOR_QNTY; i++) {
                singleRGB values = getRGB(i);
                unsigned char mode = getMode();
                unsigned char speed = getSpeed();

                if (eepromDevStat[i].values.red != values.red) {
                    updateDevStateInEeprom(i, EEPROM_RGB_RED , values.red);
                }
                if (eepromDevStat[i].values.green != values.green) {
                    updateDevStateInEeprom(i, EEPROM_RGB_GREEN , values.green);
                }
                if (eepromDevStat[i].values.blue != values.blue) {
                    updateDevStateInEeprom(i, EEPROM_RGB_BLUE , values.blue);
                }
                if (eepromDevStat[i].mode != mode) {
                    updateDevStateInEeprom(i, EEPROM_RGB_MODE , mode);
                }
                if (eepromDevStat[i].speed != speed) {
                    updateDevStateInEeprom(i, EEPROM_RGB_SPEED, speed);
                }
            }
            
        }
    } else {
        timerCounter = 0;
    }
}

void makeBroadcastCommandForRgbActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command) {

    switch (command) {
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("BRCST ALL OFF");
#endif
            setMode(MODE_OFF);
            break;

    }
}

void resetRGB() {
    for (char i = 0; i < RGB_ADVANCE_ACTUATOR_QNTY; i++) {
        rgbCurrent[i].red = 0;
        rgbCurrent[i].green = 0;
        rgbCurrent[i].blue = 0;
    }
}

void setMode(unsigned char mode) {
    if (modeRGB == mode) {
        return;
    }
    modeRGB = mode;

    switch (mode) {

        case MODE_ALL_AUTO:
            //resetRGB();
            uartSetMode(MODE_ALL_AUTO);
            break;
        case MODE_RND_ALL:
            uartSetMode(MODE_RND_ALL);
            break;
        case MODE_RND:
            uartSetMode(MODE_RND);
            break;

        case MODE_CHAIN_AUTO:
            break;
        case MODE_OFF:
            uartSetMode(MODE_OFF);
            break;
        case MODE_SIMPLE:
            uartSetMode(MODE_SIMPLE);
            for (char i = 0; i < RGB_ADVANCE_ACTUATOR_QNTY; i++) {
                uartSetRGB(rgbCurrent[i]);
            }
            break;
    }
}


void setRed(unsigned int value, signed char channel) {
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        rgbCurrent[channel].red = value;
        if (modeRGB == MODE_SIMPLE) {
            uartSetRed(value);
        }
    }
}

void setGreen(unsigned int value, signed char channel) {
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        rgbCurrent[channel].green = value;
        if (modeRGB == MODE_SIMPLE) {
            uartSetGreen(value);
        }
    }
}

void setBlue(unsigned int value, signed char channel) {
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        rgbCurrent[channel].blue = value;
        if (modeRGB == MODE_SIMPLE) {
            uartSetBlue(value);
        }
    }
}

void setRGB(singleRGB values, signed char channel) {
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        rgbCurrent[channel].red = values.red;
        rgbCurrent[channel].green = values.green;
        rgbCurrent[channel].blue = values.blue;
        if (modeRGB == MODE_SIMPLE) {
            uartSetRGB(rgbCurrent[channel]);
        }
    }
}

void setSpeed(unsigned char speed) {
    speedRGB = (char) speed;
    uartSetSpeed(speed);
}


unsigned char getSpeed() {

    return (unsigned char) speedRGB;

}

unsigned char getMode() {
    return (unsigned char) modeRGB;
}

singleRGB getRGB(signed char channel) {
    singleRGB values;
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        values.red = rgbCurrent[channel].red;
        values.green = rgbCurrent[channel].green;
        values.blue = rgbCurrent[channel].blue;
    } else {
        values.red = values.green = values.blue = 0;
    }
    return values;
}

unsigned int getRed(signed char channel) {
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        return rgbCurrent[channel].red;
    }
    return 0;
}

unsigned int getGreen(signed char channel) {
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        return rgbCurrent[channel].green;
    }
    return 0;
}

unsigned int getBlue(signed char channel) {
    if (channel < RGB_ADVANCE_ACTUATOR_QNTY) {
        return rgbCurrent[channel].blue;
    }
    return 0;
}


signed char getCanRgbStrip(categoryStruct* categoryDevices, unsigned char address) {
#ifdef DEBUG
    UART_SendNewLine("dev qnty: ");
    byte2ascii(categoryDevices->devicesQnt, 1);
    UART_SendNewLine("adr: ");
    byte2ascii(address, 1);
#endif
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
#ifdef DEBUG
        UART_SendNewLine("chk adr: ");
        byte2ascii(categoryDevices->deviceAddress[dev], 1);
#endif
        if (categoryDevices->deviceAddress[dev] == address) {
            return dev;
        }
    }
    return -1;
}

unsigned int getCanRgbSingleValue(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE + 1) {
        unsigned int value = message->Data[OFFSET_CAN_RGB_VALUE] & 0x0f;
        value << = 8;
        value += message->Data[OFFSET_CAN_RGB_VALUE + 1];

        return value;
    }
    return 0;
}

singleRGB getCanRgbAllValues(CANMessage *message) {
    singleRGB values;

    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE + 4) {
        values.red = message->Data[OFFSET_CAN_RGB_VALUE] & 0x0f;
        values.red << = 8;
        values.red += message->Data[OFFSET_CAN_RGB_VALUE + 1];
        values.green = message->Data[OFFSET_CAN_RGB_VALUE + 2];
        values.green << = 4;
        values.green |= (message->Data[OFFSET_CAN_RGB_VALUE + 3] >> 4) & 0x0f;
        values.blue = message->Data[OFFSET_CAN_RGB_VALUE + 3] & 0x0f;
        values.blue << = 8;
        values.blue += message->Data[OFFSET_CAN_RGB_VALUE + 4];
    } else {
        values.red = 0;
        values.green = 0;
        values.blue = 0;
    }
    return values;
}

unsigned char getCanRgbMode(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE) {
        return message->Data[OFFSET_CAN_RGB_VALUE];
    }
    return 0;
}

unsigned char getCanRgbSpeed(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE) {
        return message->Data[OFFSET_CAN_RGB_VALUE];
    }
    return 0;
}

void initRgbStatusesTable(categoryStruct* categoryDevices, rgbStatus *rgbStatuses) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {

        rgbStatuses[dev].address = categoryDevices->deviceAddress[dev];
        rgbStatuses[dev].valueInitialized = FALSE;
        rgbStatuses[dev].modeInitialized = FALSE;
        rgbStatuses[dev].speedInitialized = FALSE;
    }
}

unsigned char checkRgbValues(CANMessage *message, categoryStruct* categoryDevices, rgbStatus *rgbStatuses) {
    singleRGB values;
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        values = getRGB(dev);
#ifdef DEBUG
        UART_SendNewLine("channel ");
        byte2ascii((unsigned char) dev, 1);
        UART_SendNewLine("rgbCurrent red ");
        byte2ascii((unsigned char) values.red, 1);
        UART_SendNewLine("rgbStatuses red ");
        byte2ascii((unsigned char) rgbStatuses[dev].values.red, 1);
#endif
        if (rgbStatuses[dev].values.red != values.red
            || rgbStatuses[dev].values.green != values.green
            || rgbStatuses[dev].values.blue != values.blue
            || rgbStatuses[dev].valueInitialized == FALSE
            ) {
            rgbStatuses[dev].valueInitialized = TRUE;
            rgbStatuses[dev].values.red = values.red;
            rgbStatuses[dev].values.green = values.green;
            rgbStatuses[dev].values.blue = values.blue;
            insertCanCommand(message, CMD_CAN_SET_RGB_CHANNEL_ALL);
            setCanRgbAllValues(message, values);
            setSourceCanAddress(message, rgbStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
        if (rgbStatuses[dev].mode != modeRGB || rgbStatuses[dev].modeInitialized == FALSE) {
            rgbStatuses[dev].modeInitialized = TRUE;
            rgbStatuses[dev].mode = modeRGB;
            insertCanCommand(message, CMD_CAN_SET_RGB_MODE);
            setCanRgbMode(message, modeRGB);
            setSourceCanAddress(message, rgbStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
        if (rgbStatuses[dev].speed != speedRGB || rgbStatuses[dev].speedInitialized == FALSE) {
            rgbStatuses[dev].speedInitialized = TRUE;
            rgbStatuses[dev].speed = speedRGB;
            insertCanCommand(message, CMD_CAN_SET_RGB_SPEED);
            setCanRgbSpeed(message, speedRGB);
            setSourceCanAddress(message, rgbStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
    }
    return 0;
}


void setCanRgbAllValues(CANMessage* message, singleRGB values) {
    message->Data[OFFSET_CAN_RGB_VALUE] = (unsigned char) ((values.red >> 8) & 0x0f);
    message->Data[OFFSET_CAN_RGB_VALUE + 1] = (unsigned char) (values.red & 0xff);
    message->Data[OFFSET_CAN_RGB_VALUE + 2] = (unsigned char) ((values.green >> 4) & 0xff);
    values.green <<= 4;
    message->Data[OFFSET_CAN_RGB_VALUE + 3] = (unsigned char) ((values.green & 0xf0) | ((values.blue >> 8)& 0x0f));

    message->Data[OFFSET_CAN_RGB_VALUE + 4] = (unsigned char) (values.blue & 0xff);
    message->NoOfBytes += 5;

#ifdef DEBUG
    UART_SendNewLine("blue ");
    byte2ascii((unsigned char) (values.blue >> 8)& 0x0f, 1);
    byte2ascii((unsigned char) (values.blue & 0xff), 1);

    UART_SendNewLine("blue buf ");
    byte2ascii(message->Data[OFFSET_CAN_RGB_VALUE + 3], 1);
    byte2ascii(message->Data[OFFSET_CAN_RGB_VALUE + 4], 1);

#endif
}

void setCanRgbMode(CANMessage* message, unsigned char mode) {
    message->Data[OFFSET_CAN_RGB_VALUE] = mode;
    message->NoOfBytes++;
}

void setCanRgbSpeed(CANMessage* message, unsigned char speed) {
    message->Data[OFFSET_CAN_RGB_VALUE] = speed;
    message->NoOfBytes++;
}

void setCanRgbSingleValue(CANMessage* message, unsigned int rgbValue) {
    message->Data[OFFSET_CAN_RGB_VALUE] = (unsigned char) ((rgbValue >> 8) & 0x0f);
    message->Data[OFFSET_CAN_RGB_VALUE + 1] = (unsigned char) (rgbValue & 0xff);
    message->NoOfBytes += 2;
}

void uartSetMode(unsigned char mode) {
    UART_SendByte(REQ_FRAME_HDR);
    UART_SendByte(4);
    switch(mode) {
        case MODE_OFF:
            UART_SendByte(MODE_RGB_OFF);
            break;
        case MODE_SIMPLE:
            UART_SendByte(MODE_RGB_SIMPLE);
            break;
        case MODE_ALL_AUTO:
            UART_SendByte(MODE_RGB_ALL_AUTO);
            break;
        case MODE_RND:
            UART_SendByte(MODE_RGB_RND);
            break;
        case MODE_RND_ALL:
            UART_SendByte(MODE_RGB_RND_ALL);
            break;
    }

    UART_SendByte(REQ_FRAME_FTR);
}

void uartSetSpeed(unsigned char speed) {
    UART_SendByte(REQ_FRAME_HDR);
    UART_SendByte(5);
    UART_SendByte(MODE_RGB_SPEED);
    UART_SendByte(speed);
    UART_SendByte(REQ_FRAME_FTR);
}

void uartSetRed(unsigned int val) {
    UART_SendByte(REQ_FRAME_HDR);
    UART_SendByte(6);
    UART_SendByte(MODE_RGB_CH_R);
    UART_SendByte((val >> 8) & 0x0f);
    UART_SendByte((unsigned char)(val & 0xff));
    UART_SendByte(REQ_FRAME_FTR);
}

void uartSetGreen(unsigned int val) {
    UART_SendByte(REQ_FRAME_HDR);
    UART_SendByte(6);
    UART_SendByte(MODE_RGB_CH_G);
    UART_SendByte((val >> 8) & 0x0f);
    UART_SendByte((unsigned char)(val & 0xff));
    UART_SendByte(REQ_FRAME_FTR);
}

void uartSetBlue(unsigned int val) {
    UART_SendByte(REQ_FRAME_HDR);
    UART_SendByte(6);
    UART_SendByte(MODE_RGB_CH_B);
    UART_SendByte((val >> 8) & 0x0f);
    UART_SendByte((unsigned char)(val & 0xff));
    UART_SendByte(REQ_FRAME_FTR);
}

void uartSetRGB(singleRGB val) {
    UART_SendByte(REQ_FRAME_HDR);
    UART_SendByte(10);
    UART_SendByte(MODE_RGB_CH_ALL);
    UART_SendByte((val.red >> 8) & 0x0f);
    UART_SendByte((unsigned char)(val.red & 0xff));
    UART_SendByte((val.green >> 8) & 0x0f);
    UART_SendByte((unsigned char)(val.green & 0xff));
    UART_SendByte((val.blue >> 8) & 0x0f);
    UART_SendByte((unsigned char)(val.blue & 0xff));
    UART_SendByte(REQ_FRAME_FTR);
}

unsigned char switchMode() {
    
    switch(modeRGB) {
        case MODE_OFF:
            return MODE_ALL_AUTO;
        case MODE_ALL_AUTO:
            return MODE_RND_ALL;
        case MODE_RND_ALL:
            return MODE_RND;
        case MODE_RND:
            return MODE_SIMPLE;
        default:
            return MODE_OFF;
    }
   
}

void initDevStateInEeprom() {
    for (char i = 0; i < RGB_ADVANCE_ACTUATOR_QNTY; i++) {
        singleRGB vals = getRGB(i);
        eepromDevStat[i].values.red = vals.red;
        eepromDevStat[i].values.green = vals.green;
        eepromDevStat[i].values.blue = vals.blue;
        eepromDevStat[i].mode = getMode();
        eepromDevStat[i].speed = getSpeed();
        
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_MODE, eepromDevStat[i].mode);
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_SPEED, eepromDevStat[i].speed);
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED, (unsigned char)(eepromDevStat[i].values.red >> 8));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED + 1, (unsigned char)(eepromDevStat[i].values.red & 0xff));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN, (unsigned char)(eepromDevStat[i].values.green >> 8));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN + 1, (unsigned char)(eepromDevStat[i].values.green & 0xff));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE, (unsigned char)(eepromDevStat[i].values.blue >> 8));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE + 1, (unsigned char)(eepromDevStat[i].values.blue & 0xff));
    }
}

void updateDevStateInEeprom(unsigned char dev, unsigned char type, unsigned int val) {
    if (dev < RGB_ADVANCE_ACTUATOR_QNTY) {
        if (type == EEPROM_RGB_MODE) {
            eepromDevStat[dev].mode = (unsigned char)val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + type, (unsigned char)val);
        }
        if (type == EEPROM_RGB_SPEED) {
            eepromDevStat[dev].speed = (unsigned char)val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + type, (unsigned char)val);
        }
        if (type == EEPROM_RGB_RED) {
            eepromDevStat[dev].values.red = val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_RED, (unsigned char)(val >> 8));
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_RED + 1, (unsigned char)(val & 0xff));
        }
        if (type == EEPROM_RGB_GREEN) {
            eepromDevStat[dev].values.green = val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_GREEN, (unsigned char)(val >> 8));
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_GREEN + 1, (unsigned char)(val & 0xff));
        }
        if (type == EEPROM_RGB_BLUE) {
            eepromDevStat[dev].values.blue = val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_BLUE, (unsigned char)(val >> 8));
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_BLUE + 1, (unsigned char)(val & 0xff));
        }
    }
}

void loadDevStateFromEeprom() {
    for (char i = 0; i < RGB_ADVANCE_ACTUATOR_QNTY; i++) {
        eepromDevStat[i].values.red = (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED) << 8);
        eepromDevStat[i].values.red |= (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED + 1));
        setRed(eepromDevStat[i].values.red, i);
        
        eepromDevStat[i].values.green = (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN) << 8);
        eepromDevStat[i].values.green |= (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN + 1));
        setGreen(eepromDevStat[i].values.green, i);
        
        eepromDevStat[i].values.blue = (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE) << 8);
        eepromDevStat[i].values.blue |= (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE + 1));
        setBlue(eepromDevStat[i].values.blue, i);
        
        eepromDevStat[i].mode = eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_MODE);
        setMode(eepromDevStat[i].mode);
        
        eepromDevStat[i].speed = eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_SPEED);
        setSpeed(eepromDevStat[i].speed);
    }
}

#endif