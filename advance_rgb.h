/* 
 * File:   advance_rgb.h
 * Author: WinVirt
 *
 * Created on 31 lipiec 2016, 16:19
 */

#ifndef ADVANCE_RGB_H
#define	ADVANCE_RGB_H

#if RGB_ADVANCE_ACTUATOR_QNTY > 0

#include "comm_can_constans.h"
#include "utils.h"
#include <pic18.h>
#include "uart.h"


#define STRIPS_CNT  RGB_ADVANCE_ACTUATOR_QNTY

#define STRIP_1 0

#define PWM_UP  1
#define PWM_DOWN 2
#define PWM_STOP 0

#define MODE_OFF    0
#define MODE_ALL_AUTO 1
#define MODE_CHAIN_AUTO 2
#define MODE_RND        3
#define MODE_SIMPLE 4
#define MODE_RND_ALL 5

#define MODE_RGB_OFF        'z'
#define MODE_RGB_SPEED      's'
#define MODE_RGB_SIMPLE     'o'
#define MODE_RGB_ALL_AUTO   'a'
#define MODE_RGB_RND_ALL    'n'
#define MODE_RGB_RND        'd'
#define MODE_RGB_CH_R        'r'
#define MODE_RGB_CH_G        'g'
#define MODE_RGB_CH_B        'b'
#define MODE_RGB_CH_ALL      'f'


#define EEPROM_RGB_MODE     0
#define EEPROM_RGB_SPEED    1
#define EEPROM_RGB_RED      2
#define EEPROM_RGB_GREEN    4
#define EEPROM_RGB_BLUE     6

typedef struct {
    int red;
    int green;
    int blue;
} singleRGB;

unsigned char speedRGB = 2;
unsigned char modeRGB = MODE_OFF;

singleRGB rgbCurrent[RGB_ADVANCE_ACTUATOR_QNTY];

typedef void (*functionReceiver) (CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address);
typedef unsigned char (*functionTransmitter) (CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent);

typedef struct {
    unsigned char address;
    singleRGB values;
    unsigned char mode;
    unsigned char speed;
    unsigned char valueInitialized;
    unsigned char modeInitialized;
    unsigned char speedInitialized;
} rgbStatus;


typedef struct {
    singleRGB values;
    unsigned char mode;
    unsigned char speed;
} eepromStatus;


rgbStatus rgbStatuses[RGB_ADVANCE_ACTUATOR_QNTY];
eepromStatus eepromDevStat[RGB_ADVANCE_ACTUATOR_QNTY];

void initRgbActuatorPortIO();

void makeCommandForRgbActuator(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command, unsigned char address );
unsigned char readDeviceRgbActuator(CANMessage *outputMessage,  categoryStruct* categoryDevices, unsigned char timerEvent);
void makeBroadcastCommandForRgbActuator(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command);

void initRgbStatusesTable(  categoryStruct*  categoryDevices, rgbStatus *rgbStatuses);
unsigned char checkRgbValues(CANMessage *message,   categoryStruct* categoryDevices, rgbStatus *rgbStatuses);

void setMode(unsigned char mode);
void setRed(unsigned int value, signed char channel);
void setGreen(unsigned int value, signed char channel);
void setBlue(unsigned int value, signed char channel);
void setRGB(singleRGB values, signed char channel);
void setSpeed(unsigned char speed);
unsigned char switchMode();


unsigned char getSpeed();
unsigned char getMode();
unsigned int getRed(signed char channel);
unsigned int getGreen(signed char channel);
unsigned int getBlue(signed char channel);
singleRGB getRGB(signed char channel);


signed char getCanRgbStrip(   categoryStruct* categoryDevices, unsigned char address);
unsigned int getCanRgbSingleValue(CANMessage *message);
singleRGB getCanRgbAllValues(CANMessage *message);
unsigned char getCanRgbMode(CANMessage *message);
unsigned char getCanRgbSpeed(CANMessage *message);
void setCanRgbSingleValue(CANMessage *message, unsigned int rgbValue);
void setCanRgbAllValues(CANMessage *message, singleRGB values);
void setCanRgbMode(CANMessage *message, unsigned char mode);
void setCanRgbSpeed(CANMessage *message, unsigned char speed);

void resetRGB();


void uartSetMode(unsigned char mode);
void uartSetSpeed(unsigned char speed);
void uartSetRed(unsigned int val);
void uartSetGreen(unsigned int val);
void uartSetBlue(unsigned int val);
void uartSetRGB(singleRGB val);

void initDevStateInEeprom();
void updateDevStateInEeprom(unsigned char dev, unsigned char type, unsigned int val);
void loadDevStateFromEeprom();
void checkDevStatBeforeEepromUpdate();

#endif
#endif	/* ADVANCE_RGB_H */

