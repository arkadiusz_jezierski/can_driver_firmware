

#include "eeprom.h"

void initEEPROM() {
    unsigned long tout = 0xffff;
    while (EEPGD && tout--) {
        CLRWDT();
        continue;
    }
    tout = 0xffff;
    while (CFGS && tout--) {
        CLRWDT();
        continue;
    }
    tout = 0xffff;
    while (eepromRead(0) != 0 && tout--) {
        CLRWDT();
        continue;
    }
}

unsigned char eepromRead(unsigned int addr) {
    unsigned int tout = 0xffff;
    while (EECON1bits.WR && tout--) {
        CLRWDT();
        continue;
    }

    EEADRH = (addr >> 8) & 0xff;
    EEADR = addr & 0xff;
    EECON1 = 0x01;
    return EEDATA;
}

void eepromWrite(unsigned int addr, unsigned char data) {
    unsigned int tout = 0xffff;
    EEADRH = (addr >> 8) & 0xff;
    EEADR = addr & 0xff;
    EEDATA = data;
    GIE = 0;
    EECON1 = 0x04;
    EECON2 = 0x55;
    EECON2 = 0xAA;
    EECON1bits.WR = 1;
    GIE = 1;
    while (EECON1bits.WR == 1 && tout--) {
        CLRWDT();
    }

}

void loadEEPROMAddresses(unsigned char* addressTable, unsigned char size) {
    for (size_t i = 0; i < size; i++) {
        CLRWDT();
        addressTable[i] = eepromRead(OFFSET_EEPROM_ADDRESSES + i);
    }
}

void saveEEPROMAddresses(unsigned char* addressTable, unsigned char size) {
    for (size_t i = 0; i < size; i++) {
        eepromWrite(OFFSET_EEPROM_ADDRESSES + i, addressTable[i]);
    }
}



