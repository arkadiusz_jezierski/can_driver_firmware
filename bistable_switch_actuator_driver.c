/* 
 * File:   bistable_switch_actuator_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */

#include "comm_settings.h"

#if SWITCH_ACTUATOR_QNTY > 0

#include "bistable_switch_actuator_driver.h"
#ifdef DEBUG
#include "uart.h"
#endif

void makeCommandForBistableActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address) {

    if (address == 0) {
        makeBroadcastCommandForBistableActuator(outputMessage, inputMessage, categoryDevices, command);
        return;
    }

    signed char output;
    signed char state;
    
    switch (command) {
        case CMD_CAN_PING:
            insertCanCommand(outputMessage, CMD_CAN_PONG);
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            insertResetCounters(outputMessage);
            break;
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("ALL OFF");
#endif
            for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
                output = getCanOutputNumber(categoryDevices, categoryDevices->deviceAddress[dev]);
                if (output >= 0) {
#if PROXIMITY_SENSOR_ACTUATOR
                    outputRequestValue = 0;
#else
                    setOutputStatus(output, 0);
#endif
                }
            }
            break;


        case CMD_CAN_SET_OUTPUT:
#ifdef DEBUG
            UART_SendNewLine("SET");
#endif
            state = getCanOutputStatus(inputMessage);
            output = getCanOutputNumber(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) output, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (output >= 0 && state >= 0) {
#if PROXIMITY_SENSOR_ACTUATOR
                outputRequestValue = state;
#else
                setOutputStatus(output, state);
#endif
            }
#ifdef DEBUG
            UART_SendNewLine("LATA: ");
            byte2ascii(LATA, 1);
#endif
            break;
            
            
        case CMD_CAN_SET_OUTPUT_SWITCHED:
#ifdef DEBUG
            UART_SendNewLine("SET");
#endif
            output = getCanOutputNumber(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) output, 1);
#endif
            if (output >= 0) {
                switchOutputState(output);

            }
#ifdef DEBUG
            UART_SendNewLine("LATA: ");
            byte2ascii(LATA, 1);
#endif
            break;
            
        case CMD_CAN_SET_OUTPUT_RELAY_UNBLOCK:
#ifdef DEBUG
            UART_SendNewLine("SET");
#endif
            unblockNumber = getCanOutputUnblockNumber(inputMessage);
            unblockPin = getCanOutputNumber(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) output, 1);
#endif
            
#ifdef DEBUG
            UART_SendNewLine("LATA: ");
            byte2ascii(LATA, 1);
#endif
            break;
            
        case CMD_CAN_GET_OUTPUT_STATE:
#ifdef DEBUG
            UART_SendNewLine("GET");
#endif
            output = getCanOutputNumber(categoryDevices, address);
#if PROXIMITY_SENSOR_ACTUATOR
            state = outputRequestValue;
#else
            state = getOutputStatus(output);
#endif
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) output, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) state, 1);
#endif
            if (output >= 0 && state >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_OUTPUT_STATE);
                setCanOutputStatus(outputMessage, (unsigned char) state);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
    }
    
}

void makeBroadcastCommandForBistableActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command) {

    signed char output;
    signed char state;
    switch (command) {
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("BRCST ALL OFF");
#endif
            for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
                output = getCanOutputNumber(categoryDevices, categoryDevices->deviceAddress[dev]);
#ifdef DEBUG
                UART_SendNewLine("Output ");
                byte2ascii(output, 0);
                UART_SendString(" adr: ");
                byte2ascii(categoryDevices->deviceAddress[dev], 0);
#endif
                if (output >= 0) {
#if PROXIMITY_SENSOR_ACTUATOR
                    outputRequestValue = 0;
#else
                    setOutputStatus(output, 0);
#endif
                }
            }

            break;

        case CMD_CAN_SET_OUTPUT:
#ifdef DEBUG
            UART_SendNewLine("BRCST SET ALL");
#endif
            state = getCanOutputStatus(inputMessage);
            if (state >= 0) {
                for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
                    output = getCanOutputNumber(categoryDevices, categoryDevices->deviceAddress[dev]);
#ifdef DEBUG
                    UART_SendNewLine("Output ");
                    byte2ascii(output, 0);
                    UART_SendString(" adr: ");
                    byte2ascii(categoryDevices->deviceAddress[dev], 0);
#endif
                    if (output >= 0) {
#if PROXIMITY_SENSOR_ACTUATOR
                        outputRequestValue = 0;
#else
                        setOutputStatus(output, state);
#endif
                    }
                }
            }
            break;

    }
}

unsigned char readDeviceBistableActuator(CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent) {

    
    static unsigned char initialize = 1;
    if (initialize || initCategory()) {
        //                initPingTable(&pingInfo);
        initDeviceStatusesTable(categoryDevices, &deviceStatuses);
        initialize = 0;
    }

    if (timerEvent) {
        if (unblockNumber > 0) {
            counterTimer++;
            if (counterTimer > 3) {
                counterTimer = 0;
                unblockNumber--;
                switchOutputState(unblockPin);
            }

        }
        
        //        checkPing(&pingInfo);
        checkDevStatBeforeEepromUpdate();
        if (checkDeviceStatuses(outputMessage, categoryDevices, &deviceStatuses))
            return 1;
    }
    


    return 0;
}


void checkDevStatBeforeEepromUpdate() {
    char changed = 0;
    static char timerCounter = 0;
    for (char i = 0; i < SWITCH_ACTUATOR_QNTY; i++) {
        if (eepromStatuses[i] != getOutputStatus(i)) {
            changed = 1;
            break;
        }
    }
    
    if (changed) {
        timerCounter++;
        if (timerCounter > 220) {
            timerCounter = 0;
            
            for (char i = 0; i < SWITCH_ACTUATOR_QNTY; i++) {
                signed char devStat = getOutputStatus(i);
                if (eepromStatuses[i] != devStat) {
                    updateDevStateInEeprom(i, devStat);
                }
            }
            
        }
    } else {
        timerCounter = 0;
    }
}


signed char getCanOutputNumber(categoryStruct* categoryDevices, unsigned char address) {
    //    UART_SendNewLine("dev qnty: ");
    //    byte2ascii(categoryDevices->devicesQnt, 1);
    //    UART_SendNewLine("adr: ");
    //    byte2ascii(address, 1);
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        //          UART_SendNewLine("chk adr: ");
        //    byte2ascii(categoryDevices->deviceAddress[dev], 1);
        if (categoryDevices->deviceAddress[dev] == address) {
            return dev;
        }
    }
    return -1;
}

signed char getCanOutputStatus(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_OUTPUT_STAT) {
        return message->Data[OFFSET_CAN_OUTPUT_STAT];
    } else {
        return -1;
    }
}

signed char getCanOutputUnblockNumber(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_UNBLOCK_NMB) {
        return message->Data[OFFSET_CAN_UNBLOCK_NMB];
    } else {
        return -1;
    }
}

void setCanOutputStatus(CANMessage* message, unsigned char state) {
    message->Data[OFFSET_CAN_OUTPUT_STAT] = state;
    message->NoOfBytes++;
}

void initDeviceStatusesTable(categoryStruct* categoryDevices, deviceStatus *deviceStatuses) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {

        deviceStatuses[dev].address = categoryDevices->deviceAddress[dev];
#if PROXIMITY_SENSOR_ACTUATOR
        deviceStatuses[dev].state = outputRequestValue;
#else
        deviceStatuses[dev].stateInitialized = FALSE;
#endif
#ifdef DEBUG
        UART_SendNewLine("State adr ");
        byte2ascii(deviceStatuses[dev].address, 1);
        UART_SendString(" stat: ");
        byte2ascii(deviceStatuses[dev].state, 1);
        UART_SendString(" dev adr: ");
        byte2ascii(categoryDevices->deviceAddress[dev], 1);
#endif
    }
}

unsigned char checkDeviceStatuses(CANMessage *message, categoryStruct* categoryDevices, deviceStatus *deviceStatuses) {
    unsigned char state;
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
#if PROXIMITY_SENSOR_ACTUATOR
        state = outputRequestValue;
#else
        state = getOutputStatus(dev);
#endif
        //        UART_SendNewLine("State id ");
        //        byte2ascii(dev, 1);
        //        UART_SendString(" stat: ");
        //        byte2ascii(state, 1);
        //        UART_SendString(" last: ");
        //        byte2ascii(deviceStatuses[dev].state, 1);
        if (deviceStatuses[dev].state != state || deviceStatuses[dev].stateInitialized == FALSE) {
            deviceStatuses[dev].stateInitialized = TRUE;
            deviceStatuses[dev].state = state;
            insertCanCommand(message, CMD_CAN_SET_OUTPUT);
            setCanOutputStatus(message, (unsigned char) state);
            setSourceCanAddress(message, deviceStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
    }
    return 0;
}

void initBistableSwitchActuatorPortIO() {
    ADCON1bits.PCFG = 0x0f;
#if PROXIMITY_SENSOR_ACTUATOR
    ACTUATOR_DIR &= ~(1 << ACTUATOR_PIN_1);
    ACTUATOR_PORT &= ~(1 << ACTUATOR_PIN_1);
#else
#if (SWITCH_ACTUATOR_QNTY == 5)
    ACTUATOR_DIR &= ~((1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3) | (1 << ACTUATOR_PIN_4));
#else
    ACTUATOR_DIR &= ~((1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3));
    ACTUATOR_DIR_B &= ~(1 << ACTUATOR_PIN_4);
#endif

#if (SWITCH_ACTUATOR_QNTY == 5)
    ACTUATOR_PORT &= ~((1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3) | (1 << ACTUATOR_PIN_4));
    ACTUATOR_PORT2 &= ~((1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3) | (1 << ACTUATOR_PIN_4));
#else
    ACTUATOR_PORT &= ~((1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3));
    ACTUATOR_PORT_B &= ~(1 << ACTUATOR_PIN_4);
    ACTUATOR_PORT2 &= ~((1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3));
    ACTUATOR_PORT2_B &= ~(1 << ACTUATOR_PIN_4);
#endif

#if (SWITCH_ACTUATOR_QNTY == 5)
    ACTUATOR_DIR_B &= ~(1 << ACTUATOR_PIN_5);
    ACTUATOR_PORT_B &= ~(1 << ACTUATOR_PIN_5);
#endif

#endif
//    testBistableSwitchActuatorPortIO();
    
    if (startupInfo == RESET_BY_WDT || startupInfo == RESET_BY_RESET_INSTRUCTION) {
        loadDevStateFromEeprom();
    } else {
#ifdef POWER_ON_START
    #if PROXIMITY_SENSOR_ACTUATOR
  
    ACTUATOR_1_ON
    
    #else
    ACTUATOR_1_ON
    ACTUATOR_2_ON
    ACTUATOR_3_ON
    ACTUATOR_4_ON
        #if (SWITCH_ACTUATOR_QNTY == 5)
    ACTUATOR_5_ON
        #endif

    #endif
    
#else
    
    #if PROXIMITY_SENSOR_ACTUATOR
  
    ACTUATOR_1_OFF
    
    #else
    ACTUATOR_1_OFF
    ACTUATOR_2_OFF
    ACTUATOR_3_OFF
    ACTUATOR_4_OFF
        #if (SWITCH_ACTUATOR_QNTY == 5)
    ACTUATOR_5_OFF
        #endif

    #endif

#endif
        initDevStateInEeprom();
    }

}

void testBistableSwitchActuatorPortIO() {

#if PROXIMITY_SENSOR_ACTUATOR
  
    ACTUATOR_1_ON
    delay_ms(200);
    ACTUATOR_1_OFF
    
#else
    ACTUATOR_1_ON
    delay_ms(200);
    ACTUATOR_1_OFF
    ACTUATOR_2_ON
    delay_ms(200);
    ACTUATOR_2_OFF
    ACTUATOR_3_ON
    delay_ms(200);
    ACTUATOR_3_OFF
    ACTUATOR_4_ON
    delay_ms(200);
    ACTUATOR_4_OFF
#if (SWITCH_ACTUATOR_QNTY == 5)
    ACTUATOR_5_ON
    delay_ms(200);
    ACTUATOR_5_OFF
#endif

#endif
}

signed char getOutputStatus(unsigned char pinNo) {
#if PROXIMITY_SENSOR_ACTUATOR
    if (pinNo < SWITCH_ACTUATOR_QNTY) {
        switch (pinNo) {
            case 0: return ((ACTUATOR_STATE_1) > 0) ? 1: 0;
        }
    }
#else
    if (pinNo < SWITCH_ACTUATOR_QNTY) {
        switch (pinNo) {
            case 0: return ((ACTUATOR_STATE_1) > 0) ? 1: 0;
            case 1: return ((ACTUATOR_STATE_2) > 0) ? 1: 0;
            case 2: return ((ACTUATOR_STATE_3) > 0) ? 1: 0;
            case 3: return ((ACTUATOR_STATE_4) > 0) ? 1: 0;
#if (SWITCH_ACTUATOR_QNTY == 5)
            case 4: return ((ACTUATOR_STATE_5) > 0) ? 1: 0;
#endif
        }

    }
#endif
    return -1;
}

void setOutputStatus(unsigned char pinNo, unsigned char status) {
#if PROXIMITY_SENSOR_ACTUATOR
    if (pinNo < SWITCH_ACTUATOR_QNTY) {
        if (status) {
            switch (pinNo) {
                case 0: ACTUATOR_1_ON;
                    break;
            }
        } else {
            switch (pinNo) {
                case 0: ACTUATOR_1_OFF;
                    break;
            }
        }
    }
#else
    if (pinNo < SWITCH_ACTUATOR_QNTY) {
        if (status) {
            switch (pinNo) {
                case 0: ACTUATOR_1_ON;
                    break;
                case 1:
                    ACTUATOR_2_ON;
                    break;
                case 2:
                    ACTUATOR_3_ON;
                    break;
                case 3:
                    ACTUATOR_4_ON;
                    break;
#if (SWITCH_ACTUATOR_QNTY == 5)
                case 4: ACTUATOR_5_ON;
                    break;
#endif
            }
        } else {
            switch (pinNo) {
                case 0: ACTUATOR_1_OFF;
                    break;
                case 1:
                    ACTUATOR_2_OFF;
                    break;
                case 2:
                    ACTUATOR_3_OFF;
                    break;
                case 3:
                    ACTUATOR_4_OFF;
                    break;
#if (SWITCH_ACTUATOR_QNTY == 5)
                case 4: ACTUATOR_5_OFF;
                    break;
#endif
            }
        }
    }
#endif
}

void switchOutputState(unsigned char pinNumber) {
#if PROXIMITY_SENSOR_ACTUATOR
    if (pinNumber < SWITCH_ACTUATOR_QNTY) {
        if (outputRequestValue == 1) {
            outputRequestValue = 0;
        } else {
            outputRequestValue = 1;
        }
    }
#else
    if (pinNumber < SWITCH_ACTUATOR_QNTY) {
        switch (pinNumber) {
            case 0: ACTUATOR_1_TG;
                break;
            case 1:
                ACTUATOR_2_TG;
                break;
            case 2:
                ACTUATOR_3_TG;
                break;
            case 3:
                ACTUATOR_4_TG;
                break;
#if (SWITCH_ACTUATOR_QNTY == 5)
            case 4: ACTUATOR_5_TG;
                break;
#endif
        }
    }
#endif
}

void initDevStateInEeprom() {
    for (char i = 0; i < SWITCH_ACTUATOR_QNTY; i++) {
        signed char devStat = getOutputStatus(i);
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i, devStat);
        eepromStatuses[i] = devStat;
    }
}

void updateDevStateInEeprom(unsigned char dev, unsigned char val) {
    eepromStatuses[dev] = val;
    eepromWrite(OFFSET_EEPROM_DEV_STATE + dev, val);
}

void loadDevStateFromEeprom() {
    for (char i = 0; i < SWITCH_ACTUATOR_QNTY; i++) {
        eepromStatuses[i] = eepromRead(OFFSET_EEPROM_DEV_STATE + i);
        setOutputStatus(i, eepromStatuses[i]);
    }
}

#endif