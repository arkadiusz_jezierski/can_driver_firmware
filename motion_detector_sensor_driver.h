/* 
 * File:   bistable_switch_actuator_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */




#ifndef MOTION_DETECTOR_SENSOR_DRIVER_H
#define	MOTION_DETECTOR_SENSOR_DRIVER_H

#if MOTION_DETECTOR_QNTY > 0

#include "comm_can_constans.h"
#include "utils.h"
#include <pic18.h>



#define OUTPUT_DIR       TRISA
#define OUTPUT_PORT      LATA
#define OUTPUT_PORT_STATE      PORTA
#define OUTPUT_SENS_1_PIN     4
#define OUTPUT_SENS_2_PIN     3
#define OUTPUT_LED_PIN        2

#define INPUT_DIR       TRISA
#define INPUT_PORT      PORTA
#define INPUT_TMP_PIN           1
#define INPUT_MOTION_PIN        0

#define OUTPUT_SENS_1_ON    (OUTPUT_PORT |= (1 << OUTPUT_SENS_1_PIN));
#define OUTPUT_SENS_1_OFF   (OUTPUT_PORT &= ~(1 << OUTPUT_SENS_1_PIN));
#define OUTPUT_SENS_2_ON    (OUTPUT_PORT |= (1 << OUTPUT_SENS_2_PIN));
#define OUTPUT_SENS_2_OFF   (OUTPUT_PORT &= ~(1 << OUTPUT_SENS_2_PIN));
#define OUTPUT_LED_ON       (OUTPUT_PORT |= (1 << OUTPUT_LED_PIN));
#define OUTPUT_LED_OFF      (OUTPUT_PORT &= ~(1 << OUTPUT_LED_PIN));

#define OUTPUT_SENS_1_STATE (OUTPUT_PORT_STATE & (1 << OUTPUT_SENS_1_PIN))
#define OUTPUT_SENS_2_STATE (OUTPUT_PORT_STATE & (1 << OUTPUT_SENS_2_PIN))
#define OUTPUT_LED_STATE    (OUTPUT_PORT_STATE & (1 << OUTPUT_LED_PIN))

#define SENSOR_TMP_STATE    INPUT_PORT & (1 << INPUT_TMP_PIN)
#define SENSOR_MOTION_STATE INPUT_PORT & (1 << INPUT_MOTION_PIN)

#define EEPROM_LED_POWER        0
#define EEPROM_SENSITIVITY      1 
#define EEPROM_TEMPER_STATUS    2


volatile unsigned char outputRequestValue = 0;

unsigned char eepromLedPower;
unsigned char eepromSensitivity;


typedef void (*functionReceiver) (CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address);
typedef unsigned char (*functionTransmitter) (CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent);


typedef struct {
    unsigned char address;
    unsigned char ledPower;
    unsigned char sensitivity;
    unsigned char motionStatus;
    unsigned char temperStatus;
    unsigned char ledPowerInitialized;
    unsigned char sensitivityInitialized;
    unsigned char motionStatusInitialized;
    unsigned char temperStatusInitialized;
} deviceStatus;

deviceStatus deviceStatuses[MOTION_DETECTOR_QNTY];


void makeCommandForMotionDetector(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command, unsigned char address );
unsigned char readDeviceMotionDetector(CANMessage *outputMessage,  categoryStruct* categoryDevices, unsigned char timerEvent);
void makeBroadcastCommandForMotionDetector(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command);

signed char getMotionDetectorNumber(   categoryStruct* categoryDevices, unsigned char address);
signed char getCanMotionDetectorSensitivity(CANMessage *message);
signed char getCanMotionDetectorLedPower(CANMessage *message);

void setCanSensitivity(CANMessage *message, unsigned char level);
void setCanLedPower(CANMessage *message, unsigned char state);
void setCanMotionDetectorStatus(CANMessage *message, unsigned char state);
void setCanMotionDetectorTmpStatus(CANMessage *message, unsigned char state);


void initDeviceStatusesTable(  categoryStruct*  categoryDevices, deviceStatus *deviceStatuses);
unsigned char checkDeviceStatuses(CANMessage *message,   categoryStruct* categoryDevices, deviceStatus *deviceStatuses);


void initMotionDetectorPortIO();

signed char getSensitivity(unsigned char devNmb);
signed char getLedPower(unsigned char devNmb);
signed char getMotionDetectorStatus(unsigned char devNmb);
signed char getMotionDetectorTmpStatus(unsigned char devNmb);
void setSensitivity(unsigned char devNmb, unsigned char level);
void setLedPower(unsigned char devNmb, unsigned char state);

void initDevStateInEeprom();
void updateDevStateInEeprom(unsigned char type, unsigned char val);
void loadDevStateFromEeprom();
void checkDevStatBeforeEepromUpdate();

#endif

#endif	/* MOTION_DETECTOR_SENSOR_DRIVER_H */

