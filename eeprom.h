/* 
 * File:   eeprom.h
 * Author: Jezierski
 *
 * Created on 13 czerwiec 2013, 23:06
 */

#ifndef EEPROM_H
#define	EEPROM_H

#include  <pic18.h>
#include <string.h>

#define OFFSET_EEPROM_STATUS        0x00
#define OFFSET_EEPROM_INIT_UID      0x01
#define OFFSET_EEPROM_RESET_CNT     0x04
#define OFFSET_EEPROM_WDT_CNT       0x06
#define OFFSET_EEPROM_ADDRESSES     0x10
#define OFFSET_EEPROM_DEV_STATE     0x100

#define STATUS_BIT_MASK_INIT_UID       0x01
#define STATUS_BIT_MASK_ADR_READY       0x02



void loadEEPROMAddresses(unsigned char * addressTable, unsigned char size);
void saveEEPROMAddresses(unsigned char * addressTable, unsigned char size);

unsigned char eepromRead(unsigned int addr);
void eepromWrite(unsigned int addr, unsigned char data);

void initEEPROM(void);


#endif	/* EEPROM_H */

