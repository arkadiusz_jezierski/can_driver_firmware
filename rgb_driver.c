/*
 * File:  rgb_actuator_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */

#include "comm_settings.h"

#if RGB_ACTUATOR_QNTY > 0

#include "rgb_driver.h"
#ifdef DEBUG
#include "uart.h"
#endif
#include <../sources/plib/SPI/spi_open.c>

void makeCommandForRgbActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address) {

    if (address == 0) {
        makeBroadcastCommandForRgbActuator(outputMessage, inputMessage, categoryDevices, command);
        return;
    }

    signed char channel;
    unsigned int value;
    rgb values;
    switch (command) {
        case CMD_CAN_PING:
            insertCanCommand(outputMessage, CMD_CAN_PONG);
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            insertResetCounters(outputMessage);
            break;
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("ALL OFF");
#endif
            setMode(MODE_OFF);
            break;

        case CMD_CAN_SET_RGB_CHANNEL_RED:
#ifdef DEBUG
            UART_SendNewLine("SET rgb red");
#endif
            value = getCanRgbSingleValue(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setRed(value, channel);
            }
            break;

        case CMD_CAN_SET_RGB_CHANNEL_GREEN:
#ifdef DEBUG
            UART_SendNewLine("SET rgb green");
#endif
            value = getCanRgbSingleValue(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setGreen(value, channel);
            }
            break;

        case CMD_CAN_SET_RGB_CHANNEL_BLUE:
#ifdef DEBUG
            UART_SendNewLine("SET rgb blue");
#endif
            value = getCanRgbSingleValue(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setBlue(value, channel);
            }
            break;

        case CMD_CAN_SET_RGB_CHANNEL_ALL:
#ifdef DEBUG
            UART_SendNewLine("SET rgb all");
#endif
            values = getCanRgbAllValues(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("red ");
            byte2ascii((unsigned char) values.red, 1);
            UART_SendNewLine("green ");
            byte2ascii((unsigned char) values.green, 1);
            UART_SendNewLine("blue ");
            byte2ascii((unsigned char) values.blue, 1);
#endif
            if (channel >= 0) {
                setRGB(values, channel);
            }
            break;

        case CMD_CAN_SET_RGB_MODE:
#ifdef DEBUG
            UART_SendNewLine("SET rgb mode");
#endif
            value = getCanRgbMode(inputMessage);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            
            setMode(value);
            
            break;
        case CMD_CAN_SWITCH_MODE:
            setMode(getNextMode());
            
            break;
        case CMD_CAN_SET_RGB_SPEED:
#ifdef DEBUG
            UART_SendNewLine("SET rgb speed");
#endif
            value = getCanRgbSpeed(inputMessage);
            channel = getCanRgbStrip(categoryDevices, address);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                setSpeed(value, channel);
            }
            break;

        case CMD_CAN_GET_RGB_CHANNEL_RED:
#ifdef DEBUG
            UART_SendNewLine("GET rgb red");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getRed(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_RED);
                setCanRgbSingleValue(outputMessage, value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_CHANNEL_GREEN:
#ifdef DEBUG
            UART_SendNewLine("GET rgb green");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getGreen(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_GREEN);
                setCanRgbSingleValue(outputMessage, value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_CHANNEL_BLUE:
#ifdef DEBUG
            UART_SendNewLine("GET rgb blue");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getBlue(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_BLUE);
                setCanRgbSingleValue(outputMessage, value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_CHANNEL_ALL:
#ifdef DEBUG
            UART_SendNewLine("GET rgb all");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            values = getRGB(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("red ");
            byte2ascii((unsigned char) values.red, 1);
            UART_SendNewLine("green ");
            byte2ascii((unsigned char) values.green, 1);
            UART_SendNewLine("blue ");
            byte2ascii((unsigned char) values.blue, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_CHANNEL_ALL);
                setCanRgbAllValues(outputMessage, values);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_MODE:
#ifdef DEBUG
            UART_SendNewLine("GET rgb mode");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getMode(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_MODE);
                setCanRgbMode(outputMessage, (unsigned char) value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;

        case CMD_CAN_GET_RGB_SPEED:
#ifdef DEBUG
            UART_SendNewLine("GET rgb speed");
#endif
            channel = getCanRgbStrip(categoryDevices, address);
            value = getSpeed(channel);
#ifdef DEBUG
            UART_SendNewLine("out ");
            byte2ascii((unsigned char) channel, 1);
            UART_SendNewLine("stat ");
            byte2ascii((unsigned char) value, 1);
#endif
            if (channel >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_RGB_SPEED);
                setCanRgbSpeed(outputMessage, (unsigned char) value);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
    }
}

unsigned char readDeviceRgbActuator(CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent) {


    static unsigned char initialize = 1;
    if (initialize || initCategory()) {
        //                initPingTable(&pingInfo);
        initRgbStatusesTable(categoryDevices, &rgbStatuses);
        initialize = 0;
    }
    
    if (timerEvent) {
        checkDevStatBeforeEepromUpdate();
        if (checkRgbValues(outputMessage, categoryDevices, &rgbStatuses))
            return 1;
    }
    return 0;
}

void handleIRSupport() {
    setMode(getNextMode());
}


void checkDevStatBeforeEepromUpdate() {
    char changed = 0;
    static char timerCounter = 0;
    
    for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
        rgb values = getRGB(i);
        if (eepromDevStat[i].values.red != values.red
                || eepromDevStat[i].values.green != values.green
                || eepromDevStat[i].values.blue != values.blue
                || eepromDevStat[i].mode != getMode(i)
                || eepromDevStat[i].speed != getSpeed(i)) {
            changed = 1;
            break;
        }
    }
    
    if (changed) {
        timerCounter++;
        if (timerCounter > 220) {
            timerCounter = 0;
            
            for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
                rgb values = getRGB(i);
                unsigned char mode = getMode(i);
                unsigned char speed = getSpeed(i);

                if (eepromDevStat[i].values.red != values.red) {
                    updateDevStateInEeprom(i, EEPROM_RGB_RED , values.red);
                }
                if (eepromDevStat[i].values.green != values.green) {
                    updateDevStateInEeprom(i, EEPROM_RGB_GREEN , values.green);
                }
                if (eepromDevStat[i].values.blue != values.blue) {
                    updateDevStateInEeprom(i, EEPROM_RGB_BLUE , values.blue);
                }
                if (eepromDevStat[i].mode != mode) {
                    updateDevStateInEeprom(i, EEPROM_RGB_MODE , mode);
                }
                if (eepromDevStat[i].speed != speed) {
                    updateDevStateInEeprom(i, EEPROM_RGB_SPEED, speed);
                }
            }
            
        }
    } else {
        timerCounter = 0;
    }
}

void makeBroadcastCommandForRgbActuator(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command) {

//    unsigned char value;
    switch (command) {
        case CMD_CAN_ALL_OFF:
#ifdef DEBUG
            UART_SendNewLine("BRCST ALL OFF");
#endif
            setMode(MODE_OFF);
            break;

    }
}

signed char getCanRgbStrip(categoryStruct* categoryDevices, unsigned char address) {
#ifdef DEBUG
    UART_SendNewLine("dev qnty: ");
    byte2ascii(categoryDevices->devicesQnt, 1);
    UART_SendNewLine("adr: ");
    byte2ascii(address, 1);
#endif
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
#ifdef DEBUG
        UART_SendNewLine("chk adr: ");
        byte2ascii(categoryDevices->deviceAddress[dev], 1);
#endif
        if (categoryDevices->deviceAddress[dev] == address) {
            return dev;
        }
    }
    return -1;
}

unsigned int getCanRgbSingleValue(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE + 1) {
        unsigned int value = message->Data[OFFSET_CAN_RGB_VALUE] & 0x0f;
        value << = 8;
        value += message->Data[OFFSET_CAN_RGB_VALUE + 1];

        return value;
    }
    return 0;
}

rgb getCanRgbAllValues(CANMessage *message) {
    rgb values;

    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE + 4) {
        values.red = message->Data[OFFSET_CAN_RGB_VALUE] & 0x0f;
        values.red << = 8;
        values.red += message->Data[OFFSET_CAN_RGB_VALUE + 1];
        values.green = message->Data[OFFSET_CAN_RGB_VALUE + 2];
        values.green << = 4;
        values.green |= (message->Data[OFFSET_CAN_RGB_VALUE + 3] >> 4) & 0x0f;
        values.blue = message->Data[OFFSET_CAN_RGB_VALUE + 3] & 0x0f;
        values.blue << = 8;
        values.blue += message->Data[OFFSET_CAN_RGB_VALUE + 4];
    } else {
        values.red = 0;
        values.green = 0;
        values.blue = 0;
    }
    return values;
}

unsigned char getCanRgbMode(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE) {
        return message->Data[OFFSET_CAN_RGB_VALUE];
    }
    return 0;
}

unsigned char getCanRgbSpeed(CANMessage *message) {
    if (message->NoOfBytes > OFFSET_CAN_RGB_VALUE) {
        return message->Data[OFFSET_CAN_RGB_VALUE];
    }
    return 0;
}

void setCanRgbSingleValue(CANMessage* message, unsigned int rgbValue) {
    message->Data[OFFSET_CAN_RGB_VALUE] = (unsigned char) ((rgbValue >> 8) & 0x0f);
    message->Data[OFFSET_CAN_RGB_VALUE + 1] = (unsigned char) (rgbValue & 0xff);
    message->NoOfBytes += 2;
}

void setCanRgbAllValues(CANMessage* message, rgb values) {
    message->Data[OFFSET_CAN_RGB_VALUE] = (unsigned char) ((values.red >> 8) & 0x0f);
    message->Data[OFFSET_CAN_RGB_VALUE + 1] = (unsigned char) (values.red & 0xff);
    message->Data[OFFSET_CAN_RGB_VALUE + 2] = (unsigned char) ((values.green >> 4) & 0xff);
    values.green <<= 4;
    message->Data[OFFSET_CAN_RGB_VALUE + 3] = (unsigned char) ((values.green & 0xf0) | ((values.blue >> 8)& 0x0f));

    message->Data[OFFSET_CAN_RGB_VALUE + 4] = (unsigned char) (values.blue & 0xff);
    message->NoOfBytes += 5;

#ifdef DEBUG
    UART_SendNewLine("blue ");
    byte2ascii((unsigned char) (values.blue >> 8)& 0x0f, 1);
    byte2ascii((unsigned char) (values.blue & 0xff), 1);

    UART_SendNewLine("blue buf ");
    byte2ascii(message->Data[OFFSET_CAN_RGB_VALUE + 3], 1);
    byte2ascii(message->Data[OFFSET_CAN_RGB_VALUE + 4], 1);

#endif
}

void setCanRgbMode(CANMessage* message, unsigned char mode) {
    message->Data[OFFSET_CAN_RGB_VALUE] = mode;
    message->NoOfBytes++;
}

void setCanRgbSpeed(CANMessage* message, unsigned char speed) {
    message->Data[OFFSET_CAN_RGB_VALUE] = speed;
    message->NoOfBytes++;
}

void initRgbStatusesTable(categoryStruct* categoryDevices, rgbStatus *rgbStatuses) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {

        rgbStatuses[dev].address = categoryDevices->deviceAddress[dev];
        rgbStatuses[dev].valueInitialized = FALSE;
        rgbStatuses[dev].modeInitialized = FALSE;
        rgbStatuses[dev].speedInitialized = FALSE;
    }
}

unsigned char checkRgbValues(CANMessage *message, categoryStruct* categoryDevices, rgbStatus *rgbStatuses) {
    rgb values;
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        values = getRGB(dev);
#ifdef DEBUG
        UART_SendNewLine("channel ");
        byte2ascii((unsigned char) dev, 1);
        UART_SendNewLine("rgbCurrent red ");
        byte2ascii((unsigned char) values.red, 1);
        UART_SendNewLine("rgbStatuses red ");
        byte2ascii((unsigned char) rgbStatuses[dev].values.red, 1);
#endif
        if (rgbStatuses[dev].values.red != values.red
            || rgbStatuses[dev].values.green != values.green
            || rgbStatuses[dev].values.blue != values.blue
            || rgbStatuses[dev].valueInitialized == FALSE) {
            rgbStatuses[dev].valueInitialized = TRUE;
            rgbStatuses[dev].values.red = values.red;
            rgbStatuses[dev].values.green = values.green;
            rgbStatuses[dev].values.blue = values.blue;
            insertCanCommand(message, CMD_CAN_SET_RGB_CHANNEL_ALL);
            setCanRgbAllValues(message, values);
            setSourceCanAddress(message, rgbStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
        if (rgbStatuses[dev].mode != modeRGB || rgbStatuses[dev].modeInitialized == FALSE) {
            rgbStatuses[dev].modeInitialized = TRUE;
            rgbStatuses[dev].mode = modeRGB;
            insertCanCommand(message, CMD_CAN_SET_RGB_MODE);
            setCanRgbMode(message, modeRGB);
            setSourceCanAddress(message, rgbStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
        if (rgbStatuses[dev].speed != speedRGB || rgbStatuses[dev].speedInitialized == FALSE) {
            rgbStatuses[dev].speedInitialized = TRUE;
            rgbStatuses[dev].speed = speedRGB;
            insertCanCommand(message, CMD_CAN_SET_RGB_SPEED);
            setCanRgbSpeed(message, speedRGB);
            setSourceCanAddress(message, rgbStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
    }
    return 0;
}

void initRgbActuatorPortIO() {
    ADCON1bits.PCFG = 0x0f;

    LED_DIR &= ~((1 << PIN_BLANK) | (1 << PIN_XLAT));
    LED_PORT |= (1 << PIN_BLANK);
    LED_PORT &= ~(1 << PIN_XLAT);

    OpenSPI(0, 0, 0);
    
    
    resetRGB(rgbCurrentManual);
    writeRGB(rgbCurrentManual);
        
    configureAutoRGBTimer();
    stopAutoRGBTimer();
    
    if (startupInfo == RESET_BY_WDT || startupInfo == RESET_BY_RESET_INSTRUCTION) {
        loadDevStateFromEeprom();
    } else {
        initDevStateInEeprom();
    }
}

void setRed(unsigned int value, signed char channel) {

    if (channel < RGB_ACTUATOR_QNTY) {
        rgbCurrentManual[channel].red = value;
        if (modeRGB == MODE_MANUAL) {
            writeRGB(rgbCurrentManual);
        }
    }
}

void setGreen(unsigned int value, signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        rgbCurrentManual[channel].green = value;
        if (modeRGB == MODE_MANUAL) {
            writeRGB(rgbCurrentManual);
        }
    }
}

void setBlue(unsigned int value, signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        rgbCurrentManual[channel].blue = value;
        if (modeRGB == MODE_MANUAL) {
            writeRGB(rgbCurrentManual);
        }
    }
}

void setRGB(rgb values, signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        rgbCurrentManual[channel].red = values.red;
        rgbCurrentManual[channel].green = values.green;
        rgbCurrentManual[channel].blue = values.blue;
        if (modeRGB == MODE_MANUAL) {
            writeRGB(rgbCurrentManual);
        }
    }
}

void setMode(unsigned char mode) {
    if (mode == modeRGB) {
        return;
    }
    modeRGB = (char) mode;
    singleRGB tmpRGB[RGB_ACTUATOR_QNTY];
    switch (mode) {

        case MODE_ALL_AUTO:     //1
            startAutoRGBTimer();
            resetRGB(rgbCurrent);
            step = 0;
            break;
         case MODE_RND:      //3
            startAutoRGBTimer();
            for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
                for (char col = 0; col < 3; col++) {
                    color[col][i] = 0;
                    nextColor[col][i] = 0;
                }
            }
            break;
            
        case MODE_RND_ALL:      //5
            startAutoRGBTimer();
            for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
                for (char col = 0; col < 3; col++) {
                    color[col][i] = 0;
                    nextColor[col][i] = 0;
                }
            }
            break;

        case MODE_CHAIN_AUTO:   //2
            startAutoRGBTimer();
#if RGB_ACTUATOR_QNTY > 1
            step = 0;
            resetRGB(rgbCurrent);
#endif
            break;
        case MODE_OFF:      //0
            stopAutoRGBTimer();
            for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
                tmpRGB[i].red = 0;
                tmpRGB[i].green = 0;
                tmpRGB[i].blue = 0;
            }
            writeRGB(tmpRGB);
            break;
        case MODE_MANUAL:       //4
            stopAutoRGBTimer();
            writeRGB(rgbCurrentManual);
            break;
    }
}

void setSpeed(unsigned char speed, signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        speedRGB = (char) speed;
    }
}

unsigned char getSpeed(signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        return (unsigned char) speedRGB;
    }
    return 0;
}

unsigned char getMode(signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        return (unsigned char) modeRGB;
    }
    return 0;
}

rgb getRGB(signed char channel) {
    rgb values;
    if (channel < RGB_ACTUATOR_QNTY) {
        values.red = rgbCurrentManual[channel].red;
        values.green = rgbCurrentManual[channel].green;
        values.blue = rgbCurrentManual[channel].blue;
    } else {
        values.red = values.green = values.blue = 0;
    }
    return values;
}

unsigned int getRed(signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        return rgbCurrentManual[channel].red;
    }
    return 0;
}

unsigned int getGreen(signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        return rgbCurrentManual[channel].green;
    }
    return 0;
}

unsigned int getBlue(signed char channel) {
    if (channel < RGB_ACTUATOR_QNTY) {
        return rgbCurrentManual[channel].blue;
    }
    return 0;
}

void writeToBuff(unsigned int value, unsigned char *buffer, char *offset, char *tmpBufEmpty, unsigned char *tmpBuf) {
    if (*tmpBufEmpty) {
        *(buffer + (*offset)++) = (unsigned char) ((value >> 4) & 0xff);
        *tmpBuf = (unsigned char) ((value << 4) & 0xf0);
        *tmpBufEmpty = 0;
    } else {
        *tmpBuf |= ((value >> 8) & 0x0f);
        *(buffer + (*offset)++) = *tmpBuf;
        *(buffer + (*offset)++) = (unsigned char) (value & 0xff);
        *tmpBufEmpty = 1;
    }
}

void writeToBuffInterrupt(unsigned int value, unsigned char *buffer, char *offset, char *tmpBufEmpty, unsigned char *tmpBuf) {
    if (*tmpBufEmpty) {
        *(buffer + (*offset)++) = (unsigned char) ((value >> 4) & 0xff);
        *tmpBuf = (unsigned char) ((value << 4) & 0xf0);
        *tmpBufEmpty = 0;
    } else {
        *tmpBuf |= ((value >> 8) & 0x0f);
        *(buffer + (*offset)++) = *tmpBuf;
        *(buffer + (*offset)++) = (unsigned char) (value & 0xff);
        *tmpBufEmpty = 1;
    }
}

void putBufSPI(unsigned char *wrptr, unsigned char len) {
    while (len > 0) // test for string null character
    {
        SSPBUF = *wrptr++; // initiate SPI bus cycle
        while (!SSPSTATbits.BF); // wait until 'BF' bit is set
        len--;
    }
}

void putBufSPIInterrupt(unsigned char *wrptr, unsigned char len) {
    while (len > 0) // test for string null character
    {
        SSPBUF = *wrptr++; // initiate SPI bus cycle
        while (!SSPSTATbits.BF); // wait until 'BF' bit is set
        len--;
    }
}

void resetRGB(singleRGB *rgbVals) {
    for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
        rgbVals[i].red = 0;
        rgbVals[i].green = 0;
        rgbVals[i].blue = 0;
    }
}

void writeRGB(singleRGB *rgbVals) {
    unsigned char totalrgb[36] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
    unsigned char bits = 72 - (RGB_ACTUATOR_QNTY * 9); //(288 - (RGB_ACTUATOR_QNTY * 36)) / 8;
    char offset = bits / 2;
    char tmpBufEmpty;
    if (bits % 2) {
        tmpBufEmpty = 0;
    } else {
        tmpBufEmpty = 1;
    }

    unsigned char tmpBuf = 0;
    for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
        writeToBuff(~(rgbVals[i].red & 0xfff), totalrgb, &offset, &tmpBufEmpty, &tmpBuf);
        writeToBuff(~(rgbVals[i].green & 0xfff), totalrgb, &offset, &tmpBufEmpty, &tmpBuf);
        writeToBuff(~(rgbVals[i].blue & 0xfff), totalrgb, &offset, &tmpBufEmpty, &tmpBuf);
    }
    putBufSPI(totalrgb, 36);
    LED_PORT |= (1 << PIN_BLANK);
    LED_PORT |= (1 << PIN_XLAT);
    LED_PORT &= ~(1 << PIN_XLAT);
    LED_PORT &= ~(1 << PIN_BLANK);

}

void writeRGBInterrupt() {
    unsigned char totalrgb[36] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
    unsigned char bits = 72 - (RGB_ACTUATOR_QNTY * 9); //(288 - (RGB_ACTUATOR_QNTY * 36)) / 8;
    char offset = bits / 2;
    char tmpBufEmpty;
    if (bits % 2) {
        tmpBufEmpty = 0;
    } else {
        tmpBufEmpty = 1;
    }

    unsigned char tmpBuf = 0;
    for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
        writeToBuffInterrupt(~(rgbCurrent[i].red & 0xfff), totalrgb, &offset, &tmpBufEmpty, &tmpBuf);
        writeToBuffInterrupt(~(rgbCurrent[i].green & 0xfff), totalrgb, &offset, &tmpBufEmpty, &tmpBuf);
        writeToBuffInterrupt(~(rgbCurrent[i].blue & 0xfff), totalrgb, &offset, &tmpBufEmpty, &tmpBuf);
    }
    putBufSPIInterrupt(totalrgb, 36);
    LED_PORT |= (1 << PIN_BLANK);
    LED_PORT |= (1 << PIN_XLAT);
    LED_PORT &= ~(1 << PIN_XLAT);
    LED_PORT &= ~(1 << PIN_BLANK);

}


void setStrip(singleRGB *rgbVals, char strip, unsigned int red, unsigned int green, unsigned int blue) {
    rgbVals[strip].red = red;
    rgbVals[strip].green = green;
    rgbVals[strip].blue = blue;
}

unsigned char driveRGB() {
    unsigned char ret = 0;
    switch (modeRGB) {
        case MODE_ALL_AUTO:
            autoRGB();
            ret = 1;
            break;
        case MODE_CHAIN_AUTO:
            chainRGB();
            ret = 1;
            break;
        case MODE_RND:
            autoRND();
            ret = 1;
            break;
        case MODE_RND_ALL:
            autoRNDall();
            ret = 1;
            break;
    }
    
    return ret;
}


void autoRGB() {
    static unsigned int step = 0;
    
    int timeout;
    static int timer = 0;
    switch (speedRGB) {
        case 0: timeout = 4;//50;//3;//200;//30;
            break;
        case 1: timeout = 2;//10;//2;//100;
            break;
        case 2: timeout = 1;//3;//1;//60;
            break;
        default: timeout = 0;//0;//30;
            break;
    }

    if (timer > timeout) {
        for (char strip = 0; strip < RGB_ACTUATOR_QNTY; strip++) {
            setStrip(rgbCurrent, strip, lights[(step+2400)%7200], lights[step],lights[(step+4800)%7200]);    
        }
                                          
        step++;
       
        if (step > 7199) {
            step = 0;
        }
       
        timer = 0;
    }
    
    timer++;
     
}

void chainRGB() {
    
    unsigned int offset = 400;
    
    int timeout;
    static int timer = 0;
    switch (speedRGB) {
        case 0: timeout = 4;//50;//3;//200;//30;
            break;
        case 1: timeout = 2;//10;//2;//100;
            break;
        case 2: timeout = 1;//3;//1;//60;
            break;
        default: timeout = 0;//0;//30;
            break;
    }
    
    if (timer > timeout) {
        for (char strip = 0; strip < RGB_ACTUATOR_QNTY; strip++) {
            setStrip(rgbCurrent, strip, lights[(step+2400 + (strip * offset))%7200], lights[(step + (strip * offset)) % 7200],lights[(step+4800 + (strip * offset))%7200]);
        }                                    
  
        step++;
       
        if (step > 7199) {
            step = 0;
        }
       
        timer = 0;
    }
    
    timer++;
     
}

void setNextColor(unsigned char strip) {
    unsigned int a0, a1, a2;
    a0=(TMR1L % 240) * 20;
    nextColor[count[strip]][strip]=lights[a0];
    a1=rand() % 2;
    a2=((!a1)+count[strip]+1)%3;
    a1=(count[strip]+a1+1)%3;
    nextColor[a1][strip]=lights[(a0+2000)%4800];
    nextColor[a2][strip]=0;
}

void autoRND() {
   
    int timeout;
    static int timer = 0;
    
    switch (speedRGB) {
        case 0: timeout = 4;//50;//3;//200;
            break;
        case 1: timeout = 2;//100;//2;//100;
            break;
        case 1: timeout = 1;//5;//1;//75;
            break;
        default: timeout = 0;//1;//0;//50;
            break;
    }
    
    if (timer > timeout) {
       
        char generateColors[4] = {0,0,0,0};
        
        for (char strip = 0; strip < RGB_ACTUATOR_QNTY; strip++) {
            for (char col=0; col<3; col++) {
                if (color[col][strip] > nextColor[col][strip]) {
                    color[col][strip]--;
                } else if (color[col][strip] < nextColor[col][strip]) {
                    color[col][strip]++;
                } else {
                    generateColors[strip]++;
                }
            }
            
            setStrip(rgbCurrent, strip, color[0][strip], color[1][strip], color[2][strip]);
            if (generateColors[strip] == 3) {
                count[strip]++;
                count[strip]%=3;
                setNextColor(strip);
            }
        }
        
        timer = 0;
        
    }
    
    timer++;
}

void autoRNDall() {
    int timeout;
    static int timer = 0;
    
    switch (speedRGB) {
        case 0: timeout = 4;//50;//3;//200;
            break;
        case 1: timeout = 2;//10;//2;//100;
            break;
        case 1: timeout = 1;//5;//1;//75;
            break;
        default: timeout = 0;//1;//0;//50;
            break;
    }
    
    if (timer > timeout) {
       
        char generateColors = 0;
        
        for (char col=0; col<3; col++) {
            if (color[col][0] > nextColor[col][0]) {
                color[col][0]--;
            } else if (color[col][0] < nextColor[col][0]) {
                color[col][0]++;
            } else {
                generateColors++;
            }      
        }
        
        for (char strip = 0; strip < RGB_ACTUATOR_QNTY; strip++) {
            setStrip(rgbCurrent, strip, color[0][0], color[1][0], color[2][0]);
        }
        
        if (generateColors == 3) {
            count[0]++;
            count[0]%=3;
            setNextColor(0);
        }
        
        timer = 0;
        
    }
    
    timer++;
}

unsigned char getNextMode() {
    if (RGB_ACTUATOR_QNTY == 1) {
        switch(modeRGB) {
            case MODE_OFF:
                return MODE_ALL_AUTO;
            case MODE_ALL_AUTO:
                return MODE_RND;
            case MODE_RND:
                return MODE_MANUAL;
            default:
                return MODE_OFF;
        }
    } else {
        switch(modeRGB) {
            case MODE_OFF:
                return MODE_ALL_AUTO;
            case MODE_ALL_AUTO:
                return MODE_CHAIN_AUTO;
            case MODE_CHAIN_AUTO:
                return MODE_RND_ALL;
            case MODE_RND_ALL:
                return MODE_RND;
            case MODE_RND:
                return MODE_MANUAL;
            default:
                return MODE_OFF;
        }
    }
}

void initDevStateInEeprom() {
    for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
        rgb vals = getRGB(i);
        eepromDevStat[i].values.red = vals.red;
        eepromDevStat[i].values.green = vals.green;
        eepromDevStat[i].values.blue = vals.blue;
        eepromDevStat[i].mode = getMode(i);
        eepromDevStat[i].speed = getSpeed(i);
        
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_MODE, eepromDevStat[i].mode);
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_SPEED, eepromDevStat[i].speed);
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED, (unsigned char)(eepromDevStat[i].values.red >> 8));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED + 1, (unsigned char)(eepromDevStat[i].values.red & 0xff));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN, (unsigned char)(eepromDevStat[i].values.green >> 8));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN + 1, (unsigned char)(eepromDevStat[i].values.green & 0xff));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE, (unsigned char)(eepromDevStat[i].values.blue >> 8));
        eepromWrite(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE + 1, (unsigned char)(eepromDevStat[i].values.blue & 0xff));
    }
}

void updateDevStateInEeprom(unsigned char dev, unsigned char type, unsigned int val) {
    if (dev < RGB_ACTUATOR_QNTY) {
        if (type == EEPROM_RGB_MODE) {
            eepromDevStat[dev].mode = (unsigned char)val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + type, (unsigned char)val);
        }
        if (type == EEPROM_RGB_SPEED) {
            eepromDevStat[dev].speed = (unsigned char)val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + type, (unsigned char)val);
        }
        if (type == EEPROM_RGB_RED) {
            eepromDevStat[dev].values.red = val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_RED, (unsigned char)(val >> 8));
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_RED + 1, (unsigned char)(val & 0xff));
        }
        if (type == EEPROM_RGB_GREEN) {
            eepromDevStat[dev].values.green = val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_GREEN, (unsigned char)(val >> 8));
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_GREEN + 1, (unsigned char)(val & 0xff));
        }
        if (type == EEPROM_RGB_BLUE) {
            eepromDevStat[dev].values.blue = val;
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_BLUE, (unsigned char)(val >> 8));
            eepromWrite(OFFSET_EEPROM_DEV_STATE + dev * 10 + EEPROM_RGB_BLUE + 1, (unsigned char)(val & 0xff));
        }
    }
}

void loadDevStateFromEeprom() {
    for (char i = 0; i < RGB_ACTUATOR_QNTY; i++) {
        eepromDevStat[i].values.red = (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED) << 8);
        eepromDevStat[i].values.red |= (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_RED + 1));
        setRed(eepromDevStat[i].values.red, i);
        
        eepromDevStat[i].values.green = (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN) << 8);
        eepromDevStat[i].values.green |= (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_GREEN + 1));
        setGreen(eepromDevStat[i].values.green, i);
        
        eepromDevStat[i].values.blue = (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE) << 8);
        eepromDevStat[i].values.blue |= (unsigned int)(eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_BLUE + 1));
        setBlue(eepromDevStat[i].values.blue, i);
        
        eepromDevStat[i].mode = eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_MODE);
        setMode(eepromDevStat[i].mode);
        
        eepromDevStat[i].speed = eepromRead(OFFSET_EEPROM_DEV_STATE + i * 10 + EEPROM_RGB_SPEED);
        setSpeed(eepromDevStat[i].speed, i);
    }
}

void startAutoRGBTimer() {
    TMR3IE = 1;
}

void stopAutoRGBTimer() {
    TMR3IE = 0;
}

void configureAutoRGBTimer() {
    TMR3L = 0;
    TMR3H = 0xFC;
    T3CKPS1 = T3CKPS0 = 1;
    TMR3ON = 1;
    TMR3IP = 0;
    TMR3IE = 0;
}

#endif