/*********************************************************************
 *
 *                           This is the header for CAN.C
 *   
 *********************************************************************
 * FileName:        CAN.H
 * Dependencies:    CANDef.h
 * Processor:       PIC18FXX8
 * Compiler:        MCC18 v2.20 or higher
 *                  HITECH PICC-18 v8.20PL4 or higher
 * Linker:          
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company�s
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES, 
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT, 
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR 
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Thomas Castmo        06/07/2003  Initial release
 * Thomas Castmo        07/07/2003  Cleared up a bit and implemented 
 *                                  the window function CANCON<3:1> 
 *                                  for interrupts
 * Thomas Castmo        16/07/2003  Added support for the Microchip
 *                                  MPLAB C18 compiler
 * 
 ********************************************************************/
#ifndef __CAN_H
#define __CAN_H
#include "can_def.h"
#include "delay.h"

#include <pic18.h>


volatile char interruptEnabled;


//To use extended identifier for my address, comment the following line out
//#ifdef STD_IDENT
#define MY_ADDRESS_IS_STANDARD
//#endif


//If using normal mode, comment the following line out
//#ifdef LPBACK
#define USE_LOOPBACK_MODE
//#endif


//Size of RX buffer (14 bytes per buffer entry) has to be greater than or equal to 2
#define RXBUF RX_BUFFER

//Size of TX buffer (14 bytes per buffer entry) has to be greater than or equal to 2
#define TXBUF TX_BUFFER

#if TXBUF < 2 || RXBUF < 2
#error "The RXbuffer and TXbuffer has to greater than or equal to 2"
#endif

//CAN message structure (one message is 15 bytes wide)

unsigned char lastCOMSTAT;
unsigned char lastTXB0CON;
unsigned char lastTXB1CON;
unsigned char lastTXB2CON;
//unsigned char lastB0CON;
//unsigned char lastB1CON;
//unsigned char lastB2CON;
//unsigned char lastB3CON;
unsigned char lastB4CON;
unsigned char lastB5CON;
unsigned char lastRXERR;
unsigned char lastTXERR;
unsigned char lastTXBIE;
unsigned char lastBIE0;


volatile char RXRPtr = 0; //Read pointer for RXMessage buffer
volatile char RXWPtr = 0; //Write pointer for RXMessage buffer
volatile char TXRPtr = 0; //Read pointer for TXMessage buffer
volatile char TXWPtr = 0; //Write pointer for TXMessage buffer

typedef struct  {
    unsigned long Address;
    unsigned char Data[8];
    unsigned char NoOfBytes;
    unsigned char Priority;
    unsigned Ext : 1;
    unsigned Remote : 1;
}CANMessage;



/*********************************************************************
 * Function:        void ECANInitialize(void)
 *
 * Overview:        Use this function to initialize ECAN module with
 *                  options defined in ECAN.def file.
 *                  You may manually edit ECAN.def file as per your
 *                  requirements, or use Microchip Application
 *                  Maestro tool.
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    All pending transmissions are aborted.
 ********************************************************************/
void ECANInitialize(unsigned char restart);

void ECANInitializeOnStartup();
void ECANInitializeOnRestart();


/*********************************************************************
 * Function:        char CANPut(struct CANMessage Message)
 *
 * PreCondition:    None
 *
 * Input:           A CAN message
 *
 * Output:          1 -> Failed to put a CAN on the buffer, buffer is full
 *                  0 -> The CAN message is put on the buffer and will be 
 *                       transmitted eventually 
 *
 * Side Effects:    Will modify the TX Buffer register�s Write pointer
 *
 * Overview:        Initially checks if at least one buffer slot is available
 *                  and if so push the requested message in the buffer
 *
 * Note:            None
 ********************************************************************/
char CANPutToFIFO(CANMessage*);


/*********************************************************************
 * Function:        char CANRXMessageIsPending(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          1 -> At least one received message is pending in the RX buffer
 *                  0 -> No received messages are pending
 *
 * Side Effects:    None
 *
 * Overview:        Checks if the RX Write pointer is equal to RX Read pointer and
 *                  if so returns 0, else returns 1
 *
 * Note:            Since no care is taken if the buffer overflow
 *                  this function has to be polled frequently to
 *                  prevent a software receive buffer overflow
 ********************************************************************/
char CANRXMessageIsPending(void);


/*********************************************************************
 * Function:        struct CANMessage CANGet(void)
 *
 * PreCondition:    An unread message has to be in the buffer
 *                  use RXCANMessageIsPending(void) prior to 
 *                  calling this function in order to determine
 *                  if an unread message is pending.
 *
 * Input:           None
 *
 * Output:          The received message
 *
 * Side Effects:    Will modify the RX Buffer register�s Read pointer
 *
 * Overview:        Pops the the first message of the RX buffer
 *
 * Note:            None
 ********************************************************************/
CANMessage CANGet(void);

/*********************************************************************
 *
 * ECAN_OP_MODE
 *
 * This enumeration values define codes related to ECAN module
 * operation mode. ECANSetOperationMode() routine requires this code.
 * These values must be used by itself
 * i.e. it cannot be ORed to form * multiple values.
 *
 ********************************************************************/
typedef enum _ECAN_OP_MODE {
    ECAN_OP_MODE_BITS = 0xe0, // Use this to access opmode bits
    ECAN_OP_MODE_NORMAL = 0x00,
    ECAN_OP_MODE_SLEEP = 0x20,
    ECAN_OP_MODE_LOOP = 0x40,
    ECAN_OP_MODE_LISTEN = 0x60,
    ECAN_OP_MODE_CONFIG = 0x80
} ECAN_OP_MODE;

/*********************************************************************
 * Function:        void ECANSetOperationMode(ECAN_OP_MODE mode)
 *
 * Overview:        Use this function to switch ECAN module into
 *                  given operational mode.
 *                  You may not need to call this function if
 *                  your application does not require run-time
 *                  changes in ECAN mode.
 *
 * PreCondition:    None
 *
 * Input:           mode    - Operation mode code
 *                            must be of type ECAN_OP_MODES
 *
 * Output:          MCU is set to requested mode
 *
 * Side Effects:    None
 *
 * Note:            This is a blocking call.  It will not return until
 *                  requested mode is set.
 ********************************************************************/
void ECANSetOperationMode(ECAN_OP_MODE mode);

/*********************************************************************
 * Function:        char CANPutMessage(void)
 *
 * PreCondition:    <WIN2:WIN0> in the CANCON register has to set
 *                  to reflect the desired TXB registers
 *
 * Input:           None
 *
 * Output:          0 -> A new message has been put in the transmit queue
 *                  1 -> There was no messages in the TX buffer to send
 *
 * Side Effects:    Will modify the TX buffer�s Read pointer (TXRPtr)
 *
 * Overview:        Checks if there is any messages to transmit and if so
 *                  place it in the registers reflected by <WIN2:WIN0>
 *
 * Note:            None
 ********************************************************************/
char CANPutMessage(void);

/*********************************************************************
 * Function:        void CANGetMessage(void)
 *
 * PreCondition:    <WIN2:WIN0> in the CANCON register has to set
 *                  to reflect the desired RXB registers
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    Will modify the RX FIFO Write pointer (RXWPtr)
 *
 * Overview:        Gets the registers for a RXB and puts them in the
 *                  CAN Receive buffer
 *
 * Note:            Care is not taken if buffer is full
 ********************************************************************/
void CANGetMessage(void);


/*********************************************************************
 * Macro:           ECAN_OP_MODE ECANGetOperationMode()
 *
 * Overview:        Use this macro to obtain current operation mode
 *                  ECAN module.
 *                  You may not need to call this macro if your
 *                  application does not require run-time changes.
 * PreCondition:
 *
 * Input:
 *
 * Output:          Current operational mode of ECAN module is returned
 *
 * Side Effects:    None
 *
 ********************************************************************/
#define ECANGetOperationMode() (CANSTAT & ECAN_OP_MODE_BITS)

#define ECAN_PHSEG2_MODE_AUTOMATIC      0
#define ECAN_PHSEG2_MODE_PROGRAMMABLE   1

#define ECAN_BUS_SAMPLE_MODE_ONCE       0
#define ECAN_BUS_SAMPLE_MODE_THRICE     1

#define ECAN_WAKEUP_MODE_ENABLE         0
#define ECAN_WAKEUP_MODE_DISABLE        1

#define ECAN_TXDRIVE_MODE_TRISTATE  0
#define ECAN_TXDRIVE_MODE_VDD       1

#define ECAN_TX2_MODE_DISABLE   0
#define ECAN_TX2_MODE_ENABLE    1

#define ECAN_FILTER_MODE_DISABLE        0
#define ECAN_FILTER_MODE_ENABLE         1

#define ECAN_TX2_SOURCE_COMP    0
#define ECAN_TX2_SOURCE_CLOCK   1

#define ECAN_MODE_0     0x00
#define ECAN_MODE_1     0x40
#define ECAN_MODE_2     0x80

#define ECAN_DBL_BUFFER_MODE_DISABLE    0
#define ECAN_DBL_BUFFER_MODE_ENABLE     1

#define ECAN_RECEIVE_ALL_VALID  0
#define ECAN_RECEIVE_STANDARD   1
#define ECAN_RECEIVE_EXTENDED   2
#define ECAN_RECEIVE_ALL        3

#define ECAN_CAPTURE_MODE_DISABLE   0
#define ECAN_CAPTURE_MODE_ENABLE    1


/*********************************************************************
 * Macro:           ECANSetB0AutoRTRMode(mode)
 *                  ECANSetB1AutoRTRMode(mode)
 *                  ECANSetB2AutoRTRMode(mode)
 *                  ECANSetB3AutoRTRMode(mode)
 *                  ECANSetB4AutoRTRMode(mode)
 *                  ECANSetB5AutoRTRMode(mode)
 *
 * Overview:        Use these macros to set automatic RTR handling
 *                  mode for given programmable buffer.
 *                  You may not need to call this macro if your
 *                  application does not require run-time change.
 *
 * PreCondition:    ECAN_LIB_MODE_VAL = ECAN_LIB_MODE_RUN_TIME
 *                  ECAN_FUNC_MODE_VAL != ECAN_MODE_0
 *
 * Input:           mode        - AutoRTR mode
 *                  Allowable values are
 *                    ECAN_AUTORTR_MODE_DISABLE
 *                      - To disable automatic RTR handling
 *                    ECAN_AUTORTR_MODE_ENABLE
 *                      - To enable automatic RTR handling
 *
 * Output:          None
 *
 * Side Effects:
 *
 ********************************************************************/
#if ( (ECAN_LIB_MODE_VAL == ECAN_LIB_MODE_RUN_TIME) || \
      (ECAN_FUNC_MODE_VAL != ECAN_MODE_0) )

#define ECANSetB0AutoRTRMode(mode)      B0CON_RTREN = mode;     \
                                            BSEL0_B0TXEN = 1
#define ECANSetB1AutoRTRMode(mode)      B1CON_RTREN = mode;     \
                                            BSEL0_B1TXEN = 1
#define ECANSetB2AutoRTRMode(mode)      B2CON_RTREN = mode;     \
                                            BSEL0_B2TXEN = 1
#define ECANSetB3AutoRTRMode(mode)      B3CON_RTREN = mode;     \
                                            BSEL0_B3TXEN = 1
#define ECANSetB4AutoRTRMode(mode)      B4CON_RTREN = mode;     \
                                            BSEL0_B4TXEN = 1
#define ECANSetB5AutoRTRMode(mode)      B5CON_RTREN = mode;     \
                                            BSEL0_B5TXEN = 1
#endif

#define ECAN_AUTORTR_MODE_DISABLE   0
#define ECAN_AUTORTR_MODE_ENABLE    1


#define RXB0            0
#define RXB1            1
#define B0              2
#define B1              3
#define B2              4
#define B3              5
#define B4              6
#define B5              7
#define TXB0            8
#define TXB1            9
#define TXB2            10

#define ECAN_BUFFER_RX  0
#define ECAN_BUFFER_TX  1

#define RXF0            0
#define RXF1            1
#define RXF2            2
#define RXF3            3
#define RXF4            4
#define RXF5            5
#define RXF6            6
#define RXF7            7
#define RXF8            8
#define RXF9            9
#define RXF10           10
#define RXF11           11
#define RXF12           12
#define RXF13           13
#define RXF14           14
#define RXF15           15


#define _SetStdRXFnValue(f, val)                                \
    ##f##SIDH = (long)ECAN_##f##_VAL >> 3L;                     \
    ##f##SIDL = (long)ECAN_##f##_VAL << 5L

#define _SetXtdRXFnValue(f, val)                                \
    ##f##SIDH = (long)ECAN_##f##_VAL >> 21L;                    \
    ##f##SIDL = (((long)ECAN_##f##_VAL >> 13L) & 0xe0) |        \
                    ((long)(ECAN_##f##_VAL >> 16L) & 0x03L)   |        \
                    0x08;                                       \
    ##f##EIDH = (long)ECAN_##f##_VAL >> 8L;                     \
    ##f##EIDL = ECAN_##f##_VAL;


#define _SetStdRXMnValue(m, val)                                \
    RXM##m##SIDH = (long)ECAN_RXM##m##_VAL >> 3L;               \
    RXM##m##SIDL = (long)ECAN_RXM##m##_VAL << 5L

#define _SetXtdRXMnValue(m, val)                                \
    RXM##m##SIDH = (long)ECAN_RXM##m##_VAL >> 21L;              \
    RXM##m##SIDL = (((long)ECAN_RXM##m##_VAL >> 13L) & 0xe0) |  \
                    ((long)(ECAN_RXM##m##_VAL >> 16L) & 0x03L);\
    RXM##m##EIDH = (long)ECAN_RXM##m##_VAL >> 8L;\
    RXM##m##EIDL = ECAN_RXM##m##_VAL;


#define ECAN_RXFn_ENABLE        1
#define ECAN_RXFn_DISABLE       0

/*********************************************************************
 * Macro:           ECANLinkRXFnFmToBuffer(RXFnBuffer, RXFmBuffer)
 *                      n = { 0, 2, 4, 6, 8, 10, 12, 14 }
 *                      m = ( 1, 3, 5, 7, 9, 11, 13, 15 }
 *                      m = n + 1
 *
 * Overview:        Use this macro to link RXFn and RXFm to
 *                  any of RXB0, RXB1, B0, B1, B2, B3, B4, B5 buffer.
 *                  You may not need to call this macro if your
 *                  application does not require run-time change.
 *
 *
 * PreCondition:    ECAN must be in Configuration mode
 *                  Given buffer must be configured as receive buffer.
 *                      (See ECANSetRxBnTxRxMode and ECANSetBnTxRxMode)
 *
 *
 * Input:           RXFnBuffer  - Buffer that is to be linked
 *                                with RXFn filter
 *                  RXFmBuffer  - Buffer that is to be linked
 *                                with RXFm filter
 *
 *                                Allowable values for both parameters
 *                                  RXB0, RXB1, B0, B1, B2, B3, B4, B5
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 *
 * Note:            If possible, call this macro with  ant
 *                  parameters to reduce generated code.
 ********************************************************************/
#if ( (ECAN_LIB_MODE_VAL == ECAN_LIB_MODE_RUN_TIME) || \
      (ECAN_FUNC_MODE_VAL != ECAN_MODE_0) )

#define ECANLinkRXF0F1ToBuffer(RXF0Buffer, RXF1Buffer)     \
                RXFBCON0 = (RXF1Buffer << 4) | RXF0Buffer
#define ECANLinkRXF2F3ToBuffer(RXF2Buffer, RXF3Buffer)     \
                RXFBCON1 = (RXF2Buffer << 4) | RXF3Buffer
#define ECANLinkRXF4F5ToBuffer(RXF4Buffer, RXF5Buffer)     \
                RXFBCON2 = (RXF4Buffer << 4) | RXF5Buffer
#define ECANLinkRXF6F7ToBuffer(RXF6Buffer, RXF7Buffer)     \
                RXFBCON3 = (RXF6Buffer << 4) | RXF7Buffer
#define ECANLinkRXF8F9ToBuffer(RXF8Buffer, RXF9Buffer)     \
                RXFBCON4 = (RXF8Buffer << 4) | RXF9Buffer
#define ECANLinkRXF10F11ToBuffer(RXF10Buffer, RXF11Buffer) \
                RXFBCON5 = (RXF10Buffer << 4) | RXF11Buffer
#define ECANLinkRXF12F13ToBuffer(RXF12Buffer, RXF13Buffer) \
                RXFBCON6 = (RXF12Buffer << 4) | RXF13Buffer
#define ECANLinkRXF14F15ToBuffer(RXF14Buffer, RXF15Buffer) \
                RXFBCON7 = (RXF14Buffer << 4) | RXF15Buffer
#endif


/*********************************************************************
 * Macro:           ECANLinkRXF0Thru3ToMask(m0, m1, m2, m3)
 *                  ECANLinkRXF4Thru7ToMask(m4, m5, m6, m7)
 *                  ECANLinkRXF8Thru11ToMask(m8, m9, m10, m11, m12)
 *                  ECANLinkRXF12Thru15ToMask(m13, m14, m15, m16)
 *
 * Overview:        Use this macro to link receive filters to masks.
 *                  You may not need to call this macro if your
 *                  application does not require run-time change.
 *
 *
 * PreCondition:    ECAN must be in Configuration mode
 *
 * Input:           mn  - Buffer that is to be linked
 *                                with Mask
 *
 *                                Allowable values for both parameters
 *                                  ECAN_RXM0, ECAN_RXM1, ECAN_RXMF15
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 *
 * Note:            If possible, call this macro with  ant
 *                  parameters to reduce generated code.
 ********************************************************************/
#if ( (ECAN_LIB_MODE_VAL == ECAN_LIB_MODE_RUN_TIME) || \
      (ECAN_FUNC_MODE_VAL != ECAN_MODE_0) )

#define ECANLinkRXF0Thru3ToMask(m0, m1, m2, m3) \
        MSEL0 = m3 << 6 | m2 << 4 | m1 << 2 | m0;
#define ECANLinkRXF4Thru7ToMask(m4, m5, m6, m7) \
        MSEL1 = m7 << 6 | m6 << 4 | m5 << 2 | m4;
#define ECANLinkRXF8Thru11ToMask(m8, m9, m10, m11) \
        MSEL2 = m11 << 6 | m10 << 4 | m9 << 2 | m8;
#define ECANLinkRXF12Thru15ToMask(m12, m13, m14, m15) \
        MSEL2 = m15 << 6 | m14 << 4 | m13 << 2 | m12;
#endif

#define ECAN_RXM0       0
#define ECAN_RXM1       1
#define ECAN_RXMF15     2

#define RXM0SIDL_EXIDEN         RXM0SIDLbits.EXIDEN
#define RXM1SIDL_EXIDEN         RXM1SIDLbits.EXIDEN

#define ECAN_INIT_NORMAL            0x00
#define ECAN_INIT_LOOPBACK          0x40
#define ECAN_INIT_CONFIGURATION     0x80
#define ECAN_INIT_DISABLE           0x20
#define ECAN_INIT_LISTEN_ONLY       0X60

/*********************************************************************
 * Macro:           BYTE ECANGetRxErrorCount()
 *
 * Overview:        Use this macro to get current RXERRCNT value.
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          Current receive error count as defined by
 *                  CAN specifications.
 *
 * Side Effects:    None
 *
 ********************************************************************/
#define ECANGetRxErrorCount()            (RXERRCNT)

/*********************************************************************
 * Macro:           BYTE ECANGetTxErrorCount()
 *
 * Overview:        Use this macro to get current TXERRCNT value
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          Current transmit error count as defined by
 *                  CAN specifications.
 *
 * Side Effects:    None
 *
 ********************************************************************/
#define ECANGetTxErrorCount()            (TXERRCNT)


#define ECAN_MSG_STD    0
#define ECAN_MSG_XTD    1

#define ECAN_LIB_MODE_FIXED     0
#define ECAN_LIB_MODE_RUN_TIME  1

#define NoInterrupt 0x00
#define ErrorInterrupt 0x02
#define TXB2Interrupt 0x04
#define TXB1Interrupt 0x06
#define TXB0Interrupt 0x08
#define RXB1Interrupt 0x0A
#define RXB0Interrupt 0x0C
#define WakeUpInterrupt 0x0E

#define TXB2Mode1Address 0x05
#define TXB1Mode1Address 0x04
#define TXB0Mode1Address 0x03
#define RXB1Mode1Address 0x11
#define RXB0Mode1Address 0x10
#define RXTXB0Mode1Address 0x12
#define RXTXB1Mode1Address 0x13
#define RXTXB2Mode1Address 0x14
#define RXTXB3Mode1Address 0x15
#define RXTXB4Mode1Address 0x16
#define RXTXB5Mode1Address 0x17
#define TXB2Mode1Interrupt 0x04
#define TXB1Mode1Interrupt 0x06
#define TXB0Mode1Interrupt 0x08
#define RXB1Mode1Interrupt 0x11
#define RXB0Mode1Interrupt 0x10
#define RXTXB0Mode1Interrupt 0x12
#define RXTXB1Mode1Interrupt 0x13
#define RXTXB2Mode1Interrupt 0x14
#define RXTXB3Mode1Interrupt 0x15
#define RXTXB4Mode1Interrupt 0x16
#define RXTXB5Mode1Interrupt 0x17


void clearInterruptEnableBit(unsigned char interruptNumber);
unsigned char getInterruptAddress(void);
unsigned char getFirstFreeTxBuffer(void);
void setExtFilter(unsigned char number, unsigned long  filter);
void setStdFilter(unsigned char number, unsigned long  filter);
void setExtMask(unsigned char number, unsigned long  mask);
void setStdMask(unsigned char number, unsigned long  mask);
void clearAllInterruptEnableBits();
void ecanErrorHandler(void);
void restartECAN(void);
void abortAllECANTransmits(void);
void clearExtBuffer(CANMessage *message);
void setInitMaskAndFilter(unsigned long uid);
void setRegularMaskAndFilter();
#endif
