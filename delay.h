/* 
 * File:   delay.h
 * Author: Jezierski
 *
 * Created on 30 maj 2013, 20:16
 */

#ifndef DELAYMS_H
#define	DELAYMS_H


void delay_ms(unsigned long int ms);
#endif	/* DELAY_H */

