// UART.h
/*
        Author: Ngo Hai Bac
        Website: www.ngohaibac.com
 */
//=======================================================================================
#ifndef _UART_H
#define _UART_H
//========================================================================================

#include <pic18.h>
#include <stdio.h>
#include <stdlib.h>
#include "can.h"
#include "comm_can_constans.h"
//#include <../sources/common/memset.c>


#define	Fosc	32000000



#define TX		RC6
#define RX 		RC7
#define TRIS_TX		TRISC6
#define TRIS_RX		TRISC7

#define	UART_Data	TMR1H	// UART Data temporary

#define FIFO_SIZE            5
#define BUFFER_UART_SIZE     20


typedef struct  {
    unsigned char data[BUFFER_UART_SIZE];
    unsigned char length;
} UART_Buffer;

typedef struct {
    UART_Buffer uartBuffer[FIFO_SIZE];
    unsigned char ptrW;
    unsigned char ptrR;
} UART_Buffer_FIFO;


//========================================================================================
// Declare sosme functions
void UART_Init(unsigned long int baudRate); // Initialize for UART
void UART_SendByte(unsigned char a);
void UART_SendString(const  char* str);
void UART_SendNewLine(const  char* str);
unsigned char UART_getFrame( UART_Buffer_FIFO *fifo);
void getBuffer( UART_Buffer_FIFO *fifo,  UART_Buffer *buffer);
void initUartFIFO( UART_Buffer_FIFO *fifo);
unsigned char checkUartFIFObuffer(UART_Buffer_FIFO *fifo);

void printCanBuffer(CANMessage *message);
void byte2ascii(unsigned char nmb, unsigned char hex);
void printCanId(unsigned long id);
void printCanData(unsigned char *data, unsigned char len);
#endif
