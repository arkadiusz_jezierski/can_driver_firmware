#include "utils.h"
#include "eeprom.h"
#ifdef DEBUG
#include "uart.h"
#endif

void initCategories(categoryStruct* categoriesDescr) {
    for (size_t i = 0; i < CATEGORY_QNTY; i++) {
        categoriesDescr[i].categoryID = categoryIds[i];
        categoriesDescr[i].devicesQnt = devicesQnty[i];
    }
}

void resetAddresses(categoryStruct* categoriesDescr) {
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
        for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
            categoriesDescr[cat].deviceAddress[dev] = 0;
        }
    }
}

void initTimer() {
#if PROXIMITY_SENSOR_ACTUATOR
    TMR1L = 0xf0;
    TMR1H = 0xff;
    T1CONbits.RD16 = 0;
    T1CKPS0 = 0;
    T1CKPS1 = 0;
    TMR1ON = 1;
    TMR1IP = 0;

#else
    TMR1L = 0;
    TMR1H = 0xE0;
    T1CKPS0 = 1;
    T1CKPS1 = 1;
    TMR1ON = 1;
    TMR1IP = 0;
#endif
}


void startTimer() {
    TMR1IE = 1;
}

void stopTimer() {
    TMR1IE = 0;
}

void createErrorFrame(CANMessage *message, unsigned long uid) {
    message->Address = ID_REQ_ERROR_INFO | uid;
    message->Ext = 1;
    message->NoOfBytes = 0;
    for (size_t i = 0; i < 8; i++) {
        message->Data[message->NoOfBytes++] = 0;
    }
    message->Data[OFFSET_CAN_ERR_BIE0] = lastBIE0;
    message->Data[OFFSET_CAN_ERR_RXERRCNT] = lastRXERR;
    message->Data[OFFSET_CAN_ERR_TXERRCNT] = lastTXERR;
    message->Data[OFFSET_CAN_ERR_COMSTAT] = lastCOMSTAT;
    message->Data[OFFSET_CAN_ERR_TXB0B1CON] = (lastTXB0CON & 0xf0 >> 4) | (lastTXB1CON & 0xf0);
    message->Data[OFFSET_CAN_ERR_TXB2BIE] = (lastTXB2CON & 0xf0) | ((lastTXBIE & 0x1c) >> 2);
    message->Data[OFFSET_CAN_ERR_B4CON] = lastB4CON & 0x0f;
    message->Data[OFFSET_CAN_ERR_B5CON] = lastB5CON & 0x0f;

}

void createStartingUpInfo(CANMessage *message, unsigned long uid) {
    message->Address = ID_REQ_STARTING_UP_INFO | uid;
    message->Ext = 1;
    message->NoOfBytes = 2;
    message->Data[OFFSET_STARTUP_INFO] = startupInfo;    
    message->Data[OFFSET_RCON_REGISTER] = rconCopy;  
}

void prepareAddressRequestCanBuffer(CANMessage* buffer, unsigned long uid, categoryStruct* categoriesDescr) {
    buffer->Address = ID_REQ_ADDRESS | uid;
    buffer->Ext = 1;
    size_t i = 0;
    buffer->Data[i++] = getAddressRequestCounter();
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
        for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
            if (categoriesDescr[cat].deviceAddress[dev] == 0) {
                buffer->Data[i++] = categoriesDescr[cat].categoryID;
                if (i == REQUESTED_ADR_QNTY) {
                    break;
                }
            }
        }
    }
    buffer->NoOfBytes = i;

}

void prepareAddressingCompleteCanBuffer(CANMessage* buffer, unsigned long uid) {
    buffer->Address = ID_REQ_ADDRESS | uid;
    buffer->Ext = 1;
    buffer->Data[0] = CMD_ADDRESSING_COMPLETED;
    buffer->NoOfBytes = 1;

}

char getAddressesFromCanBuffer(CANMessage* buffer, categoryStruct* categoriesDescr) {
    
    if (buffer->Data[OFFSET_CAN_REQ_ADR_FRAME_ID] != (requestAddressCounter - 1)) {
        if (buffer->Data[OFFSET_CAN_REQ_ADR_FRAME_ID] == CMD_ADDRESSING_COMPLETED) {
            return ADDRESSES_ACK;
        } else {
            return -2;
        }
    }
    if (buffer->NoOfBytes <= OFFSET_CAN_REQ_ADDRESS) {
        return -1;
    }

    size_t i = OFFSET_CAN_REQ_ADDRESS;
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
        for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
            if (categoriesDescr[cat].deviceAddress[dev] == 0) {
                categoriesDescr[cat].deviceAddress[dev] = buffer->Data[i++];
                if (i >= buffer->NoOfBytes) {
                    return ADDRESSES_OK;
                }
            }
        }
    }
    return -3;
}

unsigned char checkAllAddressed(categoryStruct* categoriesDescr) {
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
        for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
            if (categoriesDescr[cat].deviceAddress[dev] == 0) {
                return 0;
            }
        }
    }
    return 1;
}

void loadAddresses(categoryStruct* categoriesDescr) {
#if CATEGORY_QNTY == 1
    int size = CATEGORY_ID_0_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY];
#endif
#if CATEGORY_QNTY == 2
    int size = CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY];
#endif
#if CATEGORY_QNTY == 3
    int size = CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY];
#endif
#if CATEGORY_QNTY == 4
    int size = CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY + CATEGORY_ID_3_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY + CATEGORY_ID_3_DEV_QNTY];
#endif

    loadEEPROMAddresses(adrTable, size);
    size_t i = 0;
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {

        for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
            categoriesDescr[cat].deviceAddress[dev] = adrTable[i++];

        }
    }
}

void printCategoryDescr(categoryStruct* categoriesDescr) {
#ifdef DEBUG
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {

        UART_SendNewLine("Category ");
        byte2ascii(cat, 0);
        UART_SendString(" ID: ");
        byte2ascii(categoriesDescr[cat].categoryID, 0);
        UART_SendNewLine("Category ");
        byte2ascii(cat, 0);
        UART_SendString(" dev qnt: ");
        byte2ascii(categoriesDescr[cat].devicesQnt, 0);


        for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
            UART_SendNewLine(" Device ");
            byte2ascii(dev, 0);
            UART_SendString(" adr: ");
            byte2ascii(categoriesDescr[cat].deviceAddress[dev], 0);

        }
    }
#endif
}

void saveAddresses(categoryStruct* categoriesDescr) {
#if CATEGORY_QNTY == 1
    int size = CATEGORY_ID_0_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY];
#endif
#if CATEGORY_QNTY == 2
    int size = CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY];
#endif
#if CATEGORY_QNTY == 3
    int size = CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY];
#endif
#if CATEGORY_QNTY == 4
    int size = CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY + CATEGORY_ID_3_DEV_QNTY;
    unsigned char adrTable[CATEGORY_ID_0_DEV_QNTY + CATEGORY_ID_1_DEV_QNTY + CATEGORY_ID_2_DEV_QNTY + CATEGORY_ID_3_DEV_QNTY];
#endif

    size_t i = 0;
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
        for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
            adrTable[i++] = categoriesDescr[cat].deviceAddress[dev];
        }
    }

    saveEEPROMAddresses(adrTable, size);
}

unsigned char isDeviceAddressed() {
    unsigned char data = eepromRead(OFFSET_EEPROM_STATUS);
    return data == STATUS_BIT_MASK_ADR_READY;
}

unsigned char isDeviceFirstRunning() {
    unsigned char data = eepromRead(OFFSET_EEPROM_STATUS);
    return data == STATUS_BIT_MASK_INIT_UID;
}

unsigned long getInitUID() {
    unsigned long uid = (unsigned long) eepromRead(OFFSET_EEPROM_INIT_UID) << 16;
    uid |= (unsigned long) eepromRead(OFFSET_EEPROM_INIT_UID + 1) << 8;
    uid |= eepromRead(OFFSET_EEPROM_INIT_UID + 2);
    return uid;
}

void setDeviceAddressed() {
    eepromWrite(OFFSET_EEPROM_STATUS, STATUS_BIT_MASK_ADR_READY);
}



unsigned char checkIfMsgBroadcast(unsigned char categoryId) {
    return categoryId == ID_CATEGORY_BROADCAST;
}

unsigned char checkIfMsgCategoryBroadcast(unsigned char address) {
    return address == ID_ADDRESS_BROADCAST;
}

unsigned char checkIfMsgGlobalBroadcast(CANMessage *message) {
    if (message->Address & ID_GLOBAL_BROADCAST) {
        return 1;
    }
    return 0;
}

unsigned char getGlobalBroadcastCommand(CANMessage *message) {
    return (unsigned char) ((message->Address >> OFFSET_CAN_GLOBAL_COMMAND) & 0xf);
}

unsigned char getReceivedMsgCategoryId(CANMessage *message) {
    unsigned char cat = (message->Address >> 11) & 0x0f;
    return cat;
}

unsigned char getReceivedMsgAddress(CANMessage* message) {
    unsigned char adr = (message->Address >> 15) & 0x7f;
    return adr;
}

unsigned char getConfirmedCommand(CANMessage* buffer) {
    return buffer->Data[OFFSET_CAN_ACK_FOR_COMMAND];
}

unsigned char checkIfMsgForThisDevice(categoryStruct* categoriesDescr, unsigned char categoryId, unsigned char address) {
    //        UART_SendNewLine(" cat ");
    //    byte2ascii(categoryId, 1);
    //    UART_SendString(" adr ");
    //    byte2ascii(address, 1);
    if (!categoryId)
        return 1;

    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
        //UART_SendNewLine("check cat ");
        //byte2ascii(categoriesDescr[cat].categoryID, 1);
        if (categoriesDescr[cat].categoryID == categoryId) {
            //UART_SendNewLine("cat found");
            for (size_t dev = 0; dev < categoriesDescr[cat].devicesQnt; dev++) {
                //UART_SendNewLine("check adr ");
                //byte2ascii(categoriesDescr[cat].deviceAddress[dev], 1);
                if (checkIfMsgCategoryBroadcast(address) || categoriesDescr[cat].deviceAddress[dev] == address) {
                    return 1;
                }
            }
        }
    }
    return 0;
}

unsigned char getReceivedMsgCommand(CANMessage* message) {
    if (message->NoOfBytes > 0) {
        return message->Data[OFFSET_CAN_COMMAND];
    }
    return CMD_CAN_DUMMY;
}

//categoryStruct *getCategoryDevices(categoryStruct* categories, unsigned char categoryId) {
//    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
//        if (categories[cat].categoryID == categoryId) {
//            return &categories[cat];
//        }
//    }
//    return 0;
//}

void insertCanCommand(CANMessage* buffer, unsigned char command) {
    buffer->Data[OFFSET_CAN_COMMAND] = command;
    buffer->NoOfBytes++;
}

void setSourceCanAddress(CANMessage* buffer, unsigned char address, unsigned char category) {
    buffer->Address = ID_ADDRESS_MASTER_DESTINATION;
    buffer->Address |= (((unsigned long) address) & 0x7f) << 4;
    buffer->Address |= (category & 0x0f);
}

void enterBoot() {
    eepromWrite(0xffff, 0xff);
    RESET();
}

unsigned char checkUID(CANMessage *inputMessage) {
    if (inputMessage->NoOfBytes > OFFSET_CAN_UID + 2) {
        unsigned long uid = getInitUID();
        if (!memcmp(&uid, &inputMessage->Data[OFFSET_CAN_UID], 3)) {
            return 1;
        }
    }
    return 0;

}

unsigned char checkUIDinFrameId(CANMessage *message) {
    return (getInitUID() == (message->Address & MASK_FOR_UID_IN_ID));
}

unsigned char checkForAllUIDs(CANMessage *inputMessage) {
    if ((inputMessage->Address & MASK_FOR_UID_IN_ID) == 0) {
        return 1;
    }
    return 0;

}

void setRunLevelInit() {
    eepromWrite(OFFSET_EEPROM_STATUS, STATUS_BIT_MASK_INIT_UID);
    for (int i = 0; i < 64; i++){
        CLRWDT();
        eepromWrite(OFFSET_EEPROM_ADDRESSES + i, 0);
    }
    runLevel = RUN_LVL_INIT;
    firstInit = 1;
    setOfflineMode();
    
}

void setRunLevelNormal() {
    eepromWrite(OFFSET_EEPROM_STATUS, STATUS_BIT_MASK_ADR_READY);
    CLRWDT();
    runLevel = RUN_LVL_NORM;
    categoriesInit = CATEGORY_QNTY;
    exitOfflineMode();
}

void setRunLevelBoot() {
    runLevel = RUN_LVL_BOOT;
}

char getRunLevel() {
    return runLevel;
}

void setRunLevelError() {

    if (runLevel != RUN_LVL_ERROR) {
#ifdef DEBUG
        UART_SendNewLine("ENTER INTO ERROR LEVEL");
#endif
        lastRunLevel = runLevel;
        runLevel = RUN_LVL_ERROR;
    }
}

void exitRunLevelError() {
#ifdef DEBUG
    UART_SendNewLine("EXIT FROM ERROR LEVEL");
#endif
    switch (lastRunLevel) {
        case RUN_LVL_INIT:
            setRunLevelInit();
            break;
        case RUN_LVL_NORM:
            setRunLevelNormal();
            break;
    }
}

unsigned char initCategory() {
    if (categoriesInit > 0) {
        categoriesInit--;
        return 1;
    } else {
        return 0;
    }
}

void makeGlobalBroadcastCommand(CANMessage *message) {
    unsigned char command = getGlobalBroadcastCommand(message);
    switch (command) {
        case CMD_CAN_GLOB_BOOT:
            //UART_SendNewLine("BOOT REQ");
            if (checkUIDinFrameId(message)) {
                //UART_SendNewLine("BOOT OK");
                setRunLevelBoot();
            }
            break;
        case CMD_CAN_GLOB_INIT:
            //UART_SendNewLine("INIT REQ");
            if (checkUIDinFrameId(message)) {
                //UART_SendNewLine("INIT OK");
                setRunLevelInit();
            }
            if (checkForAllUIDs(message)) {
                //UART_SendNewLine("INIT OK");
                setRunLevelInit();
            }
            break;
    }
}

void initStartupInfo() {
    unsigned int resetCnt = getResetCounter();
    unsigned int wdtCnt = getWdtCounter();
    rconCopy = RCON;
    
    resetCnt++;
    if (!RCONbits.TO) {
        wdtCnt++;
        eepromWrite(OFFSET_EEPROM_WDT_CNT + 1, (unsigned char) (wdtCnt & 0xff));
        eepromWrite(OFFSET_EEPROM_WDT_CNT, (unsigned char) ((wdtCnt >> 8) & 0xff));
        startupInfo = RESET_BY_WDT;
    } else {
        if (RCONbits.RI) {
            startupInfo = STARTED_ON_POWER_UP;
        } else {
            startupInfo = RESET_BY_RESET_INSTRUCTION;
        }
    }
    
    eepromWrite(OFFSET_EEPROM_RESET_CNT + 1, (unsigned char) (resetCnt & 0xff));
    eepromWrite(OFFSET_EEPROM_RESET_CNT, (unsigned char) ((resetCnt >> 8) & 0xff));

}

unsigned int getResetCounter() {
    if (resetCounter == 0xffff) {
        resetCounter = (unsigned int) (eepromRead(OFFSET_EEPROM_RESET_CNT) << 8);
        resetCounter |= (unsigned int) (eepromRead(OFFSET_EEPROM_RESET_CNT + 1) & 0xff);
    }
    return resetCounter;
}

unsigned int getWdtCounter() {
    if (watchdogCounter == 0xffff) {
        watchdogCounter = (unsigned int) (eepromRead(OFFSET_EEPROM_WDT_CNT) << 8);
        watchdogCounter |= (unsigned int) (eepromRead(OFFSET_EEPROM_WDT_CNT + 1) & 0xff);
    }
    return watchdogCounter;
}

void insertResetCounters(CANMessage* buffer) {
    unsigned int cnt = getResetCounter();
    buffer->Data[OFFSET_CAN_RESET_COUNTERS] = (unsigned char) (cnt & 0xff);
    buffer->Data[OFFSET_CAN_RESET_COUNTERS + 1] = (unsigned char) ((cnt >> 8) & 0xff);
    cnt = getWdtCounter();
    buffer->Data[OFFSET_CAN_RESET_COUNTERS + 2] = (unsigned char) (cnt & 0xff);
    buffer->Data[OFFSET_CAN_RESET_COUNTERS + 3] = (unsigned char) ((cnt >> 8) & 0xff);
    
    buffer->Data[OFFSET_CAN_RX_ERR_COUNTER] = (unsigned char) RXERRCNT;
    buffer->Data[OFFSET_CAN_TX_ERR_COUNTER] = (unsigned char) TXERRCNT;

    buffer->NoOfBytes += 6;
}

void resetAddressRequestCounter(void) {
    requestAddressCounter = 0;
}

unsigned char getAddressRequestCounter(void) {
    return requestAddressCounter++;
}