/* 
 * File:   offline_driver.h
 * Author: WinVirt
 *
 * Created on 10 grudzie? 2014, 18:19
 */

#include "utils.h"

#ifndef OFFLINE_DRIVER_H
#define	OFFLINE_DRIVER_H


unsigned char offline = 0;


void setOfflineMode();
void exitOfflineMode();
unsigned char checkOfflineMode();



#ifdef OFFLINE_MODE
void makeOfflineModeOperations(void);
void initOfflineMode(void);


#if SWITCH_SENSOR_QNTY > 0
signed char sensorOfflineStatus[SWITCH_SENSOR_QNTY];

char checkSensorChange(unsigned char);
#endif

#endif //OFFLINE_MODE

#endif	/* OFFLINE_DRIVER_H */

