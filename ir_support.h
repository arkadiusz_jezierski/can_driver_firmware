/* 
 * File:   ir_support.h
 * Author: home
 *
 * Created on 01 March 2020, 22:28
 */

#ifndef IR_SUPPORT_H
#define	IR_SUPPORT_H

#include  <pic18.h>

volatile char lastClk = 0;
#define PIN_CLK     6
#define PIN_DATA    7
#define PORT_CLK    PORTB
#define PORT_DATA   PORTB
#define DIR_CLK     TRISB
#define DIR_DATA    TRISB

volatile char incomingCode = 0;
const char codePattern = 0xf6;  //0b11110110
volatile unsigned char irCodeReceived = 0;

void initIRSupport(void);
char checkCode(char portData);
char handleIncomingData(char bitCode);

#endif	/* IR_SUPPORT_H */

