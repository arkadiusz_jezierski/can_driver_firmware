#include <iostream>
#include <cmath>

using namespace std;

#define MUL 20

unsigned int lights[360 * MUL];
unsigned int kkk = 0;

float  PI = 3.14159265358979f;

void setup(int mul) {

    //sine wave
    cout<<"const uint8_t lights["<<360 * MUL<<"]={";
    for (float k=PI; k<(3*PI); k=k+PI/(120 * mul))
      {
        lights[kkk]=int((cos(k)+1)*2047.1);//127.7); // I use cosinus
        if (lights[kkk]<10) cout<<" "; // I like to keep the table clean
        if (lights[kkk]<100) cout<<" "; // told you I use them a lot!
        cout<<(lights[kkk]);
        if (kkk<(255 * mul))
            cout<<", "; //'if' portion is useful if you're not filling the
                                         // rest of the table with zeroes. It makes sure there's
                                         // no extra comma after the final value of array.
                                         // I keep it here just for that situation
        if (kkk%8==7) cout<<endl;  // new line for cleaner table!
        kkk++;
      }

    for (; kkk<(360 * mul); kkk++) // fill the rest with zeroes
      {
        cout<<"  0";
        if (kkk<(360 * mul - 1)) cout<<", ";
        if (kkk%8==7) cout<<endl;
      }
    cout<<"};";
}


int main()
{
    setup(MUL);
    cout<<lights[1920]<<endl;
    return 0;
}
