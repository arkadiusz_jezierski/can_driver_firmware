/* 
 * File:   bistable_switch_actuator_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */




#ifndef BISTABLE_SWITCH_ACTUATOR_DRIVER_H
#define	BISTABLE_SWITCH_ACTUATOR_DRIVER_H

#if SWITCH_ACTUATOR_QNTY > 0

#include "comm_can_constans.h"
#include "utils.h"
#include <pic18.h>
#include "offline_driver.h" 

#if PROXIMITY_SENSOR_ACTUATOR

#define ACTUATOR_DIR       TRISB
#define ACTUATOR_PORT      LATB
#define ACTUATOR_PIN_1     4

#define ACTUATOR_1_ON  (ACTUATOR_PORT |= (1 << ACTUATOR_PIN_1));
#define ACTUATOR_1_OFF  (ACTUATOR_PORT &= ~(1 << ACTUATOR_PIN_1));
#define ACTUATOR_1_TG  (ACTUATOR_PORT ^= (1 << ACTUATOR_PIN_1));
#define ACTUATOR_STATE_1 (ACTUATOR_PORT & (1 << ACTUATOR_PIN_1))

volatile unsigned char outputRequestValue = 0;

#else
#define ACTUATOR_DIR       TRISA
#define ACTUATOR_DIR_B       TRISB
#define ACTUATOR_PORT      LATA
#define ACTUATOR_PORT_B      LATB
#define ACTUATOR_PORT2     PORTA
#define ACTUATOR_PORT2_B     PORTB


//---

//@MUST SET
// SW_x1:
// PIN_1 - 0
//---------
// SW_x2:
// PIN_1 - 0
// PIN_2 - 1
//---------
// SW_x2_sens_x1:
// PIN_1 - 0
// PIN_2 - 4,5,6
//---------
// SW_x4_sens_x3:
// PIN_1 - 0
// PIN_2 - 4
// PIN_3 - 5
// PIN_4 - 6
//---------

#if (SWITCH_ACTUATOR_QNTY == 5)
#define ACTUATOR_PIN_1     0
#define ACTUATOR_PIN_2     1
#define ACTUATOR_PIN_3     4
#define ACTUATOR_PIN_4     5
#define ACTUATOR_PIN_5     6
#else
#define ACTUATOR_PIN_1     0
#define ACTUATOR_PIN_2     4//1
#define ACTUATOR_PIN_3     5
#define ACTUATOR_PIN_4     6
#endif



#define ACTUATOR_1_ON  ACTUATOR_PORT |= (1 << ACTUATOR_PIN_1);
#define ACTUATOR_2_ON  ACTUATOR_PORT |= (1 << ACTUATOR_PIN_2);
//#define ACTUATOR_2_ON  ACTUATOR_PORT |= (1 << ACTUATOR_PIN_2)|(1 << ACTUATOR_PIN_3)|(1 << ACTUATOR_PIN_4);
#define ACTUATOR_3_ON  ACTUATOR_PORT |= (1 << ACTUATOR_PIN_3);


#if (SWITCH_ACTUATOR_QNTY == 5)
#define ACTUATOR_4_ON  ACTUATOR_PORT |= (1 << ACTUATOR_PIN_4);
#define ACTUATOR_5_ON  ACTUATOR_PORT_B |= (1 << ACTUATOR_PIN_5);
#else
#define ACTUATOR_4_ON  ACTUATOR_PORT_B |= (1 << ACTUATOR_PIN_4);
#endif

#define ACTUATOR_1_OFF  ACTUATOR_PORT &= ~(1 << ACTUATOR_PIN_1);
#define ACTUATOR_2_OFF  ACTUATOR_PORT &= ~(1 << ACTUATOR_PIN_2);
//#define ACTUATOR_2_OFF  ACTUATOR_PORT &= ~((1 << ACTUATOR_PIN_2)|(1 << ACTUATOR_PIN_3)|(1 << ACTUATOR_PIN_4));
#define ACTUATOR_3_OFF  ACTUATOR_PORT &= ~(1 << ACTUATOR_PIN_3);


#if (SWITCH_ACTUATOR_QNTY == 5)
#define ACTUATOR_4_OFF  ACTUATOR_PORT &= ~(1 << ACTUATOR_PIN_4);
#define ACTUATOR_5_OFF  ACTUATOR_PORT_B &= ~(1 << ACTUATOR_PIN_5);
#else
#define ACTUATOR_4_OFF  ACTUATOR_PORT_B &= ~(1 << ACTUATOR_PIN_4);
#endif

#define ACTUATOR_1_TG  ACTUATOR_PORT ^= (1 << ACTUATOR_PIN_1);
#define ACTUATOR_2_TG  ACTUATOR_PORT ^= (1 << ACTUATOR_PIN_2);
//#define ACTUATOR_2_TG  ACTUATOR_PORT ^= (1 << ACTUATOR_PIN_2)|(1 << ACTUATOR_PIN_3)|(1 << ACTUATOR_PIN_4);
#define ACTUATOR_3_TG  ACTUATOR_PORT ^= (1 << ACTUATOR_PIN_3);

#if (SWITCH_ACTUATOR_QNTY == 5)
#define ACTUATOR_4_TG  ACTUATOR_PORT ^= (1 << ACTUATOR_PIN_4);
#define ACTUATOR_5_TG  ACTUATOR_PORT_B ^= (1 << ACTUATOR_PIN_5);
#else
#define ACTUATOR_4_TG  ACTUATOR_PORT_B ^= (1 << ACTUATOR_PIN_4);
#endif


#define ACTUATOR_STATE_1 ACTUATOR_PORT & (1 << ACTUATOR_PIN_1)
#define ACTUATOR_STATE_2 ACTUATOR_PORT & (1 << ACTUATOR_PIN_2)
#define ACTUATOR_STATE_3 ACTUATOR_PORT & (1 << ACTUATOR_PIN_3)


#if (SWITCH_ACTUATOR_QNTY == 5)
#define ACTUATOR_STATE_4 ACTUATOR_PORT2 & (1 << ACTUATOR_PIN_4)
#define ACTUATOR_STATE_5 ACTUATOR_PORT2_B & (1 << ACTUATOR_PIN_5)
#else
#define ACTUATOR_STATE_4 ACTUATOR_PORT_B & (1 << ACTUATOR_PIN_4)
#endif

#endif  //not PROXIMITY_SENSOR_ACTUATOR


typedef void (*functionReceiver) (CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address);
typedef unsigned char (*functionTransmitter) (CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent);

signed char eepromStatuses[SWITCH_ACTUATOR_QNTY]; 

typedef struct {
    unsigned char address;
    unsigned char state;
    unsigned char stateInitialized;
} deviceStatus;

deviceStatus deviceStatuses[SWITCH_ACTUATOR_QNTY];
unsigned char counterTimer = 0;
signed char unblockNumber = -1;
unsigned char unblockPin;

void makeCommandForBistableActuator(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command, unsigned char address );
unsigned char readDeviceBistableActuator(CANMessage *outputMessage,  categoryStruct* categoryDevices, unsigned char timerEvent);
void makeBroadcastCommandForBistableActuator(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command);

signed char getCanOutputNumber(   categoryStruct* categoryDevices, unsigned char address);
signed char getCanOutputStatus(CANMessage *message);
signed char getCanOutputUnblockNumber(CANMessage *message);

void setCanOutputStatus(CANMessage *message, unsigned char state);


void initDeviceStatusesTable(  categoryStruct*  categoryDevices, deviceStatus *deviceStatuses);
unsigned char checkDeviceStatuses(CANMessage *message,   categoryStruct* categoryDevices, deviceStatus *deviceStatuses);


void initBistableSwitchActuatorPortIO();
void testBistableSwitchActuatorPortIO(void);

signed char getOutputStatus(unsigned char pinNo);
void setOutputStatus(unsigned char pinNo, unsigned char status);
void switchOutputState(unsigned char pinNumber);

void initDevStateInEeprom();
void updateDevStateInEeprom(unsigned char dev, unsigned char val);
void loadDevStateFromEeprom();
void checkDevStatBeforeEepromUpdate();

#endif

#endif	/* BISTABLE_SWITCH_ACTUATOR_DRIVER_H */

