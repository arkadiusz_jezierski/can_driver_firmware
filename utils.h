/* 
 * File:   utils.h
 * Author: jezierski
 *
 * Created on November 25, 2014, 11:18 PM
 */

#ifndef UTILS_H
#define	UTILS_H

#include "can.h"
#include <string.h>
#include  <pic18.h>
#include "can.h"
#include "eeprom.h"
#include "comm_can_constans.h"
#include "comm_settings.h"
#include "offline_driver.h" 



#if PROXIMITY_SENSOR_ACTUATOR
#define SEC_VALUE   65530
#else
#define SEC_VALUE   122
#endif


#define RUN_LVL_UNDEF    -1
#define RUN_LVL_INIT    1
#define RUN_LVL_NORM    2
#define RUN_LVL_BOOT    3
#define RUN_LVL_ERROR   4

#define FALSE   0
#define TRUE    1

#define RESET_BY_WDT                1
#define RESET_BY_RESET_INSTRUCTION  2
#define STARTED_ON_POWER_UP         3


#define ADDRESSES_OK    0
#define ADDRESSES_ACK   1

char runLevel = RUN_LVL_UNDEF;
char lastRunLevel = RUN_LVL_UNDEF;
char firstInit = 1;
unsigned char categoriesInit = 0;
unsigned int resetCounter = 0xffff;
unsigned int watchdogCounter = 0xffff;

unsigned char rconCopy;


unsigned char requestAddressCounter = 0;

typedef struct {
    unsigned char categoryID;
    unsigned char devicesQnt;
    volatile unsigned char *deviceAddress;
} categoryStruct;

unsigned char startupInfo;


#if CATEGORY_QNTY == 1
unsigned char categoryIds[CATEGORY_QNTY] = {CATEGORY_ID_0};
unsigned char devicesQnty[CATEGORY_QNTY] = {CATEGORY_ID_0_DEV_QNTY};
#endif
#if CATEGORY_QNTY == 2
unsigned char categoryIds[CATEGORY_QNTY] = {CATEGORY_ID_0, CATEGORY_ID_1};
unsigned char devicesQnty[CATEGORY_QNTY] = {CATEGORY_ID_0_DEV_QNTY, CATEGORY_ID_1_DEV_QNTY};
#endif
#if CATEGORY_QNTY == 3
unsigned char categoryIds[CATEGORY_QNTY] = {CATEGORY_ID_0, CATEGORY_ID_1, CATEGORY_ID_2};
unsigned char devicesQnty[CATEGORY_QNTY] = {CATEGORY_ID_0_DEV_QNTY, CATEGORY_ID_1_DEV_QNTY, CATEGORY_ID_2_DEV_QNTY};
#endif
#if CATEGORY_QNTY == 4
unsigned char categoryIds[CATEGORY_QNTY] = {CATEGORY_ID_0, CATEGORY_ID_1, CATEGORY_ID_2, CATEGORY_ID_3};
unsigned char devicesQnty[CATEGORY_QNTY] = {CATEGORY_ID_0_DEV_QNTY, CATEGORY_ID_1_DEV_QNTY, CATEGORY_ID_2_DEV_QNTY, CATEGORY_ID_3_DEV_QNTY};
#endif

//#define RUN_LVL_UNDEF    -1
//#define RUN_LVL_INIT    1
//#define RUN_LVL_NORM    2
//
//char runLevel = RUN_LVL_UNDEF;


unsigned char isDeviceAddressed(void);
unsigned char isDeviceFirstRunning(void);
unsigned long getInitUID(void);
unsigned char checkAllAddressed(  categoryStruct* categoriesDescr);


void initCategories(categoryStruct *categoriesDescr);
void resetAddresses(categoryStruct* categoriesDescr);
void loadAddresses(categoryStruct* categoriesDescr);
void saveAddresses(  categoryStruct* categoriesDescr);
void initTimer(void);
void startTimer(void);
void stopTimer(void);
void initStartupInfo();
unsigned int getResetCounter(void);
unsigned int getWdtCounter(void);

void insertCanCommand(CANMessage *buffer, unsigned char command);
void insertResetCounters(CANMessage *buffer);
void setSourceCanAddress(CANMessage *buffer, unsigned char address, unsigned char category);
void prepareAddressRequestCanBuffer(CANMessage *buffer, unsigned long uid,   categoryStruct* categoriesDescr);
void prepareAddressingCompleteCanBuffer(CANMessage* buffer, unsigned long uid);
char getAddressesFromCanBuffer(CANMessage *buffer, categoryStruct* categoriesDescr);
void setDeviceAddressed(void);
unsigned char getConfirmedCommand(CANMessage *buffer);

unsigned char checkIfMsgBroadcast(unsigned char categoryId);
unsigned char checkIfMsgCategoryBroadcast(unsigned char address);
unsigned char checkIfMsgGlobalBroadcast(CANMessage *message);
unsigned char getGlobalBroadcastCommand(CANMessage *message);
unsigned char checkIfMsgForThisDevice(  categoryStruct* categoriesDescr, unsigned char categoryId, unsigned char address);
unsigned char getReceivedMsgCategoryId(CANMessage *message);
unsigned char getReceivedMsgAddress(CANMessage *message);
unsigned char getReceivedMsgCommand(CANMessage *message);
//categoryStruct *getCategoryDevices(categoryStruct* categories, unsigned char categoryId);

void enterBoot(void);
unsigned char checkUID(CANMessage *message);
unsigned char checkUIDinFrameId(CANMessage *message);

void printCategoryDescr(  categoryStruct *categoriesDescr);

void setRunLevelInit(void);
void setRunLevelNormal(void);
void setRunLevelBoot(void);
char getRunLevel(void);
void setRunLevelError(void);
void exitRunLevelError(void);

unsigned char initCategory();

void makeGlobalBroadcastCommand(CANMessage *message);
void createErrorFrame(CANMessage *message, unsigned long uid);
void createStartingUpInfo(CANMessage *message, unsigned long uid);

void resetAddressRequestCounter(void);
unsigned char getAddressRequestCounter(void);

#endif	/* UTILS_H */

