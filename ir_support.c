#include "ir_support.h"

char handleIncomingData(char bitCode) {

    incomingCode <<= 1;
    incomingCode |= (bitCode & 1);
    
    char result = (incomingCode == codePattern);
    
    if (result) {
        incomingCode = 0;
    }
    
    return result;
}

char checkCode(char portData) {
    if ((portData & (1 << PIN_CLK)) > lastClk) {
        return handleIncomingData((portData >> PIN_DATA) & 1);
    }
        
    lastClk = portData & (1 << PIN_CLK);
    
    return 0;
}

void initIRSupport() {
    DIR_CLK |= (1 << PIN_CLK);
    DIR_DATA |= (1 << PIN_DATA);

    lastClk = PORT_CLK & (1 << PIN_CLK);
    
    RBIE = 1;
    RBIP = 0;
}
