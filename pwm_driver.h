/* 
 * File:   pwm_driver.h
 * Author: WinVirt
 *
 * Created on 8 marzec 2015, 21:53
 */

#ifndef PWM_DRIVER_H
#define	PWM_DRIVER_H

#if PWM_ACTUATOR_QNTY > 0

#define SPI_V1

#include "comm_can_constans.h"
#include "utils.h"
#include <pic18.h>

#define LED_DIR       TRISC
#define LED_PORT      LATC
#define PIN_BLANK     0
#define PIN_XLAT      6

#define OFFSET (288 - (PWM_ACTUATOR_QNTY * 12)) / 8;      //14outs->15index,  8outs->24index


//  for 4.7W    //@MUST SET
static unsigned int pwmMap[256] = {0, 100, 102, 105, 107, 110, 112, 115, 117, 120,
                                   122, 124, 126, 128, 130, 132, 134, 136, 138, 140,
                                   142, 144, 146, 148, 150, 152, 154, 156, 158, 160,
                                   162, 164, 166, 168, 170, 172, 174, 176, 178, 180,
                                   183, 186, 189, 192, 195, 198, 201, 204, 207, 210,
                                   213, 216, 219, 222, 225, 228, 231, 234, 237, 240,
                                   243, 246, 249, 252, 255, 258, 261, 264, 267, 270,
                                   274, 278, 282, 286, 290, 294, 298, 302, 306, 310,
                                   314, 318, 322, 326, 330, 334, 338, 342, 346, 350,
                                   355, 360, 365, 370, 375, 380, 385, 390, 395, 400,
                                   //
                                   405, 410, 415, 420, 425, 430, 435, 440, 445, 450,
                                   456, 462, 468, 474, 480, 486, 492, 498, 504, 510,
                                   516, 522, 528, 534, 540, 546, 552, 558, 564, 570,
                                   578, 586, 594, 602, 610, 618, 626, 634, 642, 650,
                                   660, 670, 680, 690, 700, 710, 720, 730, 740, 750,
                                   762, 774, 786, 798, 810, 822, 834, 846, 858, 870,
                                   884, 898, 912, 926, 940, 954, 968, 982, 996, 1010,
                                   1026, 1042, 1058, 1074, 1090, 1106, 1122, 1138, 1154, 1170,
                                   1188, 1206, 1224, 1242, 1260, 1278, 1296, 1314, 1332, 1350,
                                   1368, 1386, 1404, 1422, 1440, 1458, 1476, 1494, 1512, 1530,
//
                                   1548, 1566, 1584, 1602, 1620, 1638, 1656, 1674, 1692, 1710,
                                   1728, 1746, 1764, 1782, 1800, 1818, 1836, 1854, 1872, 1890,
                                   1908, 1926, 1944, 1962, 1980, 1998, 2016, 2034, 2052, 2070,
                                   2088, 2106, 2124, 2142, 2160, 2178, 2196, 2214, 2232, 2250,
                                   2275, 2300, 2325, 2350, 2375, 2400, 2425, 2450, 2475, 2500,
                                   2750, 3000, 3250, 3500, 3750, 4095};


//  for 7W  //@MUST SET
//static unsigned int pwmMap[256] = {0, 20, 22, 24, 27, 30, 33, 36, 39, 42,
//                                   45, 48, 51, 54, 57, 60, 63, 66, 69, 72,
//                                   75, 78, 81, 84, 87, 90, 93, 96, 99, 102,
//                                   105, 108, 111, 114, 117, 120, 123, 126, 129, 132,
//                                   135, 138, 141, 144, 147, 150, 153, 156, 158, 160,
//                                   164, 168, 172, 176, 180, 184, 188, 192, 196, 200,
//                                   204, 208, 212, 216, 220, 224, 228, 232, 236, 240,
//                                   245, 250, 255, 260, 265, 270, 275, 280, 285, 290,
//                                   295, 300, 305, 310, 315, 320, 325, 330, 335, 340,
//                                   346, 352, 358, 364, 370, 376, 382, 388, 394, 400,
//                                   //
//                                   405, 410, 415, 420, 425, 430, 435, 440, 445, 450,
//                                   456, 462, 468, 474, 480, 486, 492, 498, 504, 510,
//                                   516, 522, 528, 534, 540, 546, 552, 558, 564, 570,
//                                   578, 586, 594, 602, 610, 618, 626, 634, 642, 650,
//                                   660, 670, 680, 690, 700, 710, 720, 730, 740, 750,
//                                   762, 774, 786, 798, 810, 822, 834, 846, 858, 870,
//                                   884, 898, 912, 926, 940, 954, 968, 982, 996, 1010,
//                                   1026, 1042, 1058, 1074, 1090, 1106, 1122, 1138, 1154, 1170,
//                                   1188, 1206, 1224, 1242, 1260, 1278, 1296, 1314, 1332, 1350,
//                                   1368, 1386, 1404, 1422, 1440, 1458, 1476, 1494, 1512, 1530,
////
//                                   1560, 1590, 1620, 1650, 1680, 1710, 1740, 1770, 1800, 1830,
//                                   1860, 1890, 1920, 1950, 1980, 2010, 2040, 2070, 2100, 2130,
//                                   2170, 2210, 2250, 2290, 2330, 2370, 2410, 2450, 2490, 2530,
//                                   2570, 2610, 2650, 2690, 2730, 2770, 2810, 2850, 2890, 2930,
//                                   3000, 3070, 3140, 3210, 3280, 3350, 3420, 3490, 3560, 3630,
//                                   3700, 3770, 3850, 3920, 3990, 4095};

#if PWM_ACTUATOR_QNTY > 0
unsigned char pwm[PWM_ACTUATOR_QNTY];
unsigned char tmpPWM[PWM_ACTUATOR_QNTY];
unsigned char requestedPWM[PWM_ACTUATOR_QNTY];
unsigned char eepromPWM[PWM_ACTUATOR_QNTY];
#else
unsigned char pwm[1];
unsigned char tmpPWM[1];
unsigned char requestedPWM[1];
#endif

typedef void (*functionReceiver) (CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address);
typedef unsigned char (*functionTransmitter) (CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent);


typedef struct {
    unsigned char address;
    unsigned char pwmValue;    
    unsigned char pwmValueInitialized;
} pwmStatus;

#if PWM_ACTUATOR_QNTY > 0
pwmStatus pwmStatuses[PWM_ACTUATOR_QNTY];
#else
unsigned char pwmStatuses[1];
#endif

void makeCommandForPwmActuator(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command, unsigned char address );
unsigned char readDevicePwmActuator(CANMessage *outputMessage,  categoryStruct* categoryDevices, unsigned char timerEvent);
void makeBroadcastCommandForPwmActuator(CANMessage *outputMessage,  CANMessage *inputMessage,    categoryStruct* categoryDevices, unsigned char command);

signed char getCanPwmChannel(   categoryStruct* categoryDevices, unsigned char address);
unsigned char getCanChannelPwmValue(CANMessage *message);
unsigned char getCanChannelPwmValueForMultiSet(CANMessage *message, unsigned char channelOffset);

void setCanChannelPwmValue(CANMessage *message, unsigned char pwmValue);


void initPwmStatusesTable(  categoryStruct*  categoryDevices, pwmStatus *pwmStatuses);
unsigned char checkPwmValues(CANMessage *message,   categoryStruct* categoryDevices, pwmStatus *pwmStatuses);


void initPwmActuatorPortIO();

unsigned char getPwmValue(signed char channel);
void setPwmValue(signed char channel, unsigned char value);

void checkRequestedPwmValue(void);




//void setAllPWM(unsigned char *pwm);
//void setPWM(unsigned char *pwm, unsigned char value, unsigned char channel);
int remapPWM(unsigned char pwm);
unsigned char getPWM(unsigned char *pwm, unsigned char channel);
void writeToBuff(unsigned char *pwm, unsigned char *buffer);
void putBufSPI(unsigned char *wrptr, unsigned char len);
void resetPWM(unsigned char *pwm);
void writePWM(unsigned char *pwm);
void increasePWM(unsigned char *pwm, unsigned char value, unsigned char channel);aseAllChannelPWM(unsigned char *pwm, unsigned char value);
void decreasePWM(unsigned char *pwm, unsigned char value, unsigned char channel);
void decreaseAllChannelPWM(unsigned char *pwm, unsigned char value);
void increaseAllChannelPWM(unsigned char *pwm, unsigned char value);

void initDevStateInEeprom();
void updateDevStateInEeprom(unsigned char dev, unsigned char val);
void loadDevStateFromEeprom();
void checkDevStatBeforeEepromUpdate();

void handleIRSupport(void);

#endif
#endif	/* PWM_DRIVER_H */

