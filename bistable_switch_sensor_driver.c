/* 
 * File:   bistable_switch_sensor_driver.h
 * Author: jezierski
 *
 * Created on November 27, 2014, 9:05 PM
 */

#include "comm_settings.h"

#if SWITCH_SENSOR_QNTY > 0

#include "bistable_switch_sensor_driver.h"
#if SWITCH_ACTUATOR_QNTY > 0
#include "bistable_switch_actuator_driver.h"
#endif

void makeCommandForBistableSensor(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address) {
    signed char output;
    signed char state;
    if (address == 0) {
        makeBroadcastCommandForBistableSensor(outputMessage, inputMessage, categoryDevices, command);
        return;
    }
    switch (command) {
        case CMD_CAN_PING:
            insertCanCommand(outputMessage, CMD_CAN_PONG);
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            insertResetCounters(outputMessage);
            break;

        case CMD_CAN_GET_SENSOR_STATUS:
            output = getCanInputNumber(categoryDevices, address);
            state = getSensorStatus(output);
            if (output >= 0 && state >= 0) {
                insertCanCommand(outputMessage, CMD_CAN_GET_SENSOR_STATUS);
                setCanInputStatus(outputMessage, (unsigned char) state);
            } else {
                insertCanCommand(outputMessage, CMD_CAN_NACK);
            }
            setSourceCanAddress(outputMessage, address, categoryDevices->categoryID);
            break;
        case CMD_CAN_ACK:
            exitOfflineMode();
            setACK(inputMessage, categoryDevices, &sensorStatuses);
            break;
    }
}

unsigned char readDeviceBistableSensor(CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent) {

    static unsigned char initialize = 1;

    if (initialize || initCategory()) {
        //        initPingTable(&pingInfo);
        initSensorStatusesTable(categoryDevices, &sensorStatuses);

        initialize = 0;
    }

    if (timerEvent) {

        //        checkPing(&pingInfo);
        if (checkSensorStatuses(outputMessage, categoryDevices, &sensorStatuses)) {
            setAckTimeout();
            return 1;
        }
        if (checkAckStatus(outputMessage, categoryDevices)) {
            return 1;
        }
    }

    return 0;
}

void initSensorStatusesTable(categoryStruct* categoryDevices, sensorStatus *sensorStatuses) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        sensorStatuses[dev].address = categoryDevices->deviceAddress[dev];
        sensorStatuses[dev].ackStatus = ACK_INIT;
    }
}

unsigned char checkSensorStatuses(CANMessage *message, categoryStruct* categoryDevices, sensorStatus *sensorStatuses) {
    signed char state;
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        state = getSensorStatus(dev);

        if (sensorStatuses[dev].state != state || sensorStatuses[dev].ackStatus == ACK_INIT) {
            sensorStatuses[dev].state = state;
            sensorStatuses[dev].ackStatus = ACK_AWAITING;
            insertCanCommand(message, CMD_CAN_GET_SENSOR_STATUS);
            setCanInputStatus(message, (unsigned char) state);
            setSourceCanAddress(message, sensorStatuses[dev].address, categoryDevices->categoryID);
            return 1;
        }
    }
    return 0;
}

unsigned char checkAckStatus(CANMessage *message, categoryStruct* categoryDevices) {
    ackTimeout--;

    if (ackTimeout == 0) {

        if (checkOfflineMode()) {
            ackTimeout = NO_ACK_TOUT_IN_OFFLINE;
        } else {
            ackTimeout = NO_ACK_TOUT;
        }

        for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {

            if (sensorStatuses[dev].ackStatus == ACK_AWAITING) {
                insertCanCommand(message, CMD_CAN_GET_SENSOR_STATUS);
                setCanInputStatus(message, sensorStatuses[dev].state);
                setSourceCanAddress(message, sensorStatuses[dev].address, categoryDevices->categoryID);
                setOfflineMode();
                return 1;
            }
        }
    }
    return 0;
}

void setAckTimeout() {

    ackTimeout = NO_ACK_TOUT;

}

void setCanInputStatus(CANMessage* message, unsigned char state) {
    message->Data[OFFSET_CAN_INPUT_STAT] = state;
    message->NoOfBytes++;
}

signed char getCanInputNumber(categoryStruct* categoryDevices, unsigned char address) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        if (categoryDevices->deviceAddress[dev] == address) {
            return dev;
        }
    }
    return -1;
}

signed char getSensorStatus(unsigned char pinNo) {
#if PROXIMITY_SENSOR_ACTUATOR
    static char status = 0;
    static char output = 0;
    if (SENSOR_STATE_1 && (status == 0)) {
            status = 1;
            if (output) {
                output = 0;
            } else {
                output = 1;
            }
    }

    if (!SENSOR_STATE_1 && (status == 1)) {
        status = 0;
    }

    return output;
#else
#if SWITCH_SENSOR_QNTY > 0
    if (pinNo < SWITCH_SENSOR_QNTY) {
        switch (pinNo) {
            case 0: return ((SENSOR_STATE_1) > 0) ? 1: 0;
            case 1: return ((SENSOR_STATE_2) > 0) ? 1: 0;
            case 2: return ((SENSOR_STATE_3) > 0) ? 1: 0;
            case 3: return ((SENSOR_STATE_4) > 0) ? 1: 0;
        }

    }
#endif
    return -1;
#endif
    
}

void initBistableSwitchSensorPortIO() {
    ADCON1bits.PCFG = 0x0f;
    RBPU = 0;
#if PROXIMITY_SENSOR_ACTUATOR
    SENSOR_DIR |= (1 << SENSOR_PIN_1);
    LED_OUT_DIR &= ~(1 << LED_OUT_PIN);
#elif MAGNETIC_SENSOR
    SENSOR_DIR |= ((1 << SENSOR_PIN_1) | (1 << SENSOR_PIN_2) | (1 << SENSOR_PIN_3) | (1 << SENSOR_PIN_4));
#else
    SENSOR_DIR |= ((1 << SENSOR_PIN_1) | (1 << SENSOR_PIN_2)); // | (1 << SENSOR_PIN_3) | (1 << SENSOR_PIN_4)); // | (1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3) | (1 << ACTUATOR_PIN_4);

    SENSOR_DIR_B |= (1 << SENSOR_PIN_3); // | (1 << ACTUATOR_PIN_1) | (1 << ACTUATOR_PIN_2) | (1 << ACTUATOR_PIN_3) | (1 << ACTUATOR_PIN_4);
#endif

}

void setACK(CANMessage* message, categoryStruct* categoryDevices, sensorStatus* deviceStatuses) {
    for (size_t dev = 0; dev < categoryDevices->devicesQnt; dev++) {
        if ((deviceStatuses[dev].address == getReceivedMsgAddress(message)) && (CMD_CAN_GET_SENSOR_STATUS == getConfirmedCommand(message))) {
            deviceStatuses[dev].ackStatus = ACK_RECEIVED;
        }
    }
}

void makeBroadcastCommandForBistableSensor(CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command) {

    //    switch (command) {
    //
    //    }
}

#endif