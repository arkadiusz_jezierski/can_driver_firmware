
#include "uart.h"
#include <../sources/memset.c>

//========================================================================================
// Initialize UART

void UART_Init(unsigned long int baudRate) {
    unsigned int n;

    // Configure BaudRate
    BRGH = 1; // Low speed.
    BRG16 = 1; // 16-Bit Baud Rate Generator - SPBRGH and SPBRG

    // Baudrate = Fosc/[16(n+1)]  => n = ((Fosc/Baudrate)>>4) - 1;  n = SPBRGH: SPBRG;
    n = ((Fosc / baudRate) / 4) - 1;

    //    n = 68;
    SPBRG = n;
    SPBRGH = n >> 8;
    // Enable the asyncchronous serial port.
    SYNC = 0; // Asynchronous mode
    SPEN = 1; // Serial port enable.
    TRIS_TX = 0;
    TRIS_RX = 1;
    //Configure for Transmitter mode.
    //#ifdef   UART_ON
    TXEN = 1; // Transmit enable
    //Configure for Receiver mode
    CREN = 0; // Enable the reception
    //Interrupt
    RCIF = 0;
    RCIE = 0; // Reception Interrupt Enable
    RCIP = 0;
    GIE = 1; // Global Interrupt Enable
    PEIE = 1; // Perapheral Interrupt Enable
}
//=====================================================================================

void UART_SendByte(unsigned char a) {
    //    LED_UARTTX_ON;
    while (!TRMT);
    TXREG = a;
    //    LED_UARTTX_OFF;
}
//=====================================================================================

void UART_SendString(const  char* str) {
    while (*str)
        UART_SendByte(*str++);
}

void UART_SendNewLine( const char* str) {
    UART_SendByte('\r');
    UART_SendByte('\n');
    while (*str)
        UART_SendByte(*str++);
}


//====================================================================================

unsigned char UART_getFrame(UART_Buffer_FIFO *fifo) {
    static unsigned char data[BUFFER_UART_SIZE];
    static unsigned char counter = 0;
    static unsigned char length;
    static unsigned char crc = 0;


    if (counter == 0 && RCREG != REQ_FRAME_HDR) {
        crc = 0;
        return 0;
    }
    if (counter < BUFFER_UART_SIZE) {
        data[counter++] = RCREG;
        crc += data[counter - 1];
        if (counter == length && data[length - 1] == REQ_FRAME_FTR) {
            crc -= data[counter - 2];
            if (crc != data[counter - 2]) {
                crc = 0;
                counter = 0;
                length = 0;
                return 1;
            }
            if (++fifo->ptrW >= FIFO_SIZE) {
                fifo->ptrW = 0;
            }
            for (size_t i = 0; i < counter; i++) {
                fifo->uartBuffer[fifo->ptrW].data[i] = data[i];
            }
            fifo->uartBuffer[fifo->ptrW].length = length;

            counter = 0;
            length = 0;
            crc = 0;
            return 0;
        }
    } else {
        length = 0;
        counter = 0;
        crc = 0;
    }
    if (counter == 2) {
        length = data[1];
    }

    return 0;
}

void getBuffer(UART_Buffer_FIFO* fifo, UART_Buffer* buffer) {
    unsigned char tmpPtr;

    if (fifo->ptrR >= FIFO_SIZE - 1) //Check if pointer will overflow
    {
        tmpPtr = 0;
    } else {
        tmpPtr = fifo->ptrR + 1;
    }

    for (size_t i = 0; i < fifo->uartBuffer[tmpPtr].length; i++) {
        buffer->data[i] = fifo->uartBuffer[tmpPtr].data[i];
        buffer->length = fifo->uartBuffer[tmpPtr].length;
    }

    fifo->ptrR = tmpPtr;
}

unsigned char checkUartFIFObuffer(UART_Buffer_FIFO *fifo) {
    if (fifo->ptrR != fifo->ptrW)
        return 1;
    else
        return 0;
}

void initUartFIFO(UART_Buffer_FIFO *fifo) {
    fifo->ptrW = fifo->ptrR = 0;
    for (size_t f = 0; f < FIFO_SIZE; f++) {
        fifo->uartBuffer[f].length = 0;
        for (size_t i = 0; i < BUFFER_UART_SIZE; i++)
            fifo->uartBuffer[f].data[i] = 0;
    }
}

void printCanBuffer(CANMessage* message) {
    UART_SendNewLine("ID: ");
    printCanId(message->Address);
    UART_SendString(" DATA: ");
    printCanData(message->Data, message->NoOfBytes);
}

void printCanId(unsigned long id) {
    UART_SendByte('x');
    byte2ascii((unsigned char) (id >> 24), 1);
    byte2ascii((unsigned char) (id >> 16), 1);
    byte2ascii((unsigned char) (id >> 8), 1);
    byte2ascii((unsigned char) id, 1);
}

void printCanData(unsigned char* data, unsigned char len) {
    for (size_t i = 0; i < len; i++) {
        UART_SendByte('x');
        byte2ascii(data[i], 1);
        UART_SendByte(' ');
    }
}

void byte2ascii(unsigned char nmb, unsigned char hex) {
    if (hex) {
        if ((nmb >> 4) < 10) {
            UART_SendByte((nmb >> 4) + 0x30);
        } else {
            UART_SendByte((nmb >> 4) + 0x37);
        }
        if ((nmb & 0x0f) < 10) {
            UART_SendByte((nmb & 0x0f) + 0x30);
        } else {
            UART_SendByte((nmb & 0x0f) + 0x37);
        }
    } else {
        UART_SendByte(nmb / 100 + 0x30);
        UART_SendByte(nmb / 10 % 10 + 0x30);
        UART_SendByte(nmb % 10 + 0x30);
    }
}