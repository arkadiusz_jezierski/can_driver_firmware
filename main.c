#include  <pic18.h>
#include <stdlib.h>
#include "can.h"
#include "eeprom.h"
#include "comm_can_constans.h"
#include "comm_settings.h"
#include "utils.h"
#include "offline_driver.h" 

#if SWITCH_ACTUATOR_QNTY > 0
#include "bistable_switch_actuator_driver.h"
#endif
#if SWITCH_SENSOR_QNTY > 0
#include "bistable_switch_sensor_driver.h"
#endif
#if PWM_ACTUATOR_QNTY > 0
#include "pwm_driver.h"
#endif
#if RGB_ACTUATOR_QNTY > 0
#include "rgb_driver.h"
#endif
#if RGB_ADVANCE_ACTUATOR_QNTY > 0
#include "advance_rgb.h"
#endif
#if MOTION_DETECTOR_QNTY > 0
#include "motion_detector_sensor_driver.h"
#endif

#ifdef IR_SUPPORT
#include "ir_support.h"
#endif

#pragma config CONFIG1H = 0x6
#pragma config CONFIG2L = 0x08
#pragma config CONFIG2H = 0x0E
#pragma config CONFIG3H = 0x80      //PORTB <0:3> - set as I/O
#pragma config CONFIG4L = 0x91      //PORTB <0:3> - set as I/O


#ifdef DEBUG
#include "uart.h"
UART_Buffer_FIFO uartFIFO;
#endif

#if PROXIMITY_SENSOR_ACTUATOR
#define MAX_PWM 255
#define MIN_PWM 0

static unsigned char PWM_VAL[80] = {MIN_PWM, 1, 2, 3, 4, 5, 6, 8, 10, 15, 20, 25, 30, 35, 40, 45,
    50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120,
    125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190,
    195, 200, 205, 210, 215, 220, 225, 230, 235, 240, 245, 250, MAX_PWM};
volatile unsigned char pwm = 0;
volatile unsigned char pwmCnt = 0;
volatile unsigned int pwmChangeTimer = 0;

#endif



CANMessage RX_Message, TX_Message;
categoryStruct categoriesDescr[CATEGORY_QNTY];
unsigned long uid = 0;

#if 0 < CATEGORY_QNTY
unsigned char addr0[CATEGORY_ID_0_DEV_QNTY];
#endif
#if 1 < CATEGORY_QNTY
unsigned char addr1[CATEGORY_ID_1_DEV_QNTY];
#endif
#if 2 < CATEGORY_QNTY
unsigned char addr2[CATEGORY_ID_2_DEV_QNTY];
#endif
#if 3 < CATEGORY_QNTY
unsigned char addr3[CATEGORY_ID_3_DEV_QNTY];
#endif

volatile unsigned char errorOccured = 0;

volatile unsigned int secTimer = SEC_VALUE;
volatile unsigned char secElapsed = 0;
volatile unsigned char timerEvent = 0;

void initDevice(void) {
    if (firstInit) {
#ifdef DEBUG
        UART_SendNewLine("INITIALIZE DEVICE");
#endif

        setInitMaskAndFilter(uid);
        setInitMaskAndFilter(uid);
        setInitMaskAndFilter(uid);

        resetAddressRequestCounter();
        resetAddresses(&categoriesDescr);
        prepareAddressRequestCanBuffer(&TX_Message, uid, &categoriesDescr);
        startTimer();
        firstInit = 0;
    }
}

typedef void (*functionReceiver) (CANMessage *outputMessage, CANMessage *inputMessage, categoryStruct* categoryDevices, unsigned char command, unsigned char address);
typedef unsigned char (*functionTransmitter) (CANMessage *outputMessage, categoryStruct* categoryDevices, unsigned char timerEvent);

typedef struct {
    unsigned char category;
    functionReceiver functionPointer;
} categoryReceiverFunction;

typedef struct {
    unsigned char category;
    functionTransmitter functionPointer;
} categoryTransmitterFunction;

categoryReceiverFunction categoryCanReceiverFunctions[CATEGORY_QNTY];
categoryTransmitterFunction categoryCanTransmitterFunctions[CATEGORY_QNTY];

functionReceiver getCategoryCanReceiverFunction(unsigned char category) {
    for (size_t i = 0; i < CATEGORY_QNTY; i++) {
        if (category == categoryCanReceiverFunctions[i].category) {
            return categoryCanReceiverFunctions[i].functionPointer;
        }
    }
    return 0;
}

functionTransmitter getCategoryCanTransmitterFunction(unsigned char category) {
    for (size_t i = 0; i < CATEGORY_QNTY; i++) {
        if (category == categoryCanTransmitterFunctions[i].category) {
            return categoryCanTransmitterFunctions[i].functionPointer;
        }
    }
    return 0;
}

void addCategoryCanReceiverFunction(unsigned char category, functionReceiver funPointer) {
    static unsigned char i = 0;
    categoryCanReceiverFunctions[i].category = category;
    categoryCanReceiverFunctions[i++].functionPointer = funPointer;
}

void addCategoryCanTransmitterFunction(unsigned char category, functionTransmitter funPointer) {
    static unsigned char i = 0;
    categoryCanTransmitterFunctions[i].category = category;
    categoryCanTransmitterFunctions[i++].functionPointer = funPointer;
}

categoryStruct *getCategoryDevices(categoryStruct* categories, unsigned char categoryId) {
    for (size_t cat = 0; cat < CATEGORY_QNTY; cat++) {
        if (categories[cat].categoryID == categoryId) {
            return &categories[cat];
        }
    }
    return 0;
}

void runInitLevel(void) {
    initDevice();

    if (secElapsed) {
        secElapsed = 0;
#ifdef DEBUG
        UART_SendNewLine("REQUEST ADRESSES");
#endif
        CANPutToFIFO(&TX_Message);

    }

    if (CANRXMessageIsPending()) { //Check if there is an unread CAN message
        //                GIE = 0;
#ifdef DEBUG
        UART_SendNewLine("FRAME RECEIVED");
#endif
        stopTimer();
        //                CLRWDT();
        abortAllECANTransmits();

        RX_Message = CANGet(); //Get the message
        if (checkIfMsgGlobalBroadcast(&RX_Message)) {
#ifdef DEBUG
            UART_SendNewLine("CAN GLOBAL FRAME RECEIVED");
#endif
            makeGlobalBroadcastCommand(&RX_Message);
            startTimer();
            return;
        }
#ifdef DEBUG
        UART_SendNewLine("CAN ADR RESP FRAME RECEIVED");
        byte2ascii(RX_Message.Address >> 16, 1);
        byte2ascii(RX_Message.Address >> 8, 1);
        byte2ascii(RX_Message.Address >> 0, 1);
        byte2ascii(uid >> 16, 1);
        byte2ascii(uid >> 8, 1);
        byte2ascii(uid >> 0, 1);
#endif
        if (((RX_Message.Address >> 16) & 0xff) == ((uid >> 16) & 0xff)
                && ((RX_Message.Address >> 8) & 0xff) == ((uid >> 8) & 0xff)
                && ((RX_Message.Address >> 0) & 0xff) == ((uid >> 0) & 0xff)
                ) {
            char addressing = getAddressesFromCanBuffer(&RX_Message, &categoriesDescr);
#ifdef DEBUG
            UART_SendNewLine("READING ADDRESS BUFFER RESULT:");
            byte2ascii(addressing, 0);
#endif
            if (addressing == ADDRESSES_OK) {
                if (checkAllAddressed(&categoriesDescr)) {

                    prepareAddressingCompleteCanBuffer(&TX_Message, uid);

#ifdef DEBUG
                    UART_SendNewLine("ADDRESSING COMPLETE, SENDING COMPLETED FLAG");
#endif
                } else {
                    prepareAddressRequestCanBuffer(&TX_Message, uid, &categoriesDescr);

                }
            } else if (addressing == ADDRESSES_ACK) {
                if (checkAllAddressed(&categoriesDescr)) {

                    saveAddresses(&categoriesDescr);
                    //                        setRegularMaskAndFilter(uid);
                    setRegularMaskAndFilter();
                    setRegularMaskAndFilter();
                    setRegularMaskAndFilter();
                    setRunLevelNormal();
                    clearExtBuffer(&TX_Message);
#ifdef DEBUG
                    UART_SendNewLine("RECEIVED ADDRESSING_ACK");
                    UART_SendNewLine("ENTER NORMAL RUN LEVEL");
#endif
                }
            }
        }
        startTimer();
    }
  
 }

void runNormalLevel(void) {
    unsigned char categoryId = 0;
    unsigned char address = 0;
    unsigned char command = 0;
    CLRWDT();
    if (CANRXMessageIsPending()) { //Check if there is an unread CAN message
#ifdef DEBUG
        UART_SendNewLine("CAN FRAME RECEIVED");
#endif
        RX_Message = CANGet(); //Get the message
        CLRWDT();
        if (checkIfMsgGlobalBroadcast(&RX_Message)) {
#ifdef DEBUG
            UART_SendNewLine("CAN GLOBAL FRAME RECEIVED");
#endif
            makeGlobalBroadcastCommand(&RX_Message);
            return;
        }

        categoryId = getReceivedMsgCategoryId(&RX_Message);
        address = getReceivedMsgAddress(&RX_Message);
#ifdef DEBUG
        UART_SendNewLine("CAT ID: ");
        byte2ascii(categoryId, 1);
        UART_SendNewLine("ADDR: ");
        byte2ascii(address, 1);
        printCategoryDescr(&categoriesDescr);
#endif
        if (checkIfMsgForThisDevice(&categoriesDescr, categoryId, address)) {

            command = getReceivedMsgCommand(&RX_Message);
#ifdef DEBUG
                    UART_SendNewLine("COMM: ");
                    byte2ascii(command, 1);
#endif
            if (checkIfMsgBroadcast(categoryId)) {
#ifdef DEBUG
                        UART_SendNewLine("BROADCAST");
#endif
                for (size_t id = 0; id < CATEGORY_QNTY; id++) {
                    CLRWDT();
                    (categoryCanReceiverFunctions[id].functionPointer)(&TX_Message, &RX_Message, getCategoryDevices(&categoriesDescr, categoryCanReceiverFunctions[id].category), command, address);
                }
            } else {
                (getCategoryCanReceiverFunction(categoryId))(&TX_Message, &RX_Message, getCategoryDevices(&categoriesDescr, categoryId), command, address);

            }
#ifdef DEBUG
                    UART_SendNewLine("TRANS DATA QNTY");
                    byte2ascii(TX_Message.NoOfBytes, 0);
#endif
            if (TX_Message.NoOfBytes > 0) {
#ifdef DEBUG
                        UART_SendNewLine("PUT FIFO RESP DATA");
#endif
                CANPutToFIFO(&TX_Message);
                clearExtBuffer(&TX_Message);
            }
        }
#ifdef DEBUG
        else {
            UART_SendNewLine("FRAME REFUSED");
        }
#endif

    }
    for (size_t id = 0; id < CATEGORY_QNTY; id++) {
        if ((categoryCanTransmitterFunctions[id].functionPointer)(&TX_Message, getCategoryDevices(&categoriesDescr, categoryCanTransmitterFunctions[id].category), (timerEvent) ? timerEvent-- : 0)) {
            if (TX_Message.NoOfBytes > 0) {
#ifdef DEBUG
                UART_SendNewLine("PUT FIFO TRANS DATA");
#endif
                CLRWDT();
                CANPutToFIFO(&TX_Message);
                clearExtBuffer(&TX_Message);
            }
        }

    }

}

void runLevelError(void) {
    CLRWDT();
    if (secElapsed) {
#ifdef DEBUG
        UART_SendNewLine("ERROR FRAME SENDING");
#endif
        secElapsed = 0;
        createErrorFrame(&TX_Message, uid);
        CANPutToFIFO(&TX_Message);
        clearExtBuffer(&TX_Message);
    }
    if (CANRXMessageIsPending()) { //Check if there is an unread CAN message
        RX_Message = CANGet(); //Get the message
        CLRWDT();
        if (checkIfMsgGlobalBroadcast(&RX_Message)) {
            if (CMD_CAN_GLOB_ERR_ACK == getGlobalBroadcastCommand(&RX_Message) && checkUIDinFrameId(&RX_Message)) {
#ifdef DEBUG
                UART_SendNewLine("ACK FOR ERROR FRAME RECEIVED");
#endif
                exitRunLevelError();
            }
        }
    } else {
        setOfflineMode();
    }
}


void sendStartingUpInfo() {
    createStartingUpInfo(&TX_Message, uid);
    CANPutToFIFO(&TX_Message);
    clearExtBuffer(&TX_Message);
}

void main() //Main entry
{

    initEEPROM();
    initStartupInfo();

#if 0 < CATEGORY_QNTY       //@MUST SET

#if SWITCH_ACTUATOR_QNTY > 0
    initBistableSwitchActuatorPortIO();
    addCategoryCanReceiverFunction(CATEGORY_ID_0, makeCommandForBistableActuator);
    addCategoryCanTransmitterFunction(CATEGORY_ID_0, readDeviceBistableActuator);
#endif
#if SWITCH_SENSOR_QNTY > 0 && CATEGORY_QNTY == 1
    initBistableSwitchSensorPortIO();
    addCategoryCanReceiverFunction(CATEGORY_ID_0, makeCommandForBistableSensor);
    addCategoryCanTransmitterFunction(CATEGORY_ID_0, readDeviceBistableSensor);
#endif
#if PWM_ACTUATOR_QNTY > 0
    initPwmActuatorPortIO();
    addCategoryCanReceiverFunction(CATEGORY_ID_0, makeCommandForPwmActuator);
    addCategoryCanTransmitterFunction(CATEGORY_ID_0, readDevicePwmActuator);
#endif
#if RGB_ACTUATOR_QNTY > 0
    initRgbActuatorPortIO();
    addCategoryCanReceiverFunction(CATEGORY_ID_0, makeCommandForRgbActuator);
    addCategoryCanTransmitterFunction(CATEGORY_ID_0, readDeviceRgbActuator);
#endif
#if RGB_ADVANCE_ACTUATOR_QNTY > 0
    initRgbActuatorPortIO();
    addCategoryCanReceiverFunction(CATEGORY_ID_0, makeCommandForRgbActuator);
    addCategoryCanTransmitterFunction(CATEGORY_ID_0, readDeviceRgbActuator);
#endif
    #if MOTION_DETECTOR_QNTY > 0
    initMotionDetectorPortIO();
    addCategoryCanReceiverFunction(CATEGORY_ID_0, makeCommandForMotionDetector);
    addCategoryCanTransmitterFunction(CATEGORY_ID_0, readDeviceMotionDetector);
#endif
#endif

#if 1 < CATEGORY_QNTY
#if SWITCH_SENSOR_QNTY > 0
    initBistableSwitchSensorPortIO();
    addCategoryCanReceiverFunction(CATEGORY_ID_1, makeCommandForBistableSensor);
    addCategoryCanTransmitterFunction(CATEGORY_ID_1, readDeviceBistableSensor);
#endif
#endif

#if 2 < CATEGORY_QNTY
    addCategoryFunction(CATEGORY_ID_2, 0);
    addCategoryCanTransmitterFunction(CATEGORY_ID_2, 0);
#endif
#if 3 < CATEGORY_QNTY
    addCategoryFunction(CATEGORY_ID_3, 0);
    addCategoryCanTransmitterFunction(CATEGORY_ID_3, 0);
#endif


#if 0 < CATEGORY_QNTY
    categoriesDescr[0].deviceAddress = &addr0;
#endif
#if 1 < CATEGORY_QNTY
    categoriesDescr[1].deviceAddress = &addr1;
#endif
#if 2 < CATEGORY_QNTY
    categoriesDescr[2].deviceAddress = &addr2;
#endif
#if 3 < CATEGORY_QNTY
    categoriesDescr[3].deviceAddress = &addr3;
#endif

    
#ifdef DEBUG
    UART_Init(115200);
    initUartFIFO(&uartFIFO);
    UART_SendNewLine("DEVICE RUNNING");
    UART_SendNewLine("RCON: ");
    byte2ascii(RCON, 1);

#endif

    uid = getInitUID();

#ifdef DEBUG
    for (int i = 0; i < 50; i++) {
        UART_SendString(" ");
        byte2ascii(i, 0);
        UART_SendString(": x");
        byte2ascii(eepromRead(i), 1);
    }
#endif

    ECANInitializeOnStartup();
    initCategories(&categoriesDescr);

    GIE = 1;
    PEIE = 1;
    IPEN = 1;

    initTimer();
    
#ifdef IR_SUPPORT
    initIRSupport();
#endif
    
    startTimer();
    SWDTEN = 1;     
    CLRWDT();

    if (isDeviceFirstRunning()) {

        setRunLevelInit();
#ifdef DEBUG
        UART_SendNewLine("RUN LEVEL INIT");
#endif
    } else if (isDeviceAddressed()) {
        setRunLevelNormal();
        
        loadAddresses(&categoriesDescr);
        setRegularMaskAndFilter();
        setRegularMaskAndFilter();
        setRegularMaskAndFilter();
        CLRWDT();
        sendStartingUpInfo();
#ifdef DEBUG
        printCategoryDescr(&categoriesDescr);
        UART_SendNewLine("RUN LEVEL NORMAL");
#endif
    } else {
#ifdef DEBUG
        UART_SendNewLine("NO CONFIG - EXIT");
#endif
        return;
    }

    CLRWDT();

    while (1) {
        if (getRunLevel() == RUN_LVL_INIT) {
            runInitLevel();
        }
        if (getRunLevel() == RUN_LVL_NORM) {
            runNormalLevel();
        }

        if (getRunLevel() == RUN_LVL_BOOT) {
#ifdef DEBUG
            UART_SendNewLine("ENTER BOOT MODE");
#endif
            CLRWDT();
            delay_ms(1000);
            enterBoot();
        }

        if (getRunLevel() == RUN_LVL_ERROR) {
            runLevelError();
        }
#ifdef OFFLINE_MODE
        if (checkOfflineMode()) {
            makeOfflineModeOperations();
        }
#endif
        
#ifdef  IR_SUPPORT
        if (irCodeReceived) {
            irCodeReceived = 0;
            handleIRSupport();
        }
#endif


        if (errorOccured) {
            errorOccured = 0;
            if (getRunLevel() == RUN_LVL_NORM) {
                setRunLevelError();
            }
            ecanErrorHandler();
        }
        CLRWDT();
    }
}



void interrupt ISR_high(void) {
    unsigned char TempCANCON;

    if (PIR3 & PIE3) {
        TempCANCON = ECANCON;
        if (PIR3bits.IRXIF) {
            IRXIF = 0;
        } else if (ERRIF && ERRIE) {
            ERRIF = 0; //Clear interrupt flag
            errorOccured = 1;
        } else if (PIR3bits.RXB1IF && PIE3bits.RXB1IE) {
            ECANCON = (ECANCON & 0xE0) | (CANSTAT & 0x1F);
            CANGetMessage();
            RXB1IF = 0; //Clear interrupt flag
        } else if (PIR3bits.TXB2IF && PIE3bits.TXB2IE) {
            unsigned char interruptBufferAddress = getInterruptAddress();
            if (interruptBufferAddress) {
                clearInterruptEnableBit(interruptBufferAddress);
            }
            interruptBufferAddress = getFirstFreeTxBuffer();
            if (interruptBufferAddress) {
                ECANCON = (ECANCON & 0xE0) | interruptBufferAddress;
                if (CANPutMessage()) { //if there wasn't any more messages to send
                    clearInterruptEnableBit(interruptBufferAddress);
                }
            }
            PIR3bits.TXB2IF = 0;
        }  else if (WAKIF && WAKIE) {
            WAKIF = 0; //Clear interrupt flag
        }
        ECANCON = TempCANCON;
    }
    interruptEnabled = 0;
}

void interrupt low_priority ISR_low(void) {
    
#ifdef IR_SUPPORT
    
    if (RBIF) {
      
        if (checkCode(PORTB)) {
           //signal from IR receiver received - do something
            irCodeReceived = 1;
        }
        
        RBIF = 0;
    }
#endif



#if PROXIMITY_SENSOR_ACTUATOR

    static char out = 1;
    static char cnt = 2;


    if (TMR1IF) {


        pwmCnt++;

        pwmChangeTimer++;
        if (pwmCnt >= MAX_PWM) {
            pwmCnt = 0;
        }

        if (pwmChangeTimer >= 1000) {
            if (outputRequestValue) {
                if (PWM_VAL[pwm] < MAX_PWM) {
                    pwm++;
                }
            } else {
                if (pwm > MIN_PWM) {
                    pwm--;
                }
            }
            pwmChangeTimer = 0;
        }

        if (pwmCnt < PWM_VAL[pwm]) {
                        ACTUATOR_1_ON//setOutputStatus(0, 1);
        } else {
                        ACTUATOR_1_OFF//setOutputStatus(0, 0);
        }

        //CLRWDT();
        cnt--;
        if (cnt == 0) {
            if (out == 1) {
                out = 0;
                LED_OUT_ON;
                cnt = 1;
            } else {
                out = 1;
                cnt = 2;
                LED_OUT_OFF;
            }

        }
        static int timerDelay = 0;
        timerDelay++;
        if (timerDelay >= 500) {
            timerDelay = 0;
            timerEvent = CATEGORY_QNTY;
        }
        secTimer--;
        if (secTimer <= 0) {
            secElapsed = 1;
            secTimer = SEC_VALUE;
        }
        TMR1L = 0xf0;
        TMR1H = 0xff;



#else
    if (TMR1IF) {
        TMR1H = 0xE0;//0x4E;//0xE0;
        timerEvent = CATEGORY_QNTY;
        secTimer--;
        if (secTimer <= 0) {

            secElapsed = 1;
            secTimer = SEC_VALUE;
        }
#endif

        TMR1IF = 0;
    }

#ifdef DEBUG
    if (PIR1bits.RCIF && PIE1bits.RCIE) {
        //        UART_getFrame(&uartFIFO);
    }
#endif
       
#if RGB_ACTUATOR_QNTY > 0
    if (TMR3IF) { 
        TMR3H = 0xF0;//0xFC;
        if(driveRGB()) {
            writeRGBInterrupt();
        }
        
        TMR3IF = 0;
    }
#endif
  
}